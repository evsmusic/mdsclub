package com.edwardstock.mds.api;


import android.os.AsyncTask;
import android.util.Log;

import com.edwardstock.mds.helpers.HttpHelper;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.RemoteModel;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ApiClient {

	public static final String REPORTER_URL = "http://dev.api.mdsclub.ru/reporter/report"; // POST requests only; request does not return any response
	public static final String FIELD_ID                  = "id";
	public static final String FIELD_URL                 = "url";
	public static final String FIELD_AUTHOR              = "author";
	public static final String FIELD_TITLE               = "title";
	public static final String FIELD_SIZE                = "size";
	public static final String FIELD_SIZE_HUMAN_READABLE = "size_string";
	public static final String FIELD_DURATION            = "duration";
	public static final String FIELD_DATE                = "date";
	public static final String FIELD_RADIO               = "radio";
	public static final String FIELD_PAGES_COUNT         = "pages_count";
	public static final String FIELD_TOTAL_ITEMS         = "total_items";
	public static final String FIELD_NEXT_MODEL_ID       = "next_model_id";
	public static final String FIELD_PREVIOUS_MODEL_ID   = "previous_model_id"; //входящее значение по умолчанию: -1
	private static final String TAG = ApiClient.class.getName();
	private static final String URL_ITEM = "http://dev.api.mdsclub.ru/podcast/";
	private static final String URL_ITEM_COMMENTS = "http://dev.api.mdsclub.ru/podcast/comments?id=";
	private static final String URL_LIST = "http://dev.api.mdsclub.ru/podcast/get";
	private static final String URL_SEARCH = "http://dev.api.mdsclub.ru/podcast/search/q/";
	private static final String URL_RANDOM = "http://dev.api.mdsclub.ru/podcast/rand";
	
    public static int getRandomId() throws ExecutionException, InterruptedException {
        AsyncTask<Void, Void, Integer> t = new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                return ApiClient.getRandom();
            }

        }.execute();

        return t.get();
    }

	public static int getRandom() {
		URL url = null;
		try {
			url = new URL(URL_RANDOM);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		try {
			return Integer.parseInt(HttpHelper.getContentsByUrl(url));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 1;
	}

	public List<RemoteModel> getComments(int remoteModelId) throws JSONException, IOException {
		URL url = new URL(URL_ITEM_COMMENTS + String.valueOf(remoteModelId));
		JSONArray models = HttpHelper.getJSONArrayByUrl(url);

		List<RemoteModel> items = new ArrayList<>(models.size());

		for (Object model : models) {
			items.add(new RemoteModel((JSONObject) model));
		}

		return items;
	}

	/**
	 * Берем все модели с указанием номера страницы
	 *
	 * @param page Номер страницы
	 * @return Массив моделей
	 * @throws JSONException
	 * @throws MalformedURLException
	 */
	public List<RemoteModel> getAll(int page) throws JSONException, IOException {
		URL url = new URL(URL_LIST + "/page/" + String.valueOf(page));
		JSONArray models = HttpHelper.getJSONArrayByUrl(url);

		List<RemoteModel> items = new ArrayList<>(models.size());

		for (Object model : models) {
			items.add(new RemoteModel((JSONObject) model));
		}

		return items;
	}

	public List<RemoteModel> getAll(int page, String additionalParams) throws JSONException, IOException {
		URL url = new URL(URL_LIST + "/page/" + String.valueOf(page) + additionalParams);
		JSONArray models = HttpHelper.getJSONArrayByUrl(url);

		List<RemoteModel> items = new ArrayList<>(models.size());

		for (Object model : models) {
			items.add(new RemoteModel((JSONObject) model));
		}

		return items;
	}

	/**
	 * Берем модель по айдишнику
	 *
	 * @param id Айдишник модели
	 * @return Экземпляр модели
	 */
	public LocalModel getOne(int id) throws IOException {
		URL oneUrl = null;
		LocalModel oldModel = LocalModel.getByModelId(id);
		if (oldModel != null) {
			Log.d(TAG, "Model already exists");
			return oldModel;
		}

		Log.d(TAG, "New model creating");

		try {
			oneUrl = new URL(URL_ITEM + id);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		JSONObject obj = HttpHelper.getJSONObjectByUrl(oneUrl);

		return LocalModel.createFromAPI(obj);
	}

	public List<RemoteModel> search(String searchString) throws IOException {
		return search(searchString, "");
	}

	/**
	 * Ищем модели по строке
	 *
	 * @param searchString Что ищем?
	 * @return Массив моделей
	 * @throws MalformedURLException
	 */
	public List<RemoteModel> search(String searchString, String additionalParams) throws IOException {
		String requestUrl;
		requestUrl = URLEncoder.encode(searchString, "UTF-8");

		URL url = new URL(URL_SEARCH + requestUrl + additionalParams);
		Log.d(TAG, "Request url: " + URL_SEARCH + searchString);
		JSONArray models = HttpHelper.getJSONArrayByUrl(url);

		List<RemoteModel> items = new ArrayList<>(models.size());

		for (Object model : models) {
			items.add(new RemoteModel((JSONObject) model));
		}

		return items;
	}
}

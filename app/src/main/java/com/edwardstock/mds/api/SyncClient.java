package com.edwardstock.mds.api;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.edwardstock.mds.helpers.HttpHelper;
import com.edwardstock.mds.helpers.UnitConverter;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.RemotePlaylist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SyncClient {

	private final static String TAG = "SyncClient";

	private final static String DB_FILENAME = "mdsclubv7.db";

	/**
	 * Туда отправляются: email либо auth_token
	 */
	private final static String AUTH_URL = "http://dev.api.mdsclub.ru/user/auth"; //POST запрос
	private final static String SYNC_URL = "http://dev.api.mdsclub.ru/user/sync"; //POST запрос
	private final static String GET_SYNC_URL = "http://dev.api.mdsclub.ru/user/get-sync"; //GET запрос
	private final static String COUNT_MODELS_URL = "http://dev.api.mdsclub.ru/user/count-models"; //GET
	private final static String COUNT_PLAYLISTS_URL = "http://dev.api.mdsclub.ru/user/count-playlists"; //GET
	private final static String DESTROY_SESSION_URL = "http://dev.api.mdsclub.ru/user/destroySession";

	/**
	 * Имя приватных настроек где хранятся auth_key и email
	 */
	private final static String PRIVATE_PREFERENCES_KEY = "service_data";
	private final static String AUTH_TOKEN_KEY = "service_auth_token";
	private final static String RESP_AUTH_TOKEN_KEY = "auth_token";
	private final static String LAST_SYNC_TIME = "last_sync_time";

	private File dbLocation;
	private Context context;
	private SharedPreferences preferences;
	private String authToken;
	private String email;
	private SyncProgressListener progressListener;
	private String latestSyncDateString;


	public SyncClient(Context context) {
		this.context = context;
		this.preferences = context.getSharedPreferences(PRIVATE_PREFERENCES_KEY, Context.MODE_PRIVATE);
		this.authToken = preferences.getString(AUTH_TOKEN_KEY, null);
		this.latestSyncDateString = preferences.getString(LAST_SYNC_TIME, null);

		prepareDbLocation();

		Log.d(TAG, this.dbLocation.getAbsolutePath());
		Log.d(TAG, "File exists? " + this.dbLocation.exists());
		Log.d(TAG, "DB Size: " + UnitConverter.humanReadableByteCount(this.dbLocation.length(), true));
	}

	private void prepareDbLocation() {
		this.dbLocation = new File(context.getFilesDir().getPath() + "/../databases/" + DB_FILENAME);
	}

	public SyncClient(Context context, String email) {
		this.email = email;
		this.context = context;
		this.preferences = context.getSharedPreferences(PRIVATE_PREFERENCES_KEY, Context.MODE_PRIVATE);
		this.authToken = preferences.getString(AUTH_TOKEN_KEY, null);

		prepareDbLocation();

		Log.d(TAG, this.dbLocation.getAbsolutePath());
		Log.d(TAG, "File exists? " + this.dbLocation.exists());
		Log.d(TAG, "DB Size: " + UnitConverter.humanReadableByteCount(this.dbLocation.length(), true));
	}

	public static boolean userAuthenticated(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PRIVATE_PREFERENCES_KEY, Context.MODE_PRIVATE);
		return prefs.getString(AUTH_TOKEN_KEY, null) != null;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public String getLastSyncTime() {
		return this.latestSyncDateString;
	}

	public boolean syncRunFirstTime() {
		return this.latestSyncDateString != null;
	}

	private int countRemoteModels() throws IOException, JSONException {
		return countRemote(COUNT_MODELS_URL);
	}

	private int countRemote(String url) throws IOException, JSONException {
		String urlParameters = "auth_token=" + this.authToken;
		String response = HttpHelper.getContentsByUrl(new URL(url + "?" + urlParameters));

		JSONObject reader = new JSONObject(response);
		JSONObject resp = reader.getJSONObject("response");

		return resp.getInt("count_all");
	}

	private int countRemotePlaylists() throws IOException, JSONException {
		return countRemote(COUNT_PLAYLISTS_URL);
	}

	public boolean authenticate() throws IOException {
		if (!userAuthenticated()) {
			String urlParameters = "email=" + this.email;
			HttpURLConnection con = createConnection(new URL(AUTH_URL), "POST", urlParameters);
			int responseCode = con.getResponseCode();

			if (responseCode != 200) {
				return false;
			}
			Log.d(TAG, "\nSending 'POST' request to URL : " + AUTH_URL + "?" + urlParameters);
			Log.d(TAG, "Post parameters : " + urlParameters);
			Log.d(TAG, "Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuilder response = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			//print result
			try {
				return parseAuthData(response.toString());
			} catch (JSONException e) {
				return false;
			}
		} else {
			return true;
		}
	}

	public boolean userAuthenticated() {
		return this.authToken != null;
	}

	private HttpURLConnection createConnection(URL url, String method, String urlParams) throws IOException {
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		//add request header
		con.setRequestMethod(method.toUpperCase());
		con.setRequestProperty("Accept", "text/json,application/json;q=0.9,image/webp,*/*;q=0.8");
		con.setRequestProperty("User-Agent", "Android Application");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send get request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParams);
		wr.flush();
		wr.close();

		return con;
	}

	private boolean parseAuthData(String response) throws JSONException {
		JSONObject reader = new JSONObject(response);
		JSONObject resp = reader.getJSONObject("response");
		String authToken = resp.getString(RESP_AUTH_TOKEN_KEY);

		SharedPreferences.Editor editor = this.preferences.edit();
		editor.putString(AUTH_TOKEN_KEY, authToken);
		editor.apply();

		this.authToken = authToken;

		return true;
	}

	public void syncWithServer() throws IOException {

		String response = HttpHelper.getContentsByUrl(new URL(GET_SYNC_URL + "?auth_token=" + this.authToken));

		try {
			sendSyncData();
			JSONObject all = new JSONObject(response);
			if (!all.isNull("models")) {
				JSONArray models = all.getJSONArray("models");
				synchronizeModels(models);
			}
		} catch (JSONException e) {
			try {
				JSONArray all = new JSONArray(response);
				if (all.length() == 0) {
					setLastSyncTime();
					return;
				}

			} catch (JSONException e1) {
				e1.printStackTrace();
			}

		}


//		if(!all.isNull("playlists")) {
//			JSONArray playlists = all.getJSONArray("playlists");
//			synchronizePlaylists(playlists);
//		}

		setLastSyncTime();
	}

	private void sendSyncData() throws IOException {
		sendFile(this.dbLocation);
	}

	private void synchronizeModels(JSONArray models) throws JSONException {

		for (int i = 0; i < models.length(); i++) {
			JSONObject object = models.getJSONObject(i);
			if (LocalModel.exists(object.getInt("modelId"))) {
				continue;
			}

			LocalModel.createFromSync(object);
		}
	}

	private void setLastSyncTime() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
		Date now = new Date();
		this.latestSyncDateString = sdf.format(now);
		SharedPreferences.Editor e = this.preferences.edit();
		e.putString(LAST_SYNC_TIME, this.latestSyncDateString);
		e.apply();
	}

	private void sendFile(File file) throws IOException {
		if (progressListener != null) {
			progressListener.onUpdate(1, 1, SyncType.SYNC_OUT);
		}

	}

	private void synchronizePlaylists(JSONArray remotePlaylists) throws JSONException {
		for (int i = 0; i < remotePlaylists.length(); i++) {
			JSONObject remotePlaylistObject = remotePlaylists.getJSONObject(i);
			RemotePlaylist remotePlaylist = new RemotePlaylist(remotePlaylistObject);
			if (!remotePlaylist.hasLocalCopy()) {
				remotePlaylist.createLocalCopy();
			} else {
				List<RemotePlaylist.RemotePlaylistItem> remoteItems = remotePlaylist.getItems();
				for (RemotePlaylist.RemotePlaylistItem plItem : remoteItems) {
					if (!plItem.hasLocalCopy()) {
						plItem.createLocalCopy();
					}
				}
			}
		}
	}

	public enum SyncType {
		SYNC_NONE,
		SYNC_IN,
		SYNC_OUT,
		SYNC_IO
	}

	public interface SyncProgressListener {
		void onUpdate(int progress, int total, SyncType type);

		void onComplete();
	}


}

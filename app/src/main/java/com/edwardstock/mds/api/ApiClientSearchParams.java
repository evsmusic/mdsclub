package com.edwardstock.mds.api;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ApiClientSearchParams {
	public final static String PARAM_ORDER = "order";
	public final static String PARAM_ORDER_ARROW = "orderArrow";
	public final static String PARAM_AUTHOR = "author";

	public final static String SORT_ASC = "ASC";
	public final static String SORT_DESC = "DESC";

	private int order = -1;
	private int orderArrow = -1;
	private String author;

	public ApiClientSearchParams(int order, int orderArrow) {
		this.order = order;
		this.orderArrow = orderArrow;
	}

	public ApiClientSearchParams(int order, int orderArrow, String author) {
		this.order = order;
		this.orderArrow = orderArrow;
		this.author = author;
	}

	public String buildParams() throws UnsupportedEncodingException {
		StringBuilder builder = new StringBuilder();
		builder.append('/');

		if(author != null) {
			builder.append(PARAM_AUTHOR).append('/').append(URLEncoder.encode(author, "UTF-8")).append('/');
		}

		if(order > -1) {
			builder.append(PARAM_ORDER).append('/').append(String.valueOf(order)).append('/');
		}

		if(orderArrow > -1) {
			builder.append(PARAM_ORDER_ARROW).append('/').append(getStringArrow(orderArrow)); //внимание! на конце не должно быть слэша
		}

		return builder.toString();
	}

	private String getStringArrow(int arrow) {
		return arrow == 0 ? SORT_ASC : SORT_DESC;
	}


}

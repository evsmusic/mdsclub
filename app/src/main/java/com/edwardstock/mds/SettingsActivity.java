package com.edwardstock.mds;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.edwardstock.mds.api.SyncClient;
import com.edwardstock.mds.helpers.PreferencesHelper;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.playmarket.ProductManager;

import org.json.JSONException;
import org.json.JSONObject;


public class SettingsActivity extends BaseActivity{

	public final static String ACTION_DONATE = "com.edwardstock.mds.SettingsActivity.ACTION_DONATE";
	private final static String TAG = "SETTINGS";
	protected static boolean openDonate = false;
	protected Settings settingsFragment;

	protected boolean mServiceIsConnected = false;
	protected IInAppBillingService mService;
	protected ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
			mServiceIsConnected = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mServiceIsConnected = false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		findAndSetSupportActionBar(R.id.main_bar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
		serviceIntent.setPackage("com.android.vending");
		bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

		sendGAScreen();

		SyncClient client = new SyncClient(this, "edward.vstock@gmail.com");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if(id == android.R.id.home) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mService != null) {
			unbindService(mServiceConnection);
		}
	}

	@Override
	protected void onStart() {
		settingsFragment = new Settings();
		super.onStart();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.add(R.id.settings_fragment, settingsFragment);
		ft.commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "try to result activity");
		if (requestCode == 1001) {
			int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
			String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
			String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

			if (resultCode == RESULT_OK) {
				try {

					JSONObject jo = new JSONObject(purchaseData);
					String sku = jo.getString("productId");
					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
					sp.edit().putString("donate", sku).apply();
					Toast.makeText(this, "Благодарю Вас за поддержку! Проект будет жить =)", Toast.LENGTH_LONG).show();
				}
				catch (JSONException e) {
					Toast.makeText(this, "Ошибка парсинга данных Play Market", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
			}
		}
	}

	public static class Settings extends PreferenceFragment implements Preference.OnPreferenceClickListener {

		public final static String PARAM_SEEK_STEP = "seek_step";

		public final static String PARAM_HIDE_LISTENED = "hide_listened";

		public final static String PARAM_DONATE__BASIC = "help_basic";
		public final static String PARAM_DONATE__ADVANCED = "help_advanced";
		public final static String PARAM_DONATE__PREMIUM = "help_premium";

		public final static String PARAM_AUTO_BOOKMARK__STATE = "auto_bookmark";
		public final static String PARAM_AUTO_BOOKMARK__MIN_TIME = "auto_bookmark_min_time";
		public final static String PARAM_BM_BEHAVIOR = "bookmark_behavior";
		public final static String PARAM_BM__POSITION_START = "position_start";
		public final static String PARAM_BM__POSITION_ASK = "position_ask";
		public final static String PARAM_BM__POSITION_NO_ASK = "position_no_ask";

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.pref_general);

			getPreferenceScreen().findPreference(PARAM_DONATE__BASIC).setOnPreferenceClickListener(this);
			getPreferenceScreen().findPreference(PARAM_DONATE__ADVANCED).setOnPreferenceClickListener(this);
			getPreferenceScreen().findPreference(PARAM_DONATE__PREMIUM).setOnPreferenceClickListener(this);

            if(SyncClient.userAuthenticated(getActivity())) {
                Toast.makeText(getActivity(), R.string.already_auth, Toast.LENGTH_SHORT).show();
                getPreferenceScreen().findPreference("auth").setEnabled(false);
            } else {
                getPreferenceScreen().findPreference("auth").setOnPreferenceClickListener(this);
            }
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			View view = getView();
			if (view != null) {
				view.setBackgroundResource(R.color.theme_color);
				ActionBarActivity activity = (ActionBarActivity) getActivity();
				getView().setPadding(0, activity.getSupportActionBar().getHeight(), 0, 0);
				activity.setSupportActionBar(new Toolbar(activity));
				activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
			}

			final Intent intent = getActivity().getIntent();
			if(intent != null) {
				final String startScreen = intent.getStringExtra("PREF_KEY");
				if (startScreen != null) {
					PreferencesHelper.launchPreferenceScreen(this, startScreen);
				}
			}
		}

		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			Log.d(TAG, "try to result fragment activity");
			((SettingsActivity) getActivity()).onActivityResult(requestCode, resultCode, data);
		}

		@Override
		public boolean onPreferenceClick(Preference preference) {
			String key = preference.getKey();


			if(((SettingsActivity) getActivity()).mService != null && key.equals(PARAM_DONATE__BASIC) || key.equals(PARAM_DONATE__ADVANCED) || key.equals(PARAM_DONATE__PREMIUM)) {
				ProductManager productManager = ProductManager.getInstance(getActivity(), ((SettingsActivity) getActivity()).mService);
				String sku = preference.getKey();
				try{
					productManager.buyItem(sku);
				} catch (RemoteException | IntentSender.SendIntentException e) {
					e.printStackTrace();
				}
			}
			
			if(key.equals("auth")) {
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String userHasPaidFor = sp.getString("donate", null);

				if(((SettingsActivity) getActivity()).mService == null) {
					new AsyncTask<Void, Void, Void>() {
						private ProgressDialog progressDialog;

						@Override
						protected Void doInBackground(Void... params) {
							while(((SettingsActivity) getActivity()).mService == null) {
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							return null;
						}

						protected void onPreExecute() {
							progressDialog = new ProgressDialog(getActivity());
							progressDialog.setTitle("Ожидание ответа от PlayMarket");
							progressDialog.setMessage("Подождите...");
							progressDialog.show();
						}

						protected void onPostExecute(Void re) {
							super.onPostExecute(re);
							progressDialog.dismiss();
							progressDialog = null;
						}
					}.execute();
				}

				boolean userCantSync =
					userHasPaidFor == null ||
					!userHasPaidFor.equals(PARAM_DONATE__PREMIUM) ||
					!ProductManager.getInstance(getActivity(), ((SettingsActivity) getActivity()).mService).productIsBought(PARAM_DONATE__PREMIUM);

				if(userCantSync && BuildConfig.PRODUCTION_MODE) {
					Toast.makeText(getActivity(), "Чтобы получить функцию синхронизации, Вы должны внести пожертвование типа \"Premium\"", Toast.LENGTH_LONG).show();
				} else {
					startActivity(new Intent(getActivity(), SyncActivity.class));
				}
			}

			return true;
		}
	}

}

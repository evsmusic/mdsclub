package com.edwardstock.mds;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.edwardstock.mds.adapters.PlaylistAdapter;
import com.edwardstock.mds.implementations.BaseListActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.models.PlaylistItem;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class PlaylistsActivity extends BaseListActivity<PlaylistAdapter, ArrayList<Playlist>> {

	public final static String PARAM_PLAYLIST_ID = "playlist_id";
	private final static String TAG = "Playlists";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlist);

		Log.d(TAG, new
			Select()
			.from(LocalModel.class)
			.innerJoin(PlaylistItem.class).on("playlist_item.playlistId = local_model.Id")
			.toSql());

		Toolbar toolbar = findAndSetSupportActionBar(R.id.main_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		loadData();
	}

	private void loadData() {
		new AsyncTask<Void, Void, List<Playlist>>() {
			@Override
			protected List<Playlist> doInBackground(Void... params) {
				return Playlist.findAll();
			}

			@Override
			protected void onPostExecute(List<Playlist> res) {
				super.onPostExecute(res);
				initList(new ArrayList<>(res));
			}
		}.execute();
	}

	@Override
	protected void initList(ArrayList<Playlist> items) {
		setListView((ListView) findViewById(R.id.playlist_listview));
		setAdapter(new PlaylistAdapter(this, items));
		getListView().setAdapter(getAdapter());
		getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				openPlaylist(id);
			}
		});

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.attachToListView(getListView());
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showCreatePlaylistDialog(new Playlist.PlaylistDialogEventListener() {
					@Override
					public void onPositive() {
						loadData();
					}

					@Override
					public void onNegative() {

					}
				});
			}
		});
	}

	private void openPlaylist(long id) {
		Intent i = new Intent(this, PlaylistItemActivity.class);
		i.putExtra(PARAM_PLAYLIST_ID, (int) id);

		startActivity(i);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}

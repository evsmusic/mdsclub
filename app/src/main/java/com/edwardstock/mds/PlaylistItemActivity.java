package com.edwardstock.mds;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edwardstock.mds.adapters.PlaylistItemAdapter;
import com.edwardstock.mds.implementations.BaseListActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.models.PlaylistItem;

import java.util.ArrayList;

public class PlaylistItemActivity extends BaseListActivity<PlaylistItemAdapter, ArrayList<LocalModel>> {

	private Playlist playlist;
	private int playlistId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlist_items);
		Toolbar toolbar = findAndSetSupportActionBar(R.id.main_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		Bundle bundle = getIntent().getExtras();
		playlistId = (int) bundle.getLong(PlaylistsActivity.PARAM_PLAYLIST_ID, 0);

		if (playlistId == 0) {
			finish();
		}

		this.playlist = Playlist.findByPk(this.playlistId);
		setTitle(playlist.title);
		toolbar.setSubtitle("Элементов: " + Playlist.countItems(playlist));

		loadData();

	}

	private void loadData() {
		new AsyncTask<Void, Void, ArrayList<LocalModel>>() {
			@Override
			protected ArrayList<LocalModel> doInBackground(Void... params) {
				return new ArrayList<>(PlaylistItem.getModels(playlistId));
			}

			@Override
			protected void onPostExecute(ArrayList<LocalModel> res) {
				super.onPostExecute(res);
				initList(res);
			}
		}.execute();

	}

	@Override
	protected void initList(ArrayList<LocalModel> items) {
		setListView((ListView) findViewById(R.id.playlist_items_listview));
		setAdapter(new PlaylistItemAdapter(this, items, playlistId));
		getListView().setAdapter(getAdapter());
		getAdapter().setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startPlayerQueue(playlistId, position, (int) id);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}

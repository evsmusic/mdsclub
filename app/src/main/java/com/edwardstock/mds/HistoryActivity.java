package com.edwardstock.mds;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.edwardstock.mds.adapters.HistoryAdapter;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends BaseActivity {

	private ListView listView;
	private HistoryAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.favorites_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		new AsyncTask<Void, Void, List<LocalModel>>() {
			@Override
			protected List<LocalModel> doInBackground(Void... params) {
				return LocalModel.getListened();
			}

			protected void onPostExecute(List<LocalModel> result) {
				super.onPostExecute(result);
				initMainList(new ArrayList<>(result));
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
		if (!sharedPref.getBoolean(getString(R.string.history_startup_info), false)) {
			AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ad.setTitle("Формирование истории");
			ad.setMessage("Модели в историю попадают только в 2х случаях: если Вы явно отметили модель как \"Прослушанную\", либо дослушали выпуск до конца (то-есть до остановки)");
			ad.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) { /*do nothing*/ }
			});

			ad.setCancelable(true);
			ad.show();
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean(getString(R.string.history_startup_info), true);
			editor.apply();
		}

		sendGAScreen();
	}

	private void initMainList(ArrayList<LocalModel> items) {
		adapter = new HistoryAdapter(this, items);

		listView = (ListView) findViewById(R.id.historyListView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(HistoryActivity.this, PlayerActivity.class);
				intent.putExtra(PlayerActivity.PARAM_MODEL_ID, (int) id);
				startActivity(intent);
				listView.invalidate();
			}
		});

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {
				new AlertDialog.Builder(getSupportActionBar().getThemedContext())
					.setTitle("Отметить модель как не прослушанную?")
					.setIcon(android.R.drawable.stat_sys_warning)
					.setPositiveButton("Да", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							LocalModel.setListened((int) id, false);
							listView.setAdapter(new HistoryAdapter(HistoryActivity.this, new ArrayList<>(LocalModel.getListened())));
						}
					})
					.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// nothing to do
						}
					}).show();

				return true;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();

		if (listView != null) {
			listView.invalidateViews();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}

		if (id == R.id.history_clear) {
			clearHistory();
		}

		return super.onOptionsItemSelected(item);
	}

	private void clearHistory() {
		if (adapter.size() == 0) {
			Toast.makeText(this, "Нечего удалять", Toast.LENGTH_SHORT).show();
			return;
		}
		new AlertDialog.Builder(this)
			.setTitle("Очистка истории")
			.setMessage(getResources().getString(R.string.dialog_clear_history))
			.setPositiveButton("Да", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					LocalModel.clearListened();
					listView.setAdapter(new HistoryAdapter(HistoryActivity.this, new ArrayList<>(LocalModel.getListened())));
				}
			}).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		}).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_history, menu);
		return true;
	}
}

package com.edwardstock.mds;

import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.edwardstock.mds.adapters.CatalogAdapter;
import com.edwardstock.mds.api.ApiClientSearchParams;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.RemoteModel;
import com.edwardstock.mds.threads.ListItemProgressTask;
import com.edwardstock.mds.threads.ModelLoaderTask;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.io.Serializable;
import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

import static android.widget.SearchView.OnQueryTextListener;

public class CatalogActivity extends BaseActivity implements ListView.OnScrollListener, CatalogAdapter.OnMenuItemClick {
	public static final String PARAM_SEARCH_TEXT = "SEARCH_TEXT";
	public static final String PARAM_OPEN_SORTER = "OPEN_SORTER";
	private static final String TAG = "CatalogActivity";
	public static ApiClientSearchParams additionalListParams;
	public static boolean finishOnCancelFilter = false;
	private static int pageNumber = 0;
	private static Spinner sortTypes;
	private static Spinner sortArrows;
	public ListState currentListState = ListState.DEFAULT;
	public CircularProgressBar progress;
	public ListView list;
	private boolean sorted = false;
	private SorterFragment sorterFragment;
	private ModelLoaderTask task;
	private CatalogAdapter adapter;

	private String searchQuery;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(adapter == null)
			return;

		outState.putParcelableArrayList("adapter", adapter.getValues());
		outState.putBoolean("sorted", sorted);
		outState.putParcelable("list_instance_state", list.onSaveInstanceState());
		outState.putSerializable("list_state", currentListState);

		int index = list.getFirstVisiblePosition();
		View v = list.getChildAt(0);
		int top = (v == null) ? 0 : v.getTop();

		outState.putInt("list_index_view", index);
		outState.putInt("list_top_view", top);

		if(searchQuery != null) {
			outState.putString("search_query", searchQuery);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.AppTheme);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.search_bar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		initMenu(toolbar, true);

		list = (ListView) findViewById(R.id.modelsListView);
		list.postInvalidateOnAnimation();

		progress = (CircularProgressBar) findViewById(R.id.listLoader);
		progress.setIndeterminate(true);

		Button refreshButton = (Button) findViewById(R.id.refresh);
		refreshButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (adapter != null) {
					adapter.clearItems();
					adapter.notifyDataSetChanged();
				}
				hideServiceIsUnavailable();
				loadList();
			}
		});

		sorterFragment = new SorterFragment();

		Bundle bundle = getIntent().getExtras();

		{
			if(savedInstanceState != null && savedInstanceState.containsKey("search_query")) {
				searchQuery = savedInstanceState.getString("search_query");
			}
		}

		if(savedInstanceState != null && savedInstanceState.containsKey("adapter")) {
			currentListState = (ListState) savedInstanceState.getSerializable("list_state");

			sorted = savedInstanceState.getBoolean("sorted");
			list.onRestoreInstanceState(savedInstanceState.getParcelable("list_instance_state"));
			ArrayList<RemoteModel> savedItems;
			savedItems =  savedInstanceState.getParcelableArrayList("adapter");
			adapter = new CatalogAdapter(this, savedItems, list);
			adapter.setOnMenuItemClickListener(this);

			list.setAdapter(adapter);
			list.setSelectionFromTop(savedInstanceState.getInt("list_index_view"),savedInstanceState.getInt("list_top_view"));
			progress.setVisibility(View.INVISIBLE);
		} else {
			if (bundle != null) {
				if (bundle.containsKey(PARAM_SEARCH_TEXT)) {
					String searchText = bundle.getString(PARAM_SEARCH_TEXT);
					loadList(searchText);
				}
			} else {
				loadList();
			}
		}

		list.setOnScrollListener(this);

		Log.d(TAG, "onCreate catalog");
	}

	private void loadList() {
		task = new ModelLoaderTask(this);
		task.execute(list);
	}

	private void loadList(String searchText) {
		task = new ModelLoaderTask(this, searchText);
		task.execute(list);
		currentListState = ListState.SEARCHING;

		if (task.isCancelled()) {
			Toast.makeText(this, "Ошибка подключения. Проверьте соединение с интернетом.", Toast.LENGTH_LONG).show();
			finish();
		}
	}

	@Override
	protected void onDestroy() {
		if (progress != null) {
			progress.setVisibility(View.INVISIBLE);
		}

		super.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "OnStart");
		if (list != null) {
			list.invalidateViews();
			list.invalidate();
		}

		if (adapter != null) {
			adapter.invalidateListenedItems();
		}
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	public boolean onCreateOptionsMenu(final Menu menu) {

		SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
		searchView.setQueryHint("Автор или название книги");
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String s) {
				if (TextUtils.isEmpty(s)) {
					loadList();
					CatalogActivity.this.searchQuery = null;
					currentListState = ListState.DEFAULT;
				} else {
					loadSearchList(s);
					CatalogActivity.this.searchQuery = s;
					currentListState = ListState.SEARCHING;
				}
				return true;
			}

			@Override
			public boolean onQueryTextChange(String s) {
				if (TextUtils.isEmpty(s)) {
					adapter = null;
					CatalogActivity.this.searchQuery = null;
					currentListState = ListState.DEFAULT;
					loadList();
				}
				return true;
			}
		});
		searchView.setOnCloseListener(new SearchView.OnCloseListener() {
			@Override
			public boolean onClose() {
				pageNumber = 0;
				currentListState = ListState.DEFAULT;
				loadList();

				return false;
			}
		});

		if(searchQuery != null) {
			Log.d(TAG, "Search query is set");
			searchView.setIconified(false);
			searchView.setQuery(searchQuery, false);
			searchView.clearFocus();
		}

		menu
			.add("Поиск")
			.setIcon(R.drawable.ic_search_white_48dp)
			.setActionView(searchView)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		menu
			.add("Фильтр")
			.setIcon(R.drawable.ic_action_sort_ascending)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem menuItem) {
					sorterFragment.show(getFragmentManager(), "searchDialog");
					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	private void loadSearchList(final String search) {
		task = new ModelLoaderTask(this, search);
		task.execute(list);
		if (task.isCancelled()) {
			Toast.makeText(this, "Ошибка подключения. Проверьте соединение с интернетом.", Toast.LENGTH_LONG).show();
			finish();
		}
	}

	public void onPostTaskExecute(ArrayList<RemoteModel> result) {
		if (result.size() == 0) {
			if (task != null) {
				task.cancel(true);
				task = null;
			}

			if(currentListState.equals(ListState.SEARCHING)) {
				Toast.makeText(this, "Ничего не найдено", Toast.LENGTH_LONG).show();
			}

			progress.setVisibility(View.INVISIBLE);
			findViewById(R.id.modelsListView).setVisibility(View.VISIBLE);
			return;
		}

		switch (currentListState) {
			case DEFAULT:
				if (adapter == null || sorted) {
					Log.d(TAG, "Adapter null? "+(adapter==null));
					Log.d(TAG, "Sorted? "+sorted);
					adapter = new CatalogAdapter(this, result, list);
					list.setAdapter(adapter);
				} else {
					adapter.appendPage(result);
					adapter.notifyDataSetChanged();
				}
				break;

			case SORTING:
				Log.d(TAG, "Already sorted? " + sorted);
				if (!sorted) {
					adapter = new CatalogAdapter(this, result, list);
					list.setAdapter(adapter);
					sorted = true;
				} else {
					if (adapter == null) {
						adapter = new CatalogAdapter(this, result, list);
					} else {
						adapter.appendPage(result);
						adapter.notifyDataSetChanged();
					}

				}
				break;
			case SEARCHING:
				adapter = new CatalogAdapter(this, result, list);
				list.setAdapter(adapter);
				break;
		}

		adapter.setOnMenuItemClickListener(this);

		progress.setVisibility(View.INVISIBLE);
		findViewById(R.id.modelsListView).setVisibility(View.VISIBLE);
	}

	public void onPreTaskExecute() {
		if (!this.serviceIsAvailable) {
			hideServiceIsUnavailable();
		}
		progress.setVisibility(View.VISIBLE);
		Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
		progress.startAnimation(fadeIn);
	}

	@Override
	public void onScrollStateChanged(AbsListView absListView, int scrollState) {
	}

	@Override
	public void onScroll(@NonNull AbsListView absListView, int firstVisible, int visibleCount, int totalCount) {

		boolean loadMore = (firstVisible + visibleCount >= totalCount);

		if (loadMore && (adapter != null && adapter.getTotalItemsCount() > totalCount)) {
			if (currentListState.equals(ListState.SORTING)) {
				loadListWithParams(++pageNumber);
			} else {
				loadList(++pageNumber);
			}
		}
	}

	private void loadListWithParams(int pageNumber) {
		task = new ModelLoaderTask(this, pageNumber, additionalListParams);
		task.execute(list);
	}

	private void loadList(int pageNumber) {
		task = new ModelLoaderTask(this, pageNumber);
		task.execute(list);
	}

	@Override
	public void onClick(final int position, final View anchor, final CatalogAdapter.ViewHolder holder, final View convertView) {
		if(adapter == null)
			return;

		final RemoteModel item = adapter.getItem(position);
		final PopupMenu popupMenu = new PopupMenu(this, anchor);
		popupMenu.getMenuInflater().inflate(R.menu.catalog_popup, popupMenu.getMenu());

		holder.popupMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				LocalModel model = LocalModel.createFromRemote(item);

				if (model.getIsDownloaded()) {
					popupMenu.getMenu().getItem(0).setEnabled(false);
				}
				if (model.getIsFavorite()) {
					popupMenu.getMenu().getItem(1).setTitle("Убрать из избранного");
				}
				if (model.getIsListened()) {
					popupMenu.getMenu().getItem(2).setTitle("Отметить как \"Не прослушанную\"");
				}
				if (model.hasBookmark()) {
					popupMenu.getMenu().getItem(3).setVisible(true);
				}
				popupMenu.show();
			}
		});

		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem menuItem) {
				final int id = menuItem.getItemId();

				LocalModel model = LocalModel.getByModelId(item.getId());
				switch (id) {
					case R.id.gotoBookmark:
						startPlayer(model, true);
						break;

					case R.id.download:
						model.downloadFile(CatalogActivity.this, DownloadsActivity.class, true);
						menuItem.setEnabled(false);
						startProgressAnimation(getViewByPosition(position));
						break;

					case R.id.add_to_favorite:
						if (model.getIsFavorite()) {
							model.isFavorite = 0;
							model.save();
							menuItem.setTitle("Добавить в избранное");

						} else {
							model.isFavorite = 1;
							model.save();
							menuItem.setTitle("Убрать из избранного");
						}

						holder.setItemIsFavorite(model.getIsFavorite());

						break;

					case R.id.add_to_listened:
						if (model.getIsListened()) {
							model.listened = 0;
							model.save();
							menuItem.setTitle("Отметить как \"Прослушанную\"");
						} else {
							model.listened = 1;
							model.save();
							menuItem.setTitle("Отметить как \"Не прослушанную\"");
							adapter.removeListenedItem(item, position);
						}

						holder.setItemIsListened(model.getIsListened());
						break;
				}
				return true;
			}
		});
	}

	private void startProgressAnimation(final View view) {
		final CatalogAdapter.ViewHolder holder = (CatalogAdapter.ViewHolder) view.getTag();

		ListItemProgressTask task = new ListItemProgressTask(this, holder.modelId);

		task.setEventListener(new ListItemProgressTask.ProgressEventListener() {
			@Override
			public void onUpdate(final int screenProgress, int screenTotalWidth, int progress, int total, int percentOfComplete) {

//                ResizeWidthAnimation progressAnimation = new ResizeWidthAnimation(holder.progressButton, screenProgress);
//                progressAnimation.setDuration(ListItemProgressTask.PROGRESS_INTERVAL);
//                holder.progressButton.startAnimation(progressAnimation);
//
//                if (i == 0) {
//                    view.invalidate();
//                }
//
//                i++;
			}

			@Override
			public void onComplete() {
//                holder.progressButton.startAnimation(fadeOut);
				holder.setItemIsDownloaded(true);
			}

			@Override
			public void onError() {
//                Log.d(TAG, "Progress animator: onError");
//                holder.progressButton.setBackgroundColor(0xCC3333);
//                try {
//                    Thread.sleep(300);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                } finally {
//                    holder.progressButton.startAnimation(fadeOut);
//                }
//                holder.progressButton.invalidate();
			}
		});

		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public View getViewByPosition(int position) {
		final int firstListItemPosition = list.getFirstVisiblePosition();
		final int lastListItemPosition = firstListItemPosition + list.getChildCount() - 1;

		if (position < firstListItemPosition || position > lastListItemPosition) {
			return list.getAdapter().getView(position, null, list);
		} else {
			final int childIndex = position - firstListItemPosition;
			return list.getChildAt(childIndex);
		}
	}

	public enum ListState implements Serializable {
		DEFAULT,
		SEARCHING,
		SORTING
	}

	/**
	 * Сортировщик
	 */
	public static class SorterFragment extends DialogFragment {

		@Override
		public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.fragment_search, container, false);

			getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

			sortTypes = (Spinner) view.findViewById(R.id.aSortType);
			sortArrows = (Spinner) view.findViewById(R.id.aSortArrow);
			Button searchSubmitButton = (Button) view.findViewById(R.id.aSubmit);
			Button searchCancelButton = (Button) view.findViewById(R.id.aCancel);

			ArrayAdapter<CharSequence> sortTypesAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.sort_types, android.R.layout.simple_spinner_item
			);
			ArrayAdapter<CharSequence> sortArrowsAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.sort_arrows, android.R.layout.simple_spinner_item
			);

			sortTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sortArrowsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			sortTypes.setAdapter(sortTypesAdapter);
			sortArrows.setAdapter(sortArrowsAdapter);

			searchSubmitButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

					additionalListParams = new ApiClientSearchParams(
						sortTypes.getFirstVisiblePosition(), sortArrows.getFirstVisiblePosition()
					);

					((CatalogActivity) getActivity()).loadListWithParams(pageNumber);
					((CatalogActivity) getActivity()).currentListState = ListState.SORTING;
					((CatalogActivity) getActivity()).sorted = false;
					dismiss();
				}
			});

			searchCancelButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					dismiss();
					if (finishOnCancelFilter) {
						getActivity().finish();
					}
				}
			});

			return view;
		}
	}
}

package com.edwardstock.mds.playmarket;


import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.edwardstock.mds.implementations.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Eduard Maksimovich edward.vstock@gmail.com
 */
public class ProductManager {

	private final static String TAG = "ProductManager";
	private final static String PAYLOAD = "bWRzY2x1YmluYXBwcHVyY2hhc2Vz"; // mdsclubinapppurchases

	private final static String REQ_ITEM_ID_LIST = "ITEM_ID_LIST";
	private final static String REQ_BUY_INTENT = "BUY_INTENT";
	private final static String RESP_RESPONSE_CODE = "RESPONSE_CODE";
	private final static String RESP_DETAILS_LIST = "DETAILS_LIST";

	private static ProductManager instance;

	private IInAppBillingService mService;
	private Context mContext;
	private BaseActivity mActivity;
	private ProductType mProductType = ProductType.IN_APP;
	private ProductItem[] mProductItems;
	private ArrayList<String> mPurchasedItems;

	private ProductManager(Context context, IInAppBillingService service) {
		mService = service;
		mContext = context;
		mActivity = ((BaseActivity) context);
	}

	public synchronized static ProductManager getInstance(Context context, IInAppBillingService service) {
		if (instance == null)
			instance = new ProductManager(context, service);

		return instance;
	}

	/**
	 * Проверяет, куплен ли уже товар или нет
	 *
	 * @param sku Артикул товара
	 * @return
	 */
	public boolean productIsBought(String sku) {
		try {
			return requestPurchases(true).contains(sku);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Запрашивает массив купленных артикулов
	 *
	 * @param fetchRemote Если false, то пытается использовать кэшированные данные
	 * @return
	 * @throws RemoteException
	 */
	public ArrayList<String> requestPurchases(boolean fetchRemote) throws RemoteException {
		if (!fetchRemote && mPurchasedItems.size() > 0) {
			return mPurchasedItems;
		}

		Bundle ownedItems = mService.getPurchases(3, mContext.getPackageName(), mProductType.toString(), null);
		int responseCode = ownedItems.getInt(RESP_RESPONSE_CODE);

		if (responseCode != 0) {
			throw new RemoteException();
		}

		ArrayList<String> ownerSku = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");

		mPurchasedItems = ownerSku;

		return ownerSku;
	}

	/**
	 * Проверяет, существует ли в магазине товар с указанным артикулом
	 *
	 * @param sku  Артикул искомого товара
	 * @param skus Артикулы всех товаров
	 * @return
	 */
	public boolean productExists(String sku, ArrayList<String> skus) {
		boolean exists = false;
		try {
			for (ProductItem item : requestProductsDetails(skus, false)) {
				if (item.getProductId().equals(sku)) {
					exists = true;
					break;
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return exists;
	}

	/**
	 * Возвращает массив товаров запрошенных в магазине
	 *
	 * @param skus        Артикулы для загрузки
	 * @param fetchRemote
	 * @return Массив товаров
	 * @throws RemoteException
	 * @TODO возвращать копию массива
	 */
	public ProductItem[] requestProductsDetails(ArrayList<String> skus, boolean fetchRemote) throws RemoteException {
		if (!fetchRemote && mProductItems.length > 0) {
			return mProductItems;
		}

		Bundle skuBundle = new Bundle();
		skuBundle.putStringArrayList(REQ_ITEM_ID_LIST, skus);
		Bundle result = mService.getSkuDetails(3, mContext.getPackageName(), mProductType.toString(), skuBundle);

		int responseCode = result.getInt(RESP_RESPONSE_CODE);

		if (responseCode != 0) {
			Log.e(TAG, "Error response from market. Code: " + responseCode);
			throw new RemoteException();
		}

		ArrayList<String> responseList = result.getStringArrayList(RESP_DETAILS_LIST);
		ProductItem[] productItems = new ProductItem[responseList.size()];

		int i = 0;
		for (String item : responseList) {
			try {
				JSONObject object = new JSONObject(item);
				productItems[i] = new ProductItem(
					object.getString(ProductItem.FIELD_PRODUCT_ID),
					object.getString(ProductItem.FIELD_PRODUCT_PRICE),
					object.getString(ProductItem.FIELD_PRODUCT_TYPE),
					object.getString(ProductItem.FIELD_PRODUCT_PRICE_AMOUNT_MICROS),
					object.getString(ProductItem.FIELD_PRODUCT_PRICE_CURRENCY_CODE),
					object.getString(ProductItem.FIELD_PRODUCT_TITLE),
					object.getString(ProductItem.FIELD_PRODUCT_DESCRIPTION)
				);
			} catch (JSONException e) {
				Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}

			i++;
		}

		mProductItems = productItems;

		return productItems;
	}

	/**
	 * Запрос на покупку товара (далее идет гугловская обработка и вызов фрагмента покупки)
	 *
	 * @param sku Артикул товара
	 * @throws RemoteException
	 * @throws IntentSender.SendIntentException
	 */
	public void buyItem(String sku) throws RemoteException, IntentSender.SendIntentException {
		Bundle buyIntentBundle = mService.getBuyIntent(
			3, //api version
			mContext.getPackageName(), //app package name
			sku, //item sku
			"inapp", // purchase type
			PAYLOAD //payload support
		);

		PendingIntent pendingIntent = buyIntentBundle.getParcelable(REQ_BUY_INTENT);

		mActivity.startIntentSenderForResult(
			pendingIntent.getIntentSender(),
			1001, //request code
			new Intent(),
			0, //flag mask
			0, //flag values
			0  //extra flags
		);
	}

	/**
	 * Ставит тип продукта.
	 * http://developer.android.com/google/play/billing/api.html#producttypes
	 *
	 * @param type Тип продукта
	 * @see com.edwardstock.mds.playmarket.ProductManager.ProductType
	 */
	public void setProductType(ProductType type) {
		mProductType = type;
	}

	public enum ProductType {
		/**
		 * Встроенные покупки
		 */
		IN_APP("inapp"),

		/**
		 * Подписка
		 */
		SUBSCRIPTION("subs");

		private final String name;

		ProductType(String s) {
			name = s;
		}

		public String toString() {
			return name;
		}

		public boolean equalsName(@NonNull String otherName) {
			return name.equals(otherName);
		}
	}

	public static class ProductItem {
		protected final static String FIELD_PRODUCT_ID = "productId";
		protected final static String FIELD_PRODUCT_PRICE = "price";
		protected final static String FIELD_PRODUCT_TYPE = "type";
		protected final static String FIELD_PRODUCT_PRICE_AMOUNT_MICROS = "price_amount_micros";
		protected final static String FIELD_PRODUCT_PRICE_CURRENCY_CODE = "price_currency_code";
		protected final static String FIELD_PRODUCT_TITLE = "title";
		protected final static String FIELD_PRODUCT_DESCRIPTION = "description";

		private String productId;
		private String productPrice;
		private String type;
		private String priceAmountMicros;
		private String priceCurrencyCode; //ISO 4217
		private String title;
		private String description;

		public ProductItem(String productId, String productPrice, String type, String priceAmountMicros, String priceCurrencyCode, String title, String description) {
			this.productId = productId;
			this.productPrice = productPrice;
			this.type = type;
			this.priceAmountMicros = priceAmountMicros;
			this.priceCurrencyCode = priceCurrencyCode;
			this.title = title;
			this.description = description;
		}

		public String getProductId() {
			return productId;
		}

		public String getProductPrice() {
			return productPrice;
		}

		public String getType() {
			return type;
		}

		public String getPriceAmountMicros() {
			return priceAmountMicros;
		}

		public String getPriceCurrencyCode() {
			return priceCurrencyCode;
		}

		public String getTitle() {
			return title;
		}

		public String getDescription() {
			return description;
		}
	}


}

package com.edwardstock.mds.implementations;

import android.widget.BaseAdapter;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;


abstract public class BaseListActivity<AdapterType extends BaseAdapter, ListType> extends BaseActivity {

	private AdapterType adapter;

	private ListView listView;

	@Override
	protected void onResume() {
		super.onResume();
		if (listView != null && adapter != null) {
			listView.invalidateViews();
			adapter.notifyDataSetInvalidated();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	protected abstract void initList(ListType items);

	protected AdapterType getAdapter() {
		return this.adapter;
	}

	protected void setAdapter(AdapterType adapter) {
		this.adapter = adapter;
	}

	protected ListView getListView() {
		return this.listView;
	}

	protected void setListView(ListView lv) {
		this.listView = lv;
	}
}

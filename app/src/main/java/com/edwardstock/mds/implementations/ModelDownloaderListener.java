package com.edwardstock.mds.implementations;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.edwardstock.mds.DownloadsActivity;
import com.edwardstock.mds.helpers.HttpHelper;
import com.edwardstock.mds.helpers.ProgressProvider;
import com.edwardstock.mds.helpers.downloader.Downloader;
import com.edwardstock.mds.models.LocalModel;

import java.io.File;
import java.io.IOException;

public class ModelDownloaderListener implements Downloader.OnDownloadEventListener, Downloader.OnDownloadEventListener.Validator {

	private final static String TAG = "ModelDownloaderListener";

	private Context context;
	private NotificationManager notificationManager;
	private NotificationCompat.Builder builder;
	private Downloader.OnDownloadEventListener eventListener;
	private LocalModel model;
	private File file;
	private int notifierId = -1;
	private boolean saveInDB = false;

	private int totalSize = 0;
	private int progressSize = 0;

	public ModelDownloaderListener(Context context, LocalModel model) {
		this.context = context;
		this.model = model;
	}

	@Override
	public boolean validate() {
		return
				context != null &&
						notificationManager != null &&
						builder != null &&
						model != null &&
						file != null &&
						notifierId >= 0;
	}

	@Override
	public Downloader.OnDownloadEventListener getListener() {
		return this;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void setNotificationManager(NotificationManager notificationManager) {
		this.notificationManager = notificationManager;
	}

	public void setBuilder(NotificationCompat.Builder builder) {
		this.builder = builder;
	}

	public void setEventListener(Downloader.OnDownloadEventListener eventListener) {
		this.eventListener = eventListener;
	}

	public void setNotifierId(int notifierId) {
		this.notifierId = notifierId;
	}

	public void setSaveInDB(boolean saveInDB) {
		this.saveInDB = saveInDB;
	}

	@Override
	public void onUpdate(int processedSize, int totalSize) {
		this.progressSize = processedSize;
		this.totalSize = totalSize;
		builder.setProgress(totalSize, processedSize, false);
		notificationManager.notify(notifierId, builder.build());

		ProgressProvider.get().setProgress(model.getModelId(), processedSize, totalSize);

		if (eventListener != null) {
			eventListener.onUpdate(processedSize, totalSize);
		}
	}

	@Override
	public void onComplete() {
		notificationManager.notify(
				notifierId, getCompleteNotificationBuilder(context, builder).build()
		);

		ProgressProvider nu = ProgressProvider.get();
		nu.setProgress(model.getModelId(), totalSize, totalSize);

		if (saveInDB) {
			model.fileDownloaded = LocalModel.MODEL_DOWNLOADED;
			model.filename = file.getAbsolutePath();
			model.save();
		}

		if (eventListener != null) {
			eventListener.onComplete();
		}
	}

	@Override
	public void onError(IOException err) {
		notificationManager.notify(notifierId, getErrorNotificationBuilder(builder, context).build());
		ProgressProvider.get().getProxy(model.getModelId()).setErrorOccurred();
		if (eventListener != null) {
			eventListener.onError(err);
		}
	}

	private NotificationCompat.Builder getErrorNotificationBuilder(NotificationCompat.Builder builder, Context context) {
		boolean internetIsEnabled = HttpHelper.isNetworkAvailable(context);

		builder.setTicker("Ошибка загрузки модели");
		builder.setSmallIcon(android.R.drawable.stat_sys_download_done);
		builder.setContentTitle("Ошибка загрузки");

		if (internetIsEnabled) {
			builder.setContentText("Произошла ошибка во время загрузки модели. ");
		} else {
			builder.setContentText("Произошла ошибка во время загрузки модели.");
			builder.setSubText("У вас отключен интернет");
		}


		builder.setContentIntent(getErrorContentIntent(context));
		builder.setProgress(0, 0, true);
		builder.setOngoing(false);

		return builder;
	}

	private PendingIntent getErrorContentIntent(Context context) {
		Intent errorIntent = new Intent(context, DownloadsActivity.class);
		errorIntent.putExtra(DownloadsActivity.PARAM_DOWNLOAD_MODEL_ID, model.getModelId());
		return PendingIntent.getActivity(context, 0, errorIntent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	private NotificationCompat.Builder getCompleteNotificationBuilder(Context context, NotificationCompat.Builder builder) {
		builder.setProgress(0, 0, false);
		builder.setTicker("Модель теперь доступна оффлайн");
		builder.setContentText("Загрузка модели завершена!");
		builder.setSmallIcon(android.R.drawable.stat_sys_download_done);
		builder.setOngoing(false);
		builder.setContentIntent(getCompleteContentIntent(context));

		return builder;
	}

	private PendingIntent getCompleteContentIntent(Context context) {
		Intent completeIntent = new Intent(context, DownloadsActivity.class);
		return PendingIntent.getActivity(context, 0, completeIntent, PendingIntent.FLAG_CANCEL_CURRENT);
	}
}

package com.edwardstock.mds.implementations;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardstock.mds.BookmarksActivity;
import com.edwardstock.mds.CatalogActivity;
import com.edwardstock.mds.DownloadsActivity;
import com.edwardstock.mds.FavoritesActivity;
import com.edwardstock.mds.HistoryActivity;
import com.edwardstock.mds.PlayerActivity;
import com.edwardstock.mds.PlaylistsActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.SettingsActivity;
import com.edwardstock.mds.SyncActivity;
import com.edwardstock.mds.adapters.MenuAdapter;
import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.api.SyncClient;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.services.PlayerService;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

abstract public class BaseActivity extends AppCompatActivity implements PlayerService.ViewAppearanceListener {
	private final static String TAG = "BaseActivity (BASE_ACTIVITY)";
	public static long playlistId = -1;
	// Плеер основные данные
	public static int modelId;
	public static LocalModel model;
	public static PlayerService service;
	public static volatile boolean isPlaying = false;
	//===============================
	public static volatile boolean isViewOn = false;
	// Очередь воспроизвдения
	protected static ArrayList<Integer> queueItems = new ArrayList<>();
	//===============================
	protected static int queuePosition = 0;
	protected static int lastPosition = 0;
	protected static int lastDuration = 0;
	private static boolean queueHasNext = false;
	private static boolean queueHasPrev = false;
	// Все для серсива воспроизведенния
	public Intent intent;
	//===============================
	public ServiceConnection serviceConnection;
	protected boolean isBound = false;
	protected boolean serviceIsAvailable = true;
	protected DrawerLayout drawerLayout;
	private TextView playTitle;
	private TextView playAuthor;
	private RelativeLayout playingNowLayout;
	private int lastPlaybackId = 0;
	private ActionBarDrawerToggle drawerToggle;

	public static int getQueuePosition() {
		return queuePosition;
	}

	public static void incrementQueue() {
		queuePosition++;
	}

	public static void decrementQueue() {
		queuePosition--;
	}

	public static void clearQueue() {
		queueItems = new ArrayList<Integer>();
		playlistId = -1;
		queuePosition = 0;
	}
	
	public static boolean queueExists() {
		return queueItems.size() > 0;
	}
	
	public static boolean isQueueHasNextTrack() {
		validateQueueArrows();
		return queueHasNext;
	}
	
	public static void validateQueueArrows() {
		queueHasNext = (queuePosition < (queueItems.size()-1));
		queueHasPrev = (queuePosition > 0 && (queueItems.size()) > queuePosition);
	}
	
	public static boolean isQueueHasPrevTrack() {
		validateQueueArrows();
		return queueHasPrev;
	}
	
	public static int getNextQueueId() {
		validateQueueArrows();
		if(!queueHasNext)
			throw new IndexOutOfBoundsException("Out of queue");
		
		return queueItems.get(queuePosition+1);
	}
	
	public static int getPrevQueueId() {
		validateQueueArrows();
		if(!queueHasPrev)
			throw new IndexOutOfBoundsException("Out of queue");
		
		return queueItems.get(queuePosition-1);
	}


	protected void sendGAScreen() {
		Tracker t = ((MDSApplication) this.getApplication())
			.getTracker(MDSApplication.TrackerName.GLOBAL_TRACKER);
		t.setScreenName(getClass().getSimpleName());
		t.send(new HitBuilders.ScreenViewBuilder().build());
	}

	public void startPlayerQueue(long playlistId, int startPosition, int modelId) {
		final Intent intent = new Intent(this, PlayerActivity.class);
		intent.putExtra(PlayerActivity.PARAM_MODEL_ID, modelId);
		intent.putExtra(PlayerActivity.PARAM_QUEUE_ID, playlistId);
		intent.putExtra(PlayerActivity.PARAM_QUEUE_POSITION, startPosition);

		startActivity(intent);
	}

	public void startPlayer(LocalModel model, boolean fromLastPosition) {
		int startTime = fromLastPosition ? model.bookmarkPosition : 0;
		int duration = Integer.parseInt(model.getDuration());
		startPlayer(
			model.getModelId(),
			startTime,
			duration,
			model.getTitle(),
			model.getAuthor()
		);
	}

	public void startPlayer(int modelId, int lastPosition, int lastDuration, String title, String author) {
		final Intent intent = new Intent(this, PlayerActivity.class);
		intent.putExtra(PlayerActivity.PARAM_MODEL_ID, modelId);
		intent.putExtra(PlayerActivity.PARAM_AUTHOR, author);
		intent.putExtra(PlayerActivity.PARAM_TITLE, title);
		intent.putExtra(PlayerActivity.PARAM_START_FROM_TIME_POSITION, lastPosition);
		intent.putExtra(PlayerActivity.PARAM_START_FROM_TIME_DURATION, lastDuration);

		startActivity(intent);
	}

	public void startPlayer(int modelId) {
		startPlayer(modelId, 0, 0, null, null);
	}

	public void startPlayer(int modelId, String title, String author) {
		startPlayer(modelId, 0, 0, title, author);
	}

	/**
	 * Отображает описание кнопки на которой нету действия
	 *
	 * @param view
	 */
	public void showButtonDescription(View view) {
		if (view.getContentDescription().length() > 0) {
			Toast.makeText(this, view.getContentDescription(), Toast.LENGTH_SHORT).show();
		}
	}

	public void openCurrentPlayer(View view) {
		final Intent i = new Intent(this, PlayerActivity.class);
		i.putExtra(PlayerActivity.PARAM_MODEL_ID, BaseActivity.model.getModelId());
		i.putExtra(PlayerActivity.PARAM_AUTHOR, model.getAuthor());
		i.putExtra(PlayerActivity.PARAM_TITLE, model.getTitle());
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(i);
	}

	/**
	 * Меню ПОПУЛЯРНЫЕ МОДЕЛИ (в разработке)
	 *
	 * @param view
	 */
	public void openPopular(View view) {
	}

	public void syncData() {
		new AsyncTask<Void, Void, Void>() {
			private String errorMessage = null;

			@Override
			protected Void doInBackground(Void... params) {
				SyncClient client = new SyncClient(BaseActivity.this);
				try {
					client.syncWithServer();
				} catch (IOException e) {
					e.printStackTrace();
					errorMessage = e.getMessage();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void res) {
				super.onPostExecute(res);
				if (errorMessage != null) {
					Toast.makeText(BaseActivity.this, errorMessage, Toast.LENGTH_LONG).show();
				}
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	protected Toolbar findAndSetSupportActionBar(int resId) {
		Toolbar toolbar = (Toolbar) findViewById(resId);
		setSupportActionBar(toolbar);

		return toolbar;
	}

	/**
	 * Показывает в списке нотайс о том что сервис временно не доступен (например сервер не отвечает)
	 */
	public void showServiceIsUnavailable() {
		findViewById(R.id.refresh).setVisibility(View.VISIBLE);
		findViewById(R.id.serviceUnavailable).setVisibility(View.VISIBLE);
		findViewById(R.id.modelsListView).setVisibility(View.GONE);
		this.serviceIsAvailable = false;
	}

	public void hideServiceIsUnavailable() {
		findViewById(R.id.refresh).setVisibility(View.INVISIBLE);
		findViewById(R.id.serviceUnavailable).setVisibility(View.INVISIBLE);
		findViewById(R.id.modelsListView).setVisibility(View.VISIBLE);
		this.serviceIsAvailable = true;
	}

	protected void unBindService() {
		createServiceConnection();
		if (isBound) {
			unbindService(serviceConnection);
			isBound = false;
			Log.d(TAG, "__UNBIND SERVICE");
		}
	}

	protected void createServiceConnection() {
		if (serviceConnection != null) {
			return;
		}

		serviceConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
				Log.d(TAG, "onServiceConnected()");
				isBound = true;
				service = ((PlayerService.PlayerBinder) iBinder).getService();

				afterServiceConnection(service);
			}

			@Override
			public void onServiceDisconnected(ComponentName componentName) {
				Log.d(TAG, "onServiceDisconnected()");
				isBound = false;
			}
		};
	}

	protected void afterServiceConnection(PlayerService service) {

	}

	protected void bindPlayerService() {
		createServiceIntent();
		createServiceConnection();
		if (!isBound) {
			bindService(intent, serviceConnection, 0);
			isBound = true;
			Log.d(TAG, "__BIND SERVICE");
		}
	}

	protected void createServiceIntent() {
		if (intent == null)
			intent = new Intent(this, PlayerService.class);
	}

	/**
	 * Возвращаем состояние плеер (отображается или не отображается),
	 * для того чтобы зря не запускать потоки
	 *
	 * @return boolean
	 */
	@Override
	public boolean getIsViewOn() {
		return isViewOn;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle != null && drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (drawerToggle != null)
			drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (drawerToggle != null)
			drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		FragmentManager manager = getFragmentManager();
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		} else if (manager.getBackStackEntryCount() > 0) {
			manager.popBackStack();
			manager.beginTransaction().commit();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onStart() {
		Log.d(TAG, "onStart()");
		try {
			if (service != null && service.getMediaPlayer().isPlaying() && !getClass().equals(SettingsActivity.class)) {
				showPlaybackNow();
			}
		} catch (IllegalStateException err) {
			hidePlaybackNow();
			Log.d(TAG, "MediaPlayer produced IllegalStateException. Layout \"playback now\" hiding");
		}

		super.onStart();
	}

	public void showPlaybackNow() {
		initPlaybackNowViews();

		if (lastPlaybackId == model.getModelId())
			return;

		playTitle.setText(model.getTitle());
		playAuthor.setText(model.getAuthor());
		playingNowLayout.setVisibility(View.VISIBLE);
		lastPlaybackId = model.getModelId();
	}

	public void hidePlaybackNow() {
		initPlaybackNowViews();
		playingNowLayout.setVisibility(View.INVISIBLE);
	}

	private void initPlaybackNowViews() {
		if (playingNowLayout == null) {
			playingNowLayout = (RelativeLayout) findViewById(R.id.playbackNow);
		}

		if (playTitle == null)
			playTitle = (TextView) playingNowLayout.findViewById(R.id.playTitle);

		if (playAuthor == null)
			playAuthor = (TextView) playingNowLayout.findViewById(R.id.playAuthor);
	}

	protected void initMenu(Toolbar toolbar, boolean showMenuIcon) {
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		RelativeLayout menuDrawer = (RelativeLayout) findViewById(R.id.menu_drawer);
		ListView drawerList = (ListView) menuDrawer.findViewById(R.id.menuListView);

		MenuAdapter menuAdapter = new MenuAdapter(this, initMenuItems());

		drawerList.setAdapter(menuAdapter);

		if (showMenuIcon) {
			drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
			drawerLayout.setDrawerListener(drawerToggle);
		}
	}

	private ArrayList<MenuAdapter.MenuItem> initMenuItems() {
		ArrayList<MenuAdapter.MenuItem> items = new ArrayList<>();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(BaseActivity.this);

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_opened_dir, getString(R.string.sm_catalog), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openCatalog(v);
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_action_folder_plus_white_48dp, getString(R.string.sm_playlists), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openPlaylists();
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_cloud_done_white_48dp, getString(R.string.sm_downloads), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openDownloads(v);
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_grade_white_48dp, getString(R.string.sm_favorites), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openFavorites(v);
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_history_white_48dp, getString(R.string.sm_history), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openHistory(v);
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_bookmark_play, getString(R.string.sm_bookmarks), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openBookmarks(v);
			}
		}));

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_action_shuffle_vectorized, getString(R.string.menu_random_model), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openRandom(v);
			}
		}));

		if(SyncClient.userAuthenticated(this)) {
			items.add(new MenuAdapter.MenuItem(R.drawable.ic_sync_white_48dp, getString(R.string.sync_on), new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					openSync();
				}
			}));
		}

		boolean appRated = sp.getBoolean("application_rated", false);
		if (!appRated) {
			items.add(new MenuAdapter.MenuItem(R.drawable.ic_like_filled, getString(R.string.sm_rate_app), new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showRateDialog();
				}
			}));
		}

		items.add(new MenuAdapter.MenuItem(R.drawable.ic_alien, getString(R.string.open_donate), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openDonate(v);
			}
		}));


		items.add(new MenuAdapter.MenuItem(R.drawable.ic_settings, getString(R.string.sm_settings), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openSettings(v);
			}
		}));


		return items;
	}

	/**
	 * Меню КАТАЛОГ
	 *
	 * @param view
	 */
	public void openCatalog(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		Intent intent = new Intent(this, CatalogActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	public void openPlaylists() {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		startActivity(new Intent(this, PlaylistsActivity.class));
	}

	/**
	 * Меню ЗАГРУЗКИ
	 *
	 * @param view
	 */
	public void openDownloads(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		Intent intent = new Intent(this, DownloadsActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	/**
	 * Меню ИЗБРАННОЕ
	 *
	 * @param view
	 */
	public void openFavorites(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		Intent intent = new Intent(this, FavoritesActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	public void openHistory(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}

		Intent intent = new Intent(this, HistoryActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
	}

	private void openBookmarks(View v) {
		startActivity(new Intent(this, BookmarksActivity.class));
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
	}

	public void openRandom(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}

		try {
			Intent intent = new Intent(BaseActivity.this, PlayerActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra(PlayerActivity.PARAM_MODEL_ID, ApiClient.getRandomId());
			startActivity(intent);
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void openSync() {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		startActivity(new Intent(this, SyncActivity.class));
	}

	protected void showRateDialog() {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}

		AlertDialog.Builder ad = new AlertDialog.Builder(this, R.style.AppTheme);
		ad.setTitle("Оцените приложение");
		ad.setMessage(R.string.rate_me);
		ad.setPositiveButton("Хочу оценить!", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("market://details?id=com.edwardstock.mds"));
				startActivity(i);
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(BaseActivity.this);
				sp.edit().putBoolean("application_rated", true).apply();
			}
		});

		ad.setNegativeButton("Позже", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) { /*nothing*/ }
		});
		ad.show();
	}

	public void openDonate(View view) {
		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
		Intent i = new Intent(this, SettingsActivity.class);
		i.putExtra("PREF_KEY", "help_types");
		startActivity(i);
	}

	/**
	 * Меню НАСТРОЙКИ
	 *
	 * @param view
	 */
	public void openSettings(View view) {
		startActivity(new Intent(this, SettingsActivity.class));

		if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
			drawerLayout.closeDrawers();
		}
	}

	public void openCreatePlaylistDialog(View v) {
		showCreatePlaylistDialog();
	}

	public void showCreatePlaylistDialog() {
		showCreatePlaylistDialog(null);
	}

	public void showCreatePlaylistDialog(final Playlist.PlaylistDialogEventListener listener) {
		AlertDialog.Builder ad = new AlertDialog.Builder(this, R.style.AppTheme);
		ad.setMessage(getString(R.string.create_playlist));
		final EditText input = new EditText(getSupportActionBar().getThemedContext());
		ad.setView(input);
		ad.setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String title = input.getText().toString().trim();
				Playlist.create(title);
				if (listener != null) {
					listener.onPositive();
				}
			}
		});

		ad.setNegativeButton(R.string.cancelSearch, null);
		ad.show();
	}

}

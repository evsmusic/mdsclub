package com.edwardstock.mds.implementations;

import com.activeandroid.app.Application;
import com.edwardstock.mds.BuildConfig;
import com.edwardstock.mds.R;
import com.edwardstock.mds.api.ApiClient;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.HashMap;

@ReportsCrashes(
	formKey = "", // This is required for backward compatibility but not used
	formUri = ApiClient.REPORTER_URL,
	httpMethod = org.acra.sender.HttpSender.Method.POST,
	resToastText = R.string.crash_toast_text,
	mode = ReportingInteractionMode.TOAST,
	logcatFilterByPid = true,
	customReportContent = {
		ReportField.APP_VERSION_NAME,
		ReportField.ANDROID_VERSION,
		ReportField.PHONE_MODEL,
		ReportField.CUSTOM_DATA,
		ReportField.STACK_TRACE,
		ReportField.LOGCAT,
		ReportField.INITIAL_CONFIGURATION,
		ReportField.CRASH_CONFIGURATION,
		ReportField.SETTINGS_SYSTEM,
		ReportField.ENVIRONMENT,
		ReportField.AVAILABLE_MEM_SIZE,
		ReportField.TOTAL_MEM_SIZE,
		ReportField.DISPLAY,
		ReportField.SHARED_PREFERENCES
	}
)
public class MDSApplication extends Application {

	private static final String PROPERTY_ID = "UA-16000575-5";
	HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t;
			switch (trackerId){
				case APP_TRACKER:
					t = analytics.newTracker(PROPERTY_ID);
					break;
				case GLOBAL_TRACKER:
					t = analytics.newTracker(R.xml.global_tracker);
					break;

				default:
					t = analytics.newTracker(PROPERTY_ID);break;
			}

			mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}

	@Override
	public void onCreate() {
		super.onCreate();


		if(!BuildConfig.PRODUCTION_MODE) {
			GoogleAnalytics ga = GoogleAnalytics.getInstance(this);
			ga.setDryRun(true);
			ga.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
		}


		if(BuildConfig.PRODUCTION_MODE) {
			ACRA.init(this);
		}


	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	/**
	 * Enum used to identify the tracker that needs to be used for tracking.
	 *
	 * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
	 * storing them all in Application object helps ensure that they are created only once per
	 * application instance.
	 */
	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
	}
}

package com.edwardstock.mds.implementations;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.query.Select;

import java.util.List;

public class ActiveRecordModel extends Model {
	
	public static <T extends Model> void deleteItem(Class<T> modelClass, long id) {
		T model = new Select().from(modelClass).where("Id = ?", id).executeSingle();
		if(model != null)
			model.delete();
	}

	public static <T extends Model> T findByPk(Class<T> modelClass, long id) {
		return new Select().from(modelClass).where("Id = ?", id).executeSingle();
	}
	
	public static <T extends Model> List<T> findAll(Class<T> modelClass) {
		return new Select().from(modelClass).execute();
	}
	
	public static <T extends Model> boolean exists(Class<T> modelClass, long id) {
		return new Select().from(modelClass).where("Id = ?", id).exists();
	}

	public static <T extends Model> int countAll(Class<T> modelClass) {
		return new Select().from(modelClass).count();
	}

	protected <T extends Model> T belongsTo(Class<T> type, long value) {
		return new Select()
			.from(type)
			.where(Cache.getTableName(type) + ".Id=?", value)
			.executeSingle();
	}

}

package com.edwardstock.mds.models;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.edwardstock.mds.helpers.ChecksumHelper;
import com.edwardstock.mds.implementations.ActiveRecordModel;

import java.util.List;

@Table(name = "playlist_item")
public class PlaylistItem extends ActiveRecordModel {

	@Column(name = "playlistId", index = true)
	public long playlistId;

	@Column(name = "modelId", index = true)
	public int modelId;

	public static PlaylistItem createFromRemote(RemotePlaylist.RemotePlaylistItem remotePlaylistItem) {
		PlaylistItem playlistItem = new PlaylistItem();
		playlistItem.playlistId = remotePlaylistItem.getPlaylistId();
		playlistItem.modelId = remotePlaylistItem.getModelId();
		playlistItem.save();

		return playlistItem;
	}

	public static PlaylistItem create(int playlistId, int modelId) {
		PlaylistItem item = new PlaylistItem();
		item.playlistId = playlistId;
		item.modelId = modelId;
		item.save();

		return item;
	}

	public static List<LocalModel> getModels(long playlistId) {
		return new
			Select()
			.from(LocalModel.class)
			.innerJoin(PlaylistItem.class).on("playlist_item.modelId = local_model.modelId")
			.where("playlist_item.playlistId = ?", playlistId)
			.execute();
	}

	public static long findIdByPlaylistModel(long playlistId, int modelId) {
		PlaylistItem item = new Select()
			.from(PlaylistItem.class)
			.where("playlistId = ? AND modelId = ?", playlistId, modelId)
			.executeSingle();

		return item.getId();
	}

	public static PlaylistItem findByPlaylistModel(long playlistId, int modelId) {
		return new Select().from(PlaylistItem.class)
			.where("playlistId = ? AND modelId = ?", playlistId, modelId)
			.executeSingle();
	}

	public Playlist getPlaylist() {
		return belongsTo(Playlist.class, playlistId);
	}

	public long getHashSum() {
		return ChecksumHelper.getCRC32(playlistId, modelId);
	}

}

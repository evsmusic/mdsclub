package com.edwardstock.mds.models;


import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.edwardstock.mds.implementations.ActiveRecordModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Table(name = "playlist")
public class Playlist extends ActiveRecordModel implements Serializable {

	@Column(name = "title")
	public String title;

	public static Playlist createFromRemote(RemotePlaylist remotePlaylist) {
		Playlist playlist = new Playlist();
		playlist.title = remotePlaylist.getTitle();
		playlist.save();

		for (RemotePlaylist.RemotePlaylistItem remoteItem : remotePlaylist.getItems()) {
			PlaylistItem item = new PlaylistItem();
			item.modelId = remoteItem.getModelId();
			item.playlistId = playlist.getId();
			item.save();
		}

		return playlist;
	}

	public static int countItems(Playlist playlist) {
		return countItems(playlist.getId());
	}

	public static int countItems(long playlistId) {
		return new Select()
			.from(PlaylistItem.class)
			.where("playlistId = ?", playlistId)
			.count();
	}

	@Deprecated
	public static List<Playlist> getAll() {
		return new Select().from(Playlist.class).execute();
	}

	public static void create(String title) {
		Playlist item = new Playlist();
		item.title = title;
		item.save();
	}

	public static Playlist findByPk(long id) {
		return new Select().from(Playlist.class).where("Id = ?", id).executeSingle();
	}

	public static List<Playlist> findAll() {
		return new Select().from(Playlist.class).orderBy("Id DESC").execute();
	}

	public static List<Playlist> findAll(int excludeModelId) {
		return new
			Select()
			.from(Playlist.class)
			.leftJoin(PlaylistItem.class).on("playlist_item.playlistId = playlist.Id")
			.where("playlist_item.modelId != ?", excludeModelId)
			.orderBy("Id DESC")

			.execute();
	}

	public static void deleteItems(Playlist item) {
	}

	public ArrayList<PlaylistItem> getItems() {
		return new ArrayList<>(getMany(PlaylistItem.class, "playlistId"));
	}

	public interface PlaylistDialogEventListener {
		void onPositive();

		void onNegative();
	}


}

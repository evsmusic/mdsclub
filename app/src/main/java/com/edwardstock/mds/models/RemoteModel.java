package com.edwardstock.mds.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.helpers.ProgressProvider;
import com.edwardstock.mds.helpers.TimeHelper;

import org.json.simple.JSONObject;

public class RemoteModel implements Parcelable {

	public static final Parcelable.Creator<RemoteModel> CREATOR = new Parcelable.Creator<RemoteModel>() {
		public RemoteModel createFromParcel(@NonNull Parcel in) {
			return new RemoteModel(in);
		}

		@NonNull
		public RemoteModel[] newArray(int size) {
			return new RemoteModel[size];
		}
	};

	private int id;
	private int itemsCount;
	private int pagesCount;
	private long size;
	private String author;
	private String date;
	private String duration;
	private String radio;
	private String humanSize;
	private String title;
	private String url;
	private int previousModelId;
	private int nextModelId;
	private LocalModel model;

	public RemoteModel(JSONObject obj) {
		this.id = Integer.valueOf(obj.get(ApiClient.FIELD_ID).toString());
		this.author = (String) obj.get(ApiClient.FIELD_AUTHOR);
		this.date = (String) obj.get(ApiClient.FIELD_DATE);
		this.duration = obj.get(ApiClient.FIELD_DURATION).toString();
		this.radio = (String) obj.get(ApiClient.FIELD_RADIO);
		this.size = Long.valueOf(obj.get(ApiClient.FIELD_SIZE).toString());
		this.humanSize = (String) obj.get(ApiClient.FIELD_SIZE_HUMAN_READABLE);
		this.title = (String) obj.get(ApiClient.FIELD_TITLE);
		this.url = (String) obj.get(ApiClient.FIELD_URL);
		this.itemsCount = Integer.valueOf(obj.get(ApiClient.FIELD_TOTAL_ITEMS).toString());
		this.pagesCount = Integer.valueOf(obj.get(ApiClient.FIELD_PAGES_COUNT).toString());
		this.previousModelId = Integer.valueOf(obj.get(ApiClient.FIELD_PREVIOUS_MODEL_ID).toString());
		this.nextModelId = Integer.valueOf(obj.get(ApiClient.FIELD_NEXT_MODEL_ID).toString());
	}

	public RemoteModel(Parcel in) {
		this.id = in.readInt();
		this.author = in.readString();
		this.date = in.readString();
		this.duration = in.readString();
		this.size = in.readInt();
		this.humanSize = in.readString();
		this.title = in.readString();
		this.url = in.readString();
		this.itemsCount = in.readInt();
		this.pagesCount = in.readInt();
		this.previousModelId = in.readInt();
		this.nextModelId = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(@NonNull Parcel destiny, int flags) {
		destiny.writeInt(this.id);
		destiny.writeString(this.author);
		destiny.writeString(this.date);
		destiny.writeString(this.duration);
		destiny.writeLong(this.size);
		destiny.writeString(this.humanSize);
		destiny.writeString(this.title);
		destiny.writeString(this.url);
		destiny.writeInt(this.itemsCount);
		destiny.writeInt(this.pagesCount);
		destiny.writeInt(this.previousModelId);
		destiny.writeInt(this.nextModelId);
	}

	public int getItemsCount() {
		return itemsCount;
	}

	public int getPagesCount() {
		return pagesCount;
	}

	public String getSource() {
		return url;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getDuration() {
		return TimeHelper.getTimeString(getIntDuration());
	}

	public int getIntDuration() {
		return Integer.valueOf(duration);
	}

	public String getRadio() {
		return radio;
	}

	public long getSize() {
		return size;
	}

	public String getHumanSize() {
		return humanSize;
	}

	public String getDate() {
		return date;
	}

	public int getNextModelId() {
		return nextModelId;
	}

	public int getPreviousModelId() {
		return previousModelId;
	}

	public void fetchLocalInfo() {
		if (this.model == null)
			this.model = LocalModel.getByModelId(this.getId());
	}

	public int getId() {
		return id;
	}

	public void clearLocalInfo() {
		this.model = null;
	}

	public boolean isDownloaded() {
		return model != null && model.getIsDownloaded();
	}

	public boolean isInFavorites() {
		return model != null && model.getIsFavorite();
	}

	public boolean isListened() {
		return model != null && model.getIsListened();
	}

	public boolean hasBookmark() {
		return model != null && model.hasBookmark();
	}

	public ProgressProvider.Proxy getProgressProxy() {
		return ProgressProvider.get().getProxy(this.getId());
	}

	public class Comment {
		private int id;
		private String title;
		private String author;

	}


}

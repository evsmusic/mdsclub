package com.edwardstock.mds.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RemotePlaylist implements Serializable {

	private final static String TAG = "RemotePlaylist";

	private long id;
	private String title;
	private List<RemotePlaylistItem> remotePlaylistItemList;

	public RemotePlaylist(JSONObject remote) throws JSONException {
		this.id = remote.getLong("id");
		this.title = remote.getString("title");

		JSONArray plItems = remote.getJSONArray("items");
		remotePlaylistItemList = new ArrayList<>(plItems.length());
		for (int i = 0; i < plItems.length(); i++) {
			remotePlaylistItemList.add(new RemotePlaylistItem(
				plItems.getJSONObject(i).getInt("model_id"),
				plItems.getJSONObject(i).getInt("playlist_id")
			));
		}
	}

	public boolean hasLocalCopy() {
		Playlist item = Playlist.findByPk(this.id);
		Log.d(TAG, "Playlist has local copy: " + (!(item == null || !item.title.equals(this.title))));
		Log.d(TAG, "Playlist id: " + getId());
		Log.d(TAG, "Playlist title: " + getTitle());
		return !(item == null || !item.title.equals(this.title));
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public Playlist createLocalCopy() {
		Playlist playlist = new Playlist();
		playlist.title = getTitle();
		playlist.save();

		for (RemotePlaylist.RemotePlaylistItem remoteItem : getItems()) {
			PlaylistItem item = new PlaylistItem();
			item.modelId = remoteItem.getModelId();
			item.playlistId = playlist.getId();
			item.save();
		}

		return playlist;
	}

	public List<RemotePlaylistItem> getItems() {
		return remotePlaylistItemList;
	}

	public Playlist getLocalInstance() {
		return Playlist.findByPk(this.id);
	}

	public int countItems() {
		return remotePlaylistItemList.size();
	}

	public class RemotePlaylistItem {
		private int modelId;
		private int playlistId;

		public RemotePlaylistItem(int modelId, int playlistId) {
			this.modelId = modelId;
			this.playlistId = playlistId;
		}

		public PlaylistItem createLocalCopy() {
			PlaylistItem playlistItem = new PlaylistItem();
			playlistItem.playlistId = getPlaylistId();
			playlistItem.modelId = getModelId();
			playlistItem.save();

			return playlistItem;
		}

		public int getPlaylistId() {
			return playlistId;
		}

		public int getModelId() {
			return modelId;
		}

		public boolean hasLocalCopy() {
			PlaylistItem playlistItem = PlaylistItem.findByPlaylistModel(this.playlistId, this.modelId);
			return playlistItem != null;

		}
	}
}

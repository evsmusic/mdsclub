package com.edwardstock.mds.models;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.edwardstock.mds.BuildConfig;
import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.helpers.StorageHelper;
import com.edwardstock.mds.helpers.downloader.Downloader;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.implementations.ModelDownloaderListener;

import org.json.JSONException;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.InvalidObjectException;
import java.util.List;

@Table(name = "local_model")
public class LocalModel extends com.activeandroid.Model {

	public final static int MODEL_DOWNLOADED = 1;
	public final static int MODEL_NOT_DOWNLOADED = 0;
	public final static int MODEL_LISTENED = 1;
	public final static int MODEL_NOT_LISTENED = 0;
	public final static int MODEL_IS_FAVORITE = 1;
	public final static int MODEL_IS_NOT_FAVORITE = 0;
	private final static String TAG = "LOCAL_MODEL";
	@Column(name = "author")
	public String author;

	@Column(name = "title")
	public String title;

	@Column(name = "duration")
	public String duration;

	@Column(name = "modelId", index = true, unique = true)
	public int modelId;

	@Column(name = "filename")
	public String filename;

	@Column(name = "added")
	public String added;

	@Column(name = "date")
	public String date;

	@Column(name = "radio")
	public String radio;

	@Column(name = "size")
	public long size;

	@Column(name = "humanSize")
	public String humanSize;

	@Column(name = "srcUrl")
	public String srcUrl;

	@Column(name = "fileDownloaded")
	public int fileDownloaded;

	@Column(name = "isFavorite")
	public int isFavorite;

	@Column(name = "listened")
	public int listened;

	@Column(name = "bookmarkPosition")
	public int bookmarkPosition;

	@Column(name = "nextModelId")
	public int nextModelId;

	@Column(name = "previousModelId")
	public int previousModelId;

	public static LocalModel createFromSync(org.json.JSONObject obj) throws JSONException {
		LocalModel model = new LocalModel();
		model.modelId = obj.getInt("modelId");
		model.author = obj.getString(ApiClient.FIELD_AUTHOR);
		model.duration = obj.getString(ApiClient.FIELD_DURATION);
		model.size = obj.getLong(ApiClient.FIELD_SIZE);
		model.title = obj.getString(ApiClient.FIELD_TITLE);
		model.isFavorite = obj.getInt("isFavorite");
		model.listened = obj.getInt("listened");
		model.bookmarkPosition = obj.getInt("bookmarkPosition");
		model.nextModelId = obj.getInt("nextModelId");
		model.previousModelId = obj.getInt("previousModelId");
		model.srcUrl = obj.getString("srcUrl");
		model.fileDownloaded = 0;
		model.save();
		Log.d(TAG, "Create from sync: " + model.author + ": " + model.title);

		return model;
	}

	public static LocalModel createFromAPI(JSONObject obj) {
		LocalModel model = new LocalModel();
		model.modelId = Integer.valueOf(obj.get(ApiClient.FIELD_ID).toString());
		model.author = (String) obj.get(ApiClient.FIELD_AUTHOR);
		model.date = (String) obj.get(ApiClient.FIELD_DATE);
		model.duration = obj.get(ApiClient.FIELD_DURATION).toString();
		model.radio = (String) obj.get(ApiClient.FIELD_RADIO);
		model.size = Long.valueOf(obj.get(ApiClient.FIELD_SIZE).toString());
		model.humanSize = (String) obj.get(ApiClient.FIELD_SIZE_HUMAN_READABLE);
		model.title = (String) obj.get(ApiClient.FIELD_TITLE);
		model.srcUrl = (String) obj.get(ApiClient.FIELD_URL);
		model.previousModelId = Integer.valueOf(obj.get(ApiClient.FIELD_PREVIOUS_MODEL_ID).toString());
		model.nextModelId = Integer.valueOf(obj.get(ApiClient.FIELD_NEXT_MODEL_ID).toString());
		model.setDefaults();
		model.save();

		return model;
	}

	public void setDefaults() {
		this.fileDownloaded = MODEL_NOT_DOWNLOADED;
		this.listened = MODEL_NOT_LISTENED;
		this.isFavorite = MODEL_IS_NOT_FAVORITE;
		this.bookmarkPosition = 0;
	}

	public static LocalModel createFromRemote(RemoteModel remoteModel) {
		LocalModel model;

		if (null != (model = LocalModel.getByModelId(remoteModel.getId())))
			return model;

		model = new LocalModel();
		model.modelId = remoteModel.getId();
		model.author = remoteModel.getAuthor();
		model.date = remoteModel.getDate();
		model.duration = remoteModel.getDuration();
		model.radio = remoteModel.getRadio();
		model.size = remoteModel.getSize();
		model.humanSize = remoteModel.getHumanSize();
		model.title = remoteModel.getTitle();
		model.srcUrl = remoteModel.getSource();
		model.previousModelId = remoteModel.getPreviousModelId();
		model.nextModelId = remoteModel.getNextModelId();
		model.setDefaults();
		model.save();

		return model;
	}

	public static LocalModel getByModelId(int modelId) {
		return new Select().from(LocalModel.class).where("modelId = ?", modelId).executeSingle();
	}

	public static boolean isDownloaded(int modelId) {
		LocalModel model = new Select().from(LocalModel.class).where("modelId = ? AND fileDownloaded = ?", modelId, 1).executeSingle();
		return model != null;
	}

	public static boolean isFavorite(int modelId) {
		LocalModel model = new Select().from(LocalModel.class).where("modelId = ? AND isFavorite = ?", modelId, 1).executeSingle();
		return model != null;
	}

	public static boolean isListened(int modelId) {
		LocalModel model = new Select().from(LocalModel.class).where("modelId = ? AND listened = ?", modelId, 1).executeSingle();
		return model != null;
	}

	public static List<LocalModel> getDownloaded() {
		return new Select().from(LocalModel.class)
			.where("fileDownloaded = ?", MODEL_DOWNLOADED)
			.orderBy("Id DESC")
			.execute();
	}

	public static void clearListened() {
		for (LocalModel item : getListened()) {
			item.listened = 0;
			item.save();
		}
	}

	public static List<LocalModel> getListened() {
		return new Select().from(LocalModel.class)
			.where("listened = ?", MODEL_LISTENED)
			.orderBy("Id DESC")
			.execute();
	}

	public static void clearFavorites() {
		for (LocalModel item : getFavorites()) {
			item.isFavorite = 0;
			item.save();
		}
	}

	public static List<LocalModel> getFavorites() {
		return new Select().from(LocalModel.class)
			.where("isFavorite = ?", MODEL_IS_FAVORITE)
			.orderBy("Id DESC")
			.execute();
	}

	public static void clearBookmarks() {
		for (LocalModel item : getBookmarks()) {
			item.bookmarkPosition = 0;
			item.save();
		}
	}

	public static List<LocalModel> getBookmarks() {
		return new Select().from(LocalModel.class)
			.where("bookmarkPosition > 0")
			.orderBy("Id DESC")
			.execute();
	}

	public static void setBookmark(int modelId, int position) {
		LocalModel m = LocalModel.getByModelId(modelId);
		m.bookmarkPosition = position;
		m.save();
	}

	public static void setListened(int modelId, boolean listened) {
		LocalModel m = LocalModel.getByModelId(modelId);
		m.listened = listened ? MODEL_LISTENED : MODEL_NOT_LISTENED;
		m.save();
	}

	public static void setFavorite(int modelId, boolean favorite) {
		LocalModel m = LocalModel.getByModelId(modelId);
		m.isFavorite = favorite ? MODEL_IS_FAVORITE : MODEL_IS_NOT_FAVORITE;
		m.save();
	}

	public static boolean exists(int modelId) {
		return new Select().from(LocalModel.class).where("modelId = ?", modelId).exists();
	}

	public static int countAll() {
		return new Select().from(LocalModel.class).count();
	}

	public long getSize() {
		return size;
	}

	public String getDuration() {
		return duration;
	}

	public String getRadio() {
		return radio;
	}

	public void downloadFile(Context context, Class<? extends BaseActivity> intentClassName, final boolean saveDb) {
		downloadFile(context, intentClassName, saveDb, null);
	}

	public void downloadFile(
		final Context context,
		Class<? extends BaseActivity> intentClassName,
		final boolean saveDb,
		final Downloader.OnDownloadEventListener eventListener
	) {
		String[] tmpName = getSource().split("/");
		final String filename = tmpName[tmpName.length - 1];

		File sdCard = StorageHelper.getInstance(context).getTrueExternalStoragePath();
		File dir = null;
		if (sdCard != null) {
			dir = new File(sdCard.getAbsolutePath() + "/offline_data");
			dir.mkdirs();
		}

		final File file = new File(dir, filename);
		if (file.exists()) {
			file.delete();
		}

		final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		final int notifierId = getModelId();
		Intent notificationIntent = new Intent(context, intentClassName);
		PendingIntent defaultIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		final NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

		builder
			.setOngoing(true)
			.setSmallIcon(android.R.drawable.stat_sys_download)
			.setAutoCancel(true)
			.setContentIntent(defaultIntent)
			.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), android.R.drawable.stat_sys_download)) // большая картинка
			.setTicker("Загрузка модели...") // текст в строке состояния
			.setWhen(System.currentTimeMillis()) // java.lang.System.currentTimeMillis()
			.setAutoCancel(true)
			.setContentTitle("Загрузка")
			.setContentText("Загружается: " + getAuthor() + " - " + getTitle()); // Заголовок уведомления


		Downloader downloader = new Downloader(getSource(), file);

		ModelDownloaderListener downloaderListener = new ModelDownloaderListener(context, this);
		downloaderListener.setBuilder(builder);
		downloaderListener.setEventListener(eventListener);
		downloaderListener.setFile(file);
		downloaderListener.setNotificationManager(notificationManager);
		downloaderListener.setNotifierId(notifierId);
		downloaderListener.setSaveInDB(saveDb);

		try {
			downloader.setEventListener(downloaderListener);
			downloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} catch (InvalidObjectException e) {
			e.printStackTrace();
		}
	}

	public String getSource() {
		Log.d(TAG, "FileDownloaded? " + fileDownloaded);
		if (fileDownloaded == 1) {
			return filename;
		} else {

			return srcUrl.replace("mds.static.es-dev.ru", BuildConfig.API_HOST);
		}
	}

	public int getModelId() {
		return modelId;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public boolean fileIsDownloaded() {
		return this.fileDownloaded == MODEL_DOWNLOADED && this.filename != null && (new File(this.filename)).exists();
	}

	public boolean getIsFavorite() {
		return this.isFavorite == MODEL_IS_FAVORITE;
	}

	public LocalModel setIsFavorite() {
		this.isFavorite = MODEL_IS_FAVORITE;
		return this;
	}

	public boolean getIsListened() {
		return this.listened == MODEL_LISTENED;
	}

	public boolean hasNextModel() {
		return this.nextModelId > 0;
	}

	public boolean hasPreviousModel() {
		return this.previousModelId > 0;
	}

	public boolean hasBookmark() {
		return this.bookmarkPosition > 0;
	}

	public LocalModel deleteFile() {
		return deleteFile(false);
	}

	public LocalModel deleteFile(boolean markAsNotDownloaded) {
		if (!getIsDownloaded())
			return this;

		File file = new File(getSource());
		if (!file.exists()) {
			return this;
		}

		file.delete();

		if (markAsNotDownloaded) {
			this.fileDownloaded = MODEL_NOT_DOWNLOADED;
			this.save();
		}

		return this;
	}

	public boolean getIsDownloaded() {
		LocalModel model = new Select().from(LocalModel.class)
			.where("modelId = ? AND fileDownloaded = ?", modelId, 1).executeSingle();
		return model != null;
	}
}

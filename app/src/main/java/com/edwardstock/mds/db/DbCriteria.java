package com.edwardstock.mds.db;


public class DbCriteria {
	
	private String[] columns;
	
	public DbCriteria select(String... columns) {
		this.columns = columns;
		return this;
	}
}

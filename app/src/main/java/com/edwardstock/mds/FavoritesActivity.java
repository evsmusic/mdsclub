package com.edwardstock.mds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.edwardstock.mds.adapters.FavoritesAdapter;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;
import java.util.List;


public class FavoritesActivity extends BaseActivity {

	private ListView listView;
	private FavoritesAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorites);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.favorites_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		new AsyncTask<Void, Void, List<LocalModel>>() {
			@Override
			protected List<LocalModel> doInBackground(Void... params) {
				return LocalModel.getFavorites();
			}

			protected void onPostExecute(List<LocalModel> result) {
				super.onPostExecute(result);
				initMainList(new ArrayList<>(result));
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		sendGAScreen();
	}

	private void initMainList(ArrayList<LocalModel> items) {
		adapter = new FavoritesAdapter(this, items);

		listView = (ListView) findViewById(R.id.favoritesListView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(FavoritesActivity.this, PlayerActivity.class);
				intent.putExtra(PlayerActivity.PARAM_MODEL_ID, (int) id);
				startActivity(intent);
				listView.invalidate();
			}
		});

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {
				new AlertDialog.Builder(getSupportActionBar().getThemedContext())
					.setTitle("Удалить модель из избранного?")
					.setIcon(android.R.drawable.stat_sys_warning)
					.setPositiveButton("Да", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							LocalModel.setFavorite((int) id, false);
							listView.setAdapter(new FavoritesAdapter(FavoritesActivity.this, new ArrayList<>(LocalModel.getFavorites())));
						}
					})
					.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// nothing to do
						}
					}).show();

				return true;
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}

		if (id == R.id.favorites_clear) {
			clearFavorites();
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStart() {
		super.onStart();

		if (listView != null) {
			listView.invalidateViews();
		}
	}

	private void clearFavorites() {
		if (adapter.size() == 0) {
			Toast.makeText(this, "Нечего удалять", Toast.LENGTH_SHORT).show();
			return;
		}
		new AlertDialog.Builder(this)
			.setTitle("Очистка избранного")
			.setMessage(getResources().getString(R.string.dialog_clear_favorites))
			.setPositiveButton("Да", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					List<LocalModel> favorites = LocalModel.getFavorites();
					for (LocalModel item : favorites) {
						item.isFavorite = LocalModel.MODEL_IS_NOT_FAVORITE;
						item.save();
					}
					listView.setAdapter(new FavoritesAdapter(FavoritesActivity.this, new ArrayList<>(LocalModel.getFavorites())));
				}
			}).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		}).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_favorites, menu);
		return true;
	}
}

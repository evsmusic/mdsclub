package com.edwardstock.mds.helpers.downloader;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class Downloader extends AsyncTask<Void, Integer, Void>
{

	public final static int BUFFER_SIZE = 0x800;
	private final static String TAG = "DownloadTask";
	public volatile int totalSize;
	public volatile int processedSize = 0;
	private String targetPath;
	private String url;
	private OnDownloadEventListener eventListener;
	private File file;
	private boolean complete = true;

	public Downloader(String url, File file) {
		this.url = url;
		this.targetPath = file.getAbsolutePath();
		this.file = file;
	}

	public void setEventListener(OnDownloadEventListener.Validator listener) throws InvalidObjectException {
		if(!listener.validate()) {
			throw new InvalidObjectException("Object OnDownloadEventListener is not validated. Check your setters");
		}
		this.eventListener = listener.getListener();
	}

	private boolean createFileAndDir() {
		File dir = new File(this.targetPath);
		System.out.println(this.targetPath);
		if (!dir.getParentFile().exists()) {
			dir.getParentFile().mkdirs();
		}

		if (!dir.exists()) {
			try {
				dir.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (dir.exists()) {
			return false;
		}

		return true;
	}

	public long getSize() {
		return totalSize;
	}

	@Override
	protected Void doInBackground(Void... voids) {
		download();

		return null;
	}

	private void download() {
		try {
			Log.d(TAG, "Start downloading... "+url);

			URL connection = new URL(url);
			HttpURLConnection urlConnection;
			urlConnection = (HttpURLConnection) connection.openConnection();
			urlConnection.setRequestMethod("GET");
			Log.d(TAG, "Connecting to url...");
			urlConnection.connect();

			totalSize = urlConnection.getContentLength();
			Log.d(TAG, "Total size of content: " + String.valueOf(totalSize));
			InputStream in;
			in = urlConnection.getInputStream();

			OutputStream writer = new FileOutputStream(file);

			byte buffer[] = new byte[BUFFER_SIZE];
			int c = in.read(buffer);

			int parts = 100;
			int part = totalSize / parts;
			int nextProgressStep = part;

			publishProgress(0, totalSize);
			while (c > 0) {
				writer.write(buffer, 0, c);
				c = in.read(buffer);
				processedSize += c;

				if(processedSize >= nextProgressStep) {
					publishProgress(processedSize, totalSize);
					nextProgressStep+= part;
				}

			}
			writer.flush();
			writer.close();
			in.close();

		} catch (IOException e) {
			complete = false;
			eventListener.onError(e);
		} finally {
			Log.d(TAG, "Done!");
		}
	}

	public void onPostExecute(Void result) {
		super.onPostExecute(result);

		if(complete)
			eventListener.onComplete();
	}

	public void onProgressUpdate(Integer... progress) {
		super.onProgressUpdate(progress);
		eventListener.onUpdate(progress[0], progress[1]);
	}

	public interface OnDownloadEventListener {
		void onUpdate(int processedSize, int totalSize);
		void onComplete();
		void onError(IOException err);

		interface Validator{
			boolean validate();
			OnDownloadEventListener getListener();
		}
	}
}

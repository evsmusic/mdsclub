package com.edwardstock.mds.helpers;

import android.content.Intent;
import android.os.Bundle;

public class Dumper
{
	public static String dumpBundleExtras(Intent intent) {
		StringBuilder builder = new StringBuilder("\n");

		for(String key : intent.getExtras().keySet()) {
			builder.append(key).append(" = ").append(intent.getExtras().get(key)).append("\n");
		}

		return builder.toString();
	}

	public static String dumpBundleExtras(Bundle bundle) {
		StringBuilder builder = new StringBuilder("\n");

		for(String key : bundle.keySet()) {
			builder.append(key).append(" = ").append(bundle.get(key)).append("\n");
		}

		return builder.toString();
	}
}

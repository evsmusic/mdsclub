package com.edwardstock.mds.helpers;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;


public class ResizeWidthAnimation extends Animation {
	private int width;
	private int mStartWidth;
	private View mView;

	public ResizeWidthAnimation(View view) {
		mView = view;
		mStartWidth = view.getWidth();
	}

	public ResizeWidthAnimation(View view, int width) {
		mView = view;
		this.width = width;
		mStartWidth = view.getWidth();
	}

    public ResizeWidthAnimation(View view, int width, long duration) {
        mView = view;
        this.width = width;
        mStartWidth = view.getWidth();
        setDuration(duration);
    }

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
	}

	@Override
	public boolean willChangeBounds() {
		return true;
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		mView.getLayoutParams().width = mStartWidth + (int) ((width - mStartWidth) * interpolatedTime);
		mView.requestLayout();
	}
}

package com.edwardstock.mds.helpers;

public class TimeHelper
{
	public static String getTimeString(int milliseconds) {
		int hours = (milliseconds / (1000 * 60 * 60));
		int minutes = ((milliseconds % (1000 * 60 * 60)) / (1000 * 60));
		int seconds = (((milliseconds % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

		return String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
	}
}

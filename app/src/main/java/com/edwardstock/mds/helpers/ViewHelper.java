package com.edwardstock.mds.helpers;

import android.view.View;
import android.view.ViewGroup;


public class ViewHelper {

	public static void setViewWidth(View view, int width) {
		ViewGroup.LayoutParams params = view.getLayoutParams();
		params.width = width;
		view.setLayoutParams(params);
	}

	public static void setViewHeight(View view, int height) {
		ViewGroup.LayoutParams params = view.getLayoutParams();
		params.height = height;
		view.setLayoutParams(params);
	}
}

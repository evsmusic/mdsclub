package com.edwardstock.mds.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class HttpHelper {
	final static String TAG = "HttpHelper";

	public static JSONObject getJSONObjectByUrl(URL url) throws IOException {
		String content = getContentsByUrl(url);

		JSONParser parser = new JSONParser();
		JSONObject object = null;
		Object obj;

		try {
			obj = parser.parse(content);
			object = (JSONObject) obj;
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			Log.d(TAG, "URL passed: " + url);
			throw e;
		}

		return object;
	}

	public static String getContentsByUrl(URL url) throws IOException {
		StringBuilder result = new StringBuilder();
		try {
			// get URL content
			URLConnection conn = url.openConnection();
			conn.addRequestProperty("Accept", "text/json,application/json;q=0.9,image/webp,*/*;q=0.8");
			InputStream stream = conn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(stream, "UTF-8");
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String line;

			//Log.d(TAG, "Trying to get url: "+url.toString());

			while((line = bufferedReader.readLine()) != null) {
				result.append(line);
			}

			inputStreamReader.close();
			bufferedReader.close();
			stream.close();

		} catch (MalformedURLException e) {

			e.printStackTrace();
		}



		return result.toString();
	}

	public static JSONArray getJSONArrayByUrl(URL url) throws IOException {
		String content = getContentsByUrl(url);

		JSONParser parser = new JSONParser();
		JSONArray array = null;
		Object obj;

		try {
			obj = parser.parse(content);
			array = (JSONArray) obj;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return array;
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager
			= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
}

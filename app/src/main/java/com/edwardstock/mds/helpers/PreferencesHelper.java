package com.edwardstock.mds.helpers;


import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.widget.ListAdapter;

public class PreferencesHelper {

	public static void launchPreferenceScreen(PreferenceFragment pref, String key) {
		final Preference preference = pref.findPreference(key);
		final PreferenceScreen preferenceScreen = pref.getPreferenceScreen();
		final ListAdapter listAdapter = preferenceScreen.getRootAdapter();
		final int itemsCount = listAdapter.getCount();
		int itemNumber;
		for (itemNumber = 0; itemNumber < itemsCount; ++itemNumber) {
			if (listAdapter.getItem(itemNumber).equals(preference)) {
				preferenceScreen.onItemClick(null, null, itemNumber, 0);
				break;
			}
		}
	}
}

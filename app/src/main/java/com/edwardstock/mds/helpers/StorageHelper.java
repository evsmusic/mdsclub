package com.edwardstock.mds.helpers;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class StorageHelper {

	private static StorageHelper instance;

	private Context context;

	/**
	 * Singleton private constructor
	 */
	private StorageHelper(Context context) {
		this.context = context;
	}

	public static synchronized StorageHelper getInstance(Context context) {
		if(instance == null) {
			instance = new StorageHelper(context);
		}

		return instance;
	}

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state) ||
			Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}

	public File getTrueExternalStoragePath() {
		File file = new File("/system/etc/vold.fstab");
		FileReader fr = null;
		BufferedReader br = null;
		String path = null;

		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			if (fr != null) {
				br = new BufferedReader(fr);
				String s = br.readLine();
				while (s != null) {
					if (s.startsWith("dev_mount")) {
						String[] tokens = s.split("\\s");
						path = tokens[2]; //mount_point
						if (!Environment.getExternalStorageDirectory().getAbsolutePath().equals(path)) {
							break;
						}
					}
					s = br.readLine();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if(path != null) {
			path += "/Android/data/com.edwardstock.mds/files/Podcasts";
			return new File(path);
		}

		return this.context.getExternalFilesDir(Environment.DIRECTORY_PODCASTS);
	}
}

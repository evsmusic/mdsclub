package com.edwardstock.mds.helpers;

import java.util.zip.CRC32;

public class ChecksumHelper {
	
	public static long getCRC32(Object... values) {
		CRC32 hash = new CRC32();
		StringBuilder stringBuilder = new StringBuilder();
		
		for(Object s: values) {
			stringBuilder.append(s);
		}
		
		hash.update(stringBuilder.toString().getBytes());
		return hash.getValue();
	}

	public static long getCRC32(String glue, Object... values) {
		CRC32 hash = new CRC32();
		StringBuilder stringBuilder = new StringBuilder();

		int i = 0;
		for(Object s: values) {
			if(i > 0 && i < values.length) {
				stringBuilder.append(glue);				
			}
			
			stringBuilder.append(s);			
			
			i++;
		}

		hash.update(stringBuilder.toString().getBytes());
		return hash.getValue();
	}
}

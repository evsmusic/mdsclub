package com.edwardstock.mds.helpers;

import java.util.HashMap;

public class ProgressProvider {
	private static ProgressProvider instance;
	private HashMap<Integer, Proxy> proxies = new HashMap<>();

	private ProgressProvider() {
	}

	public static synchronized ProgressProvider get() {
		if (instance == null)
			instance = new ProgressProvider();

		return instance;
	}

	public void setProgress(int id, int progress, int total) {
		if (proxies.containsKey(id)) {
			proxies.get(id).setProgress(progress).setTotal(total);
		} else {
			proxies.put(id, new Proxy(progress, total));
		}
	}

	public synchronized void setProxyListener(int id, ProxyListener listener) {
		if (listener == null)
			throw new NullPointerException("ProxyListener cannot be null");

		if (proxies.containsKey(id)) {
			proxies.get(id).setProxyListener(listener);
		}
	}

	public Proxy getProxy(int id) {
		if (proxies.containsKey(id))
			return proxies.get(id);

		return null;
	}

	public void deleteProgress(int id) {
		if (proxies.containsKey(id))
			proxies.remove(id);
	}

	public boolean exists(int id) {
		return existsInternal(id);

	}

	private boolean existsInternal(int id) {
		return proxies.containsKey(id);
	}

	public interface ProxyListener {
		void onUpdate(int progress, int total);

		void onComplete();

		void onError();
	}

	public static class Proxy {
		private int progress;
		private int total;
		private boolean error = false;
		private ProxyListener listener;

		Proxy(int progress, int total) {

			this.progress = progress;
			this.total = total;
		}

		public boolean complete() {
			return this.progress == this.total;
		}

		public boolean errorOccurred() {
			return this.error;
		}

		public int getProgress() {
			return progress;
		}

		public Proxy setProgress(int progress) {
			this.progress = progress;
			if (listener != null) {
				if (progress <= total) {
					listener.onUpdate(progress, total);
				} else if (progress >= total) {
					listener.onComplete();
				} else if (error) {
					listener.onError();
				}
			}

			return this;
		}

		public int getTotal() {
			return total;
		}

		public Proxy setTotal(int total) {
			this.total = total;
			return this;
		}

		public void setProxyListener(ProxyListener listener) {
			this.listener = listener;
		}

		public Proxy setErrorOccurred() {
			this.error = true;
			return this;
		}
	}

}

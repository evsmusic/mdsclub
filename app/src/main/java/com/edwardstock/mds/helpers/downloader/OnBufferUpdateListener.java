package com.edwardstock.mds.helpers.downloader;

/**
 * Created by SYSTEM on 29.06.2014.
 */
public interface OnBufferUpdateListener {

    void onUpdate(int processedSize, int totalSize);

	void onComplete();
}

package com.edwardstock.mds.adapters;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;

public class HistoryAdapter extends DownloadsAdapter {

	public HistoryAdapter(Context context, ArrayList<LocalModel> models) {
		super(context, models);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		LocalModel item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(getListItemLayout(), parent, false);
			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		fillDefaultHolder(holder, item);

		setItemIsDownloaded(item.getIsDownloaded(), holder);
		setItemIsFavorite(item.getIsFavorite(), holder);
		setItemIsPlaybackNow(item.getModelId(), holder);

		return view;
	}

	@Override
	protected void fillDefaultHolder(BaseListAdapter.BaseListViewHolder holder, LocalModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(data.getDuration());
	}


	static class ViewHolder extends BaseListAdapter.BaseListViewHolder {
	}
}

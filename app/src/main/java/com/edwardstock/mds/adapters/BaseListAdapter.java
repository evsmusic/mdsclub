package com.edwardstock.mds.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edwardstock.mds.R;
import com.edwardstock.mds.helpers.UnitConverter;
import com.edwardstock.mds.implementations.BaseActivity;

import java.io.Serializable;
import java.util.ArrayList;

abstract public class BaseListAdapter<T> extends BaseAdapter implements Serializable {

	protected ArrayList<T> items;
	protected LayoutInflater inflater;
	protected Context context;
	private boolean useAdvancedListItem = false;

	public ArrayList<T> getValues() {
		if (this.items != null)
			return this.items;

		return new ArrayList<>(0);
	}

	public void setValues(ArrayList<T> items) {
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public T getItem(int position) {
		return items.get(position);
	}

	@Override
	abstract public long getItemId(int position);

	@Override
	abstract public View getView(int position, View convertView, ViewGroup parent);

	public void clearItems() {
		items.clear();
	}

	protected void initDefaultHolder(BaseListViewHolder holder, View view) {
		holder.title = (TextView) view.findViewById(R.id.title);
		holder.author = (TextView) view.findViewById(R.id.author);
		holder.duration = (TextView) view.findViewById(R.id.duration);
		holder.itemIsPlayback = (ImageView) view.findViewById(R.id.currentlyPlayback);
		holder.itemIsListened = (ImageView) view.findViewById(R.id.itemIsListened);
		holder.itemIsFavorite = (ImageView) view.findViewById(R.id.itemIsFavorite);
		holder.itemIsDownloaded = (ImageView) view.findViewById(R.id.itemIsDownloaded);
		holder.itemHasBookmark = (ImageView) view.findViewById(R.id.itemHasBookmark);
	}

	abstract protected void fillDefaultHolder(BaseListViewHolder holder, T data);

	protected void setItemIsListened(boolean condition, BaseListViewHolder holder) {
		if (condition) {
			holder.itemIsListened.setVisibility(View.VISIBLE);
		} else {
			holder.itemIsListened.setVisibility(View.GONE);
		}
	}

	protected void setItemIsDownloaded(boolean condition, BaseListViewHolder holder) {
		if (condition) {
			holder.itemIsDownloaded.setVisibility(View.VISIBLE);
		} else {
			holder.itemIsDownloaded.setVisibility(View.GONE);
		}
	}

	protected void setItemIsFavorite(boolean condition, BaseListViewHolder holder) {
		if (condition) {
			holder.itemIsFavorite.setVisibility(View.VISIBLE);
		} else {
			holder.itemIsFavorite.setVisibility(View.GONE);
		}
	}

	protected void setItemHasBookmark(boolean condition, BaseListViewHolder holder) {
		if (condition) {
			holder.itemHasBookmark.setVisibility(View.VISIBLE);
		} else {
			holder.itemHasBookmark.setVisibility(View.GONE);
		}
	}

	protected void setItemIsPlaybackNow(int id, BaseListViewHolder holder) {
		try {
			if (BaseActivity.service == null || !BaseActivity.service.getMediaPlayer().isPlaying() || BaseActivity.service.getModel().getModelId() != id) {
				holder.itemIsPlayback.setVisibility(View.GONE);
				holder.title.setPadding(0, 0, 0, 0);
				return;
			}
		} catch (IllegalStateException e) {
			holder.itemIsPlayback.setVisibility(View.GONE);
			holder.title.setPadding(0, 0, 0, 0);
			return;
		}


		holder.itemIsPlayback.setVisibility(View.VISIBLE);
		holder.title.setPadding(UnitConverter.calcDP(context, 35), 0, 0, 0);
	}

	public void useAdvancedListItem() {
		this.useAdvancedListItem = true;
	}

	protected int getListItemLayout() {
		return this.useAdvancedListItem ? R.layout.list_item_advanced : R.layout.list_item;
	}

	protected static class BaseListViewHolder {
		public TextView title;
		public TextView author;
		public TextView duration;
		public ImageView itemIsDownloaded;
		public ImageView itemIsFavorite;
		public ImageView itemIsListened;
		public ImageView itemIsPlayback;
		public ImageView itemHasBookmark;
		public ImageView popupMenu;
		public RelativeLayout mainListItem;

		public void initDefaults(View view) {
			this.title = (TextView) view.findViewById(R.id.title);
			this.author = (TextView) view.findViewById(R.id.author);
			this.duration = (TextView) view.findViewById(R.id.duration);
			this.itemIsPlayback = (ImageView) view.findViewById(R.id.currentlyPlayback);
			this.itemIsListened = (ImageView) view.findViewById(R.id.itemIsListened);
			this.itemIsFavorite = (ImageView) view.findViewById(R.id.itemIsFavorite);
			this.itemIsDownloaded = (ImageView) view.findViewById(R.id.itemIsDownloaded);
		}

		public void setItemIsDownloaded(boolean condition) {
			if (this.itemIsDownloaded == null)
				return;

			if (condition) {
				this.itemIsDownloaded.setVisibility(View.VISIBLE);
			} else {
				this.itemIsDownloaded.setVisibility(View.GONE);
			}
		}

		public void setItemIsListened(boolean condition) {
			if (this.itemIsListened == null)
				return;
			if (condition) {
				this.itemIsListened.setVisibility(View.VISIBLE);
			} else {
				this.itemIsListened.setVisibility(View.GONE);
			}
		}

		public void setItemIsFavorite(boolean condition) {
			if (this.itemIsFavorite == null)
				return;
			if (condition) {
				this.itemIsFavorite.setVisibility(View.VISIBLE);
			} else {
				this.itemIsFavorite.setVisibility(View.GONE);
			}
		}

		public void setItemIsPlaybackNow(int id, Context context) {
			if (this.itemIsPlayback == null || this.title == null)
				return;

			if (BaseActivity.service == null || !BaseActivity.service.getMediaPlayer().isPlaying() || BaseActivity.service.getModel().getModelId() != id) {
				this.itemIsPlayback.setVisibility(View.GONE);
				this.title.setPadding(0, 0, 0, 0);
				return;
			}

			this.itemIsPlayback.setVisibility(View.VISIBLE);
			this.title.setPadding(UnitConverter.calcDP(context, 35), 0, 0, 0);
		}
	}
}

package com.edwardstock.mds.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edwardstock.mds.R;
import com.edwardstock.mds.helpers.TimeHelper;
import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;


public class BookmarksAdapter extends DownloadsAdapter {

	public BookmarksAdapter(Context context, ArrayList<LocalModel> items) {
		super(context, items);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		LocalModel item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(R.layout.list_item_bookmarks, parent, false);
			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			holder.lastPosition = (TextView) view.findViewById(R.id.lastPosition);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		fillDefaultHolder(holder, item);
		holder.lastPosition.setText(TimeHelper.getTimeString(item.bookmarkPosition));

		setItemIsDownloaded(item.getIsDownloaded(), holder);
		setItemIsFavorite(item.getIsFavorite(), holder);

		setItemIsPlaybackNow(item.getModelId(), holder);

		return view;
	}

	@Override
	protected void fillDefaultHolder(BaseListAdapter.BaseListViewHolder holder, LocalModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(data.getDuration());
	}


	static class ViewHolder extends BaseListAdapter.BaseListViewHolder {
		TextView lastPosition;
	}
}

package com.edwardstock.mds.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edwardstock.mds.PlaylistItemActivity;
import com.edwardstock.mds.PlaylistsActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.models.PlaylistItem;

import java.util.ArrayList;
import java.util.List;

public class PlaylistAdapter extends BaseAdapter {

	private final static String TAG = "PlaylistAdapter";

	private Context context;
	private ArrayList<Playlist> items;
	private LayoutInflater inflater;
	private AdapterView.OnItemClickListener listener;

	public PlaylistAdapter(Context context, List<Playlist> playlists) {
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.items = new ArrayList<>(playlists);
	}

	public void reinitElements(List<Playlist> items) {
		this.items = new ArrayList<>(items);
	}

	public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
		this.listener = listener;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Playlist getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final Playlist item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(R.layout.playlist_list_item, parent, false);
			holder = new ViewHolder();
			holder.mainItem = (RelativeLayout) view.findViewById(R.id.main_item);
			holder.title = (TextView) view.findViewById(R.id.playlist_title);
			holder.popupMenu = (ImageView) view.findViewById(R.id.popup_menu);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.title.setText(item.title + " (" + Playlist.countItems(item.getId()) + ")");

		if (this.listener != null) {
			holder.mainItem.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(TAG, "On item click with id: " + getItemId(position));
					Log.d(TAG, "On item click el: " + getItem(position).title);
					listener.onItemClick(null, convertView, position, getItemId(position));
				}
			});
		} else {
			holder.mainItem.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, PlaylistItemActivity.class);
					intent.putExtra(PlaylistsActivity.PARAM_PLAYLIST_ID, getItemId(position));
					context.startActivity(intent);
				}
			});
		}


		holder.popupMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popupMenu = new PopupMenu(context, holder.popupMenu);
				popupMenu.getMenuInflater().inflate(R.menu.playlist_options, popupMenu.getMenu());
				popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem menuItem) {
						Playlist pl = Playlist.findByPk(getItemId(position));
						if (menuItem.getItemId() == R.id.delete) {
							List<PlaylistItem> items = pl.getItems();
							for (PlaylistItem item : items) {
								item.delete();
							}
							pl.delete();
							removeItem(position);
							notifyDataSetChanged();
						} else if (menuItem.getItemId() == R.id.rename) {
							final EditText input = new EditText(context);
							input.setText(getItem(position).title);
							AlertDialog.Builder ad = new AlertDialog.Builder(context);
							ad.setTitle("Переименовать");
							ad.setView(input);
							ad.setNegativeButton(context.getString(R.string.cancelSearch), null);
							ad.setPositiveButton(context.getString(R.string.save), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Playlist pl = Playlist.findByPk(getItemId(position));
									pl.title = input.getText().toString();
									pl.save();
									notifyDataSetChanged();
								}
							});
							ad.show();


						}
						return true;
					}
				});
				popupMenu.show();
			}
		});


		return view;
	}

	public void removeItem(int position) {
		this.items.remove(position);
	}

	static class ViewHolder {
		TextView title;
		ImageView popupMenu;
		RelativeLayout mainItem;
	}
}

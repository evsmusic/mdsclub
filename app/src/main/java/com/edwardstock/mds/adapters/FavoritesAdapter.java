package com.edwardstock.mds.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;

public class FavoritesAdapter extends DownloadsAdapter {
	public FavoritesAdapter(Context context, ArrayList<LocalModel> models) {
		super(context, models);
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		LocalModel item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(getListItemLayout(), parent, false);
			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		fillDefaultHolder(holder, item);

		setItemIsDownloaded(item.getIsDownloaded(), holder);
		setItemIsListened(item.getIsListened(), holder);
		setItemIsPlaybackNow(item.getModelId(), holder);

		return view;
	}

	@Override
	protected void fillDefaultHolder(BaseListViewHolder holder, LocalModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(data.getDuration());
	}


	static class ViewHolder extends BaseListViewHolder {
	}

}

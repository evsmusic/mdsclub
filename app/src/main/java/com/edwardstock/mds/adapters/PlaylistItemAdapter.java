package com.edwardstock.mds.adapters;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.edwardstock.mds.R;
import com.edwardstock.mds.implementations.ActiveRecordModel;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.PlaylistItem;

import java.util.ArrayList;

public class PlaylistItemAdapter extends DownloadsAdapter {

	private long playlistId;
	private AdapterView.OnItemClickListener listener;


	public PlaylistItemAdapter(Context context, ArrayList<LocalModel> models, long playlistId) {
		super(context, models);
		this.playlistId = playlistId;
	}

	public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
		this.listener = listener;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		LocalModel item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			useAdvancedListItem();
			view = inflater.inflate(getListItemLayout(), parent, false);
			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			holder.popupMenu = (ImageView) view.findViewById(R.id.popup_menu);
			holder.mainListItem = (RelativeLayout) view.findViewById(R.id.list_main_data);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		fillDefaultHolder(holder, item);

		setItemIsDownloaded(item.getIsDownloaded(), holder);
		setItemIsFavorite(item.getIsFavorite(), holder);
		setItemIsPlaybackNow(item.getModelId(), holder);

		if (listener != null) {
			holder.mainListItem.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					listener.onItemClick(null, convertView, position, getItemId(position));
				}
			});
		}

		holder.popupMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popupMenu = new PopupMenu(context, holder.popupMenu);
				popupMenu.getMenuInflater().inflate(R.menu.playlistitem_options, popupMenu.getMenu());
				popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem menuItem) {
						if (menuItem.getItemId() == R.id.delete_from_playlist) {
							ActiveRecordModel.deleteItem(PlaylistItem.class, PlaylistItem.findIdByPlaylistModel(playlistId, (int) getItemId(position)));
							removeItem(position);
							notifyDataSetChanged();
						}
						return true;
					}
				});

				popupMenu.show();
			}
		});

		return view;
	}

	private void removeItem(int position) {
		this.items.remove(position);
	}

	@Override
	protected void fillDefaultHolder(BaseListAdapter.BaseListViewHolder holder, LocalModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(data.getDuration());
	}


	static class ViewHolder extends BaseListAdapter.BaseListViewHolder {
	}
}

package com.edwardstock.mds.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.edwardstock.mds.PlayerActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.SettingsActivity;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.RemoteModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CatalogAdapter extends BaseListAdapter<RemoteModel> {

	private final static String TAG = "CatalogAdapter";
	private boolean skipListened = false;
	private HashMap<Integer, RemoteModel> skippedItems = new HashMap<>();
	private OnMenuItemClick onMenuItemClick;

	public CatalogAdapter(Context context, ArrayList<RemoteModel> remoteModels, ListView listView) {
		this.items = remoteModels;
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		this.skipListened = sp.getBoolean(SettingsActivity.Settings.PARAM_HIDE_LISTENED, false);
		removeListenedItems(this.items);

		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	private void removeListenedItems(ArrayList<RemoteModel> models) {
		if (!skipListened)
			return;

		int i = 0;
		for (Iterator<RemoteModel> iterator = models.iterator(); iterator.hasNext(); ) {
			RemoteModel el = iterator.next();
			el.fetchLocalInfo();

			if (el.isListened()) {
				skippedItems.put(i, el);
				iterator.remove();
			}
			i++;
		}
	}

	public void setOnMenuItemClickListener(OnMenuItemClick listener) {
		this.onMenuItemClick = listener;
	}

	public void appendPage(ArrayList<RemoteModel> remoteModels) {
		Log.d(TAG, "Appending page... Size before: " + items.size() + "; Size after: " + (items.size() + remoteModels.size()));
		removeListenedItems(remoteModels);
		List<RemoteModel> old = this.items;
		ArrayList<RemoteModel> newList = new ArrayList<>(remoteModels.size() + old.size());

		newList.addAll(old);
		newList.addAll(remoteModels);

		this.items = newList;
	}

	public void invalidateListenedItems() {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		this.skipListened = sp.getBoolean(SettingsActivity.Settings.PARAM_HIDE_LISTENED, false);
		removeListenedItems(this.items);
		notifyDataSetChanged();
	}

	public void removeListenedItem(RemoteModel model, int position) {
		if (!skipListened)
			return;

		skippedItems.put(position, model);
		deleteItem(position);
		notifyDataSetChanged();
	}

	public void deleteItem(int position) {
		this.items.remove(position);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public RemoteModel getItem(int position) {
		if (!skipListened && skippedItems.size() > 0 && skippedItems.containsKey(position)) {
			items.add(position, skippedItems.get(position));
			skippedItems.remove(position);
		}
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		final RemoteModel item = getItem(position);
		final int itemId = (int) getItemId(position);
		final ViewHolder holder;
		View view = convertView;

		//грузим локальную модель если не загрузилась раньше
		item.fetchLocalInfo();

		if (view == null) {
			useAdvancedListItem();
			view = inflater.inflate(getListItemLayout(), parent, false);

			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			//holder.initDefaults(view);
			holder.modelId = itemId;
			holder.popupMenu = (ImageView) view.findViewById(R.id.popup_menu);
			holder.mainListItem = (RelativeLayout) view.findViewById(R.id.list_main_data);
			holder.position = position;
			holder.progressButton = view.findViewById(R.id.button_progress);
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
			holder.modelId = itemId;
			holder.position = position;
		}

		if (holder.itemHasBookmark.getVisibility() == View.VISIBLE) {
			holder.itemHasBookmark.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					((BaseActivity) context).startPlayer(LocalModel.getByModelId(itemId), true);
				}
			});
		}


		//поп-ап менюшка
		//initPopupMenu(position, holder.popupMenu, holder, view);
		if (this.onMenuItemClick != null) {
			this.onMenuItemClick.onClick(position, holder.popupMenu, holder, convertView);
		}
		holder.mainListItem.setOnClickListener(new OnItemClickListener(item));

		fillDefaultHolder(holder, item);
		setItemIsFavorite(item.isInFavorites(), holder);
		setItemIsDownloaded(item.isDownloaded(), holder);
		setItemIsListened(item.isListened(), holder);
		setItemIsPlaybackNow(itemId, holder);
		setItemHasBookmark(item.hasBookmark(), holder);


		//убиваем объект в null
		item.clearLocalInfo();

		return view;
	}

	@Override
	protected void fillDefaultHolder(BaseListViewHolder holder, RemoteModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(data.getDuration());
	}

	public int getTotalItemsCount() {
		if (items.size() >= 1) {
			return items.get(0).getItemsCount();
		}

		return 0;
	}

	public interface OnMenuItemClick {
		void onClick(final int position, View anchor, final ViewHolder holder, final View convertView);
	}

	public static class ViewHolder extends BaseListViewHolder implements Serializable {
		public View progressButton;
		public int modelId;
		public int position;
	}

	final private class OnItemClickListener implements View.OnClickListener, Serializable {
		private RemoteModel item;

		public OnItemClickListener(RemoteModel item) {
			this.item = item;
		}

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(context, PlayerActivity.class);
			intent.putExtra(PlayerActivity.PARAM_MODEL_ID, item.getId());
			intent.putExtra(PlayerActivity.PARAM_AUTHOR, item.getAuthor());
			intent.putExtra(PlayerActivity.PARAM_TITLE, item.getTitle());
			context.startActivity(intent);
		}
	}


}

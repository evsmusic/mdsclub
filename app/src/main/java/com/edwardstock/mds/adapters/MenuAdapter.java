package com.edwardstock.mds.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edwardstock.mds.R;

import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {

	private ArrayList<MenuItem> items = new ArrayList<>();
	private Context context;
	private LayoutInflater inflater;

	public MenuAdapter(Context context, ArrayList<MenuItem> items) {
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.items.addAll(items);
	}

	public MenuAdapter(Context context) {
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void add(MenuItem item) {
		this.items.add(item);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public MenuItem getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.indexOf(getItem(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		MenuItem item = getItem(position);

		if (view == null) {
			view = inflater.inflate(R.layout.menu_item, parent, false);
			holder = new ViewHolder();
			holder.title = (TextView) view.findViewById(R.id.menuTitle);
			holder.icon = (ImageView) view.findViewById(R.id.menuImage);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.icon.setImageResource(item.getIcon());
		holder.title.setText(item.getTitle());
		view.setOnClickListener(item.getClickListener());

		return view;
	}

	private static class ViewHolder {
		ImageView icon;
		TextView title;
	}

	public static class MenuItem {
		private int icon;
		private CharSequence title;
		private View.OnClickListener clickListener;

		public MenuItem(int resIconId, CharSequence title, View.OnClickListener listener) {
			this.icon = resIconId;
			this.title = title;
			this.clickListener = listener;
		}

		public MenuItem(int resIconId, CharSequence title) {
			this.icon = resIconId;
			this.title = title;
		}

		public CharSequence getTitle() {
			return this.title;
		}

		public int getIcon() {
			return this.icon;
		}

		public View.OnClickListener getClickListener() {
			return this.clickListener;
		}
	}

}

package com.edwardstock.mds.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edwardstock.mds.R;
import com.edwardstock.mds.helpers.TimeHelper;
import com.edwardstock.mds.helpers.UnitConverter;
import com.edwardstock.mds.models.LocalModel;

import java.util.ArrayList;

public class DownloadsAdapter extends BaseLocalListAdapter {
	private final static String TAG = "DOWNLOADS_ADAPTER";

	public DownloadsAdapter(Context context, ArrayList<LocalModel> models) {
		this.items = models;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public int size() {
		return items.size();
	}

	@Override
	public long getItemId(int i) {
		return items.get(i).getModelId();
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		LocalModel item = getItem(position);
		final ViewHolder holder;
		View view = convertView;
		if (view == null) {
			view = inflater.inflate(getListItemLayout(), parent, false);
			holder = new ViewHolder();
			initDefaultHolder(holder, view);
			holder.modelSize = (TextView) view.findViewById(R.id.size);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		fillDefaultHolder(holder, item);
		holder.modelSize.setText(UnitConverter.humanReadableByteCount(item.getSize(), true));
		holder.modelSize.setVisibility(View.VISIBLE);

		setItemIsListened(item.getIsListened(), holder);
		setItemIsFavorite(item.getIsFavorite(), holder);
		setItemIsPlaybackNow(item.getModelId(), holder);

		return view;
	}

	@Override
	protected void fillDefaultHolder(BaseListViewHolder holder, LocalModel data) {
		holder.title.setText(data.getTitle());
		holder.author.setText(data.getAuthor());
		holder.duration.setText(TimeHelper.getTimeString(Integer.parseInt(data.getDuration())));
	}

	static class ViewHolder extends BaseListViewHolder {
		TextView modelSize;
	}


}

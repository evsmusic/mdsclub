package com.edwardstock.mds;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardstock.mds.adapters.DownloadsAdapter;
import com.edwardstock.mds.helpers.UnitConverter;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.threads.ListItemProgressTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.content.DialogInterface.OnDismissListener;

public class DownloadsActivity extends BaseActivity implements OnDismissListener {
	public final static String PARAM_DOWNLOAD_MODEL_ID = "DOWNLOAD_MODEL_ID";
	private final static String TAG = "DownloadsActivity";
	private ListView listView;
	private DownloadsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_downloads);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.downloads_bar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		initMenu(toolbar, false);

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			int modelId = (int) bundle.getLong(PARAM_DOWNLOAD_MODEL_ID, -1);
			Log.d(TAG, "Input model id: " + modelId);
			//Перезапускаем загрузку файла в случае ошибки загрузки
			if (modelId >= 0) {
				this.reDownloadModel(modelId, true);
			}
		}

		this.loadModels();

		sendGAScreen();
	}

	public void reDownloadModel(int modelId, boolean finishActivity) {
		LocalModel model = LocalModel.getByModelId(modelId);
		model.deleteFile(true).downloadFile(this, DownloadsActivity.class, true);

		if (finishActivity) {
			finish();
		}
	}

	protected void loadModels() {
		new AsyncTask<Void, Void, List<LocalModel>>() {
			@Override
			protected List<LocalModel> doInBackground(Void... params) {
				return LocalModel.getDownloaded();
			}

			protected void onPostExecute(List<LocalModel> result) {
				super.onPostExecute(result);
				initMainList(new ArrayList<>(result));
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	protected void initMainList(ArrayList<LocalModel> items) {

		adapter = new DownloadsAdapter(this, items);

		listView = (ListView) findViewById(R.id.downloadListView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				final LocalModel item = adapter.getItem(position);
				if (item.fileIsDownloaded()) {
					Intent intent = new Intent(DownloadsActivity.this, PlayerActivity.class);
					intent.putExtra(PlayerActivity.PARAM_MODEL_ID, (int) id);
					startActivity(intent);
				} else {
					AlertDialog.Builder ad = new AlertDialog.Builder(DownloadsActivity.this);
					ad.setTitle("Модель не доступна оффлайн");
					ad.setMessage("Вы хотите загрузить модель и сделать ее доступной оффлайн?");
					ad.setPositiveButton("Да", new AlertDialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							reDownloadModel(item.getModelId(), false);
						}
					});

					ad.setNegativeButton("Нет", new AlertDialog.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {

						}
					});

					ad.setCancelable(true);
					ad.show();
				}
				listView.invalidate();
			}
		});

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
				DownloadParamsFragment fragment = DownloadParamsFragment.newInstance((int) id);
				fragment.show(getFragmentManager(), "downloadItemParamsFragment");
				return true;
			}
		});
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");
		Log.d(TAG, "Is playing: " + (isPlaying ? "true" : "false"));
		Log.d(TAG, "Service is null? " + (service == null ? "true" : "false"));

		if (listView != null && adapter != null) {
			listView.invalidateViews();
			adapter.notifyDataSetInvalidated();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		File sdCard = getExternalFilesDir(Environment.DIRECTORY_PODCASTS);
		if (sdCard != null) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.download_space_usage, null);

			((TextView) view.findViewById(R.id.spaceUsage)).setText("Свободное место: " + UnitConverter.humanReadableByteCount(sdCard.getFreeSpace(), false));
			menu.add("Занимаемое место").setActionView(view).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}

		getMenuInflater().inflate(R.menu.menu_downloads, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}

		if (id == R.id.downloads_clear) {
			clearDownloads();
		}

		return super.onOptionsItemSelected(item);
	}

	private void clearDownloads() {
		if (adapter.size() == 0) {
			Toast.makeText(this, "Нечего удалять", Toast.LENGTH_SHORT).show();
			return;
		}
		new AlertDialog.Builder(this)
			.setTitle("Удаление оффлайн моделей")
			.setMessage(getResources().getString(R.string.dialog_clear_downloads))
			.setPositiveButton("Да", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					List<LocalModel> downloads = LocalModel.getDownloaded();
					for (LocalModel item : downloads) {
						item.deleteFile(false);
						item.fileDownloaded = LocalModel.MODEL_NOT_DOWNLOADED;
						item.save();
					}
					listView.setAdapter(new DownloadsAdapter(DownloadsActivity.this, new ArrayList<>(LocalModel.getDownloaded())));
				}
			}).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// do nothing
			}
		}).show();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		listView.setAdapter(new DownloadsAdapter(this, new ArrayList<>(LocalModel.getDownloaded())));
	}

	public static class DownloadParamsFragment extends DialogFragment {
		public final static String PARAM_DOWNLOAD_ID = "DOWNLOAD_ID";
		private final static String TAG = "DownloadParamsFragment (DOWNLOAD_PARAMS_F)";

		private int id;

		public static DownloadParamsFragment newInstance(int downloadId) {
			DownloadParamsFragment fragment = new DownloadParamsFragment();
			final Bundle args = new Bundle(1);
			args.putInt(PARAM_DOWNLOAD_ID, downloadId);
			fragment.setArguments(args);
			Log.d(TAG, "New instance model id: " + downloadId);
			return fragment;
		}

		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			Log.d(TAG, "DownloadParamsFragment onCreate");

			this.id = getArguments().getInt(PARAM_DOWNLOAD_ID);
			Log.d(TAG, "onCreate model id: " + this.id);
		}

		@Override
		public void onDismiss(final DialogInterface dialog) {
			super.onDismiss(dialog);
			final Activity activity = getActivity();
			if (activity != null && activity instanceof OnDismissListener) {
				((OnDismissListener) activity).onDismiss(dialog);
			}
		}

		public void onActivityCreated(Bundle savedState) {
			super.onActivityCreated(savedState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.fragment_download_params, container, false);
			getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			String[] params = new String[]{
				"Удалить",
				"Загрузить заново"
			};

			ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, params);
			ListView paramsList = (ListView) view.findViewById(R.id.downloadParamsList);
			paramsList.setAdapter(adapter);
			paramsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
					switch (position) {
						case 0:
							Log.d(TAG, "Delete offline model: " + DownloadParamsFragment.this.id);
							deleteOfflineModel();
							break;
						case 1:
							Log.d(TAG, "Restart downloading model: " + DownloadParamsFragment.this.id);
							restartModelDownloading();
					}
				}
			});

			return view;
		}

		private void deleteOfflineModel() {
			LocalModel model = LocalModel.getByModelId(this.id);
			model.deleteFile().delete();
			LocalModel.delete(LocalModel.class, model.getId());

			dismiss();
		}

		public void restartModelDownloading() {
			final DownloadsActivity activity = (DownloadsActivity) getActivity();

			LocalModel model = LocalModel.getByModelId(this.id);
			model
				.deleteFile(true)
				.downloadFile(activity, DownloadsActivity.class, true);

			ListItemProgressTask task = new ListItemProgressTask(activity, this.id);
			task.setEventListener(new ListItemProgressTask.ProgressEventListener() {
				@Override
				public void onUpdate(int screenProgress, int screenTotalWidth, int progress, int total, int progressPercentage) {

				}

				@Override
				public void onComplete() {
					activity.loadModels();
				}

				@Override
				public void onError() {

				}
			});
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


			dismiss();
		}

	}
}

package com.edwardstock.mds.services;


import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.edwardstock.mds.R;
import com.edwardstock.mds.models.LocalModel;

public class PlayerForegroundNotificationBuilder {
	
	private Context context;
	private RemoteViews smallStatus, bigStatus;
	private boolean isPlaylist = false;
	private int nextModelId, prevModelId, modelId;
	private LocalModel model;
	
	public PlayerForegroundNotificationBuilder(Context context, LocalModel model) {
		this.context = context;		
		this.model = model;

		smallStatus = new RemoteViews(context.getPackageName(), R.layout.notification_player_small);
		bigStatus = new RemoteViews(context.getPackageName(), R.layout.notification_player_big);

		smallStatus.setTextViewText(R.id.small_notification_title, model.getTitle());
		smallStatus.setTextViewText(R.id.small_notification_author, model.author);
		buildBroadcast(PlayerService.OnCloseStatusListener.class, R.id.small_notification_close, smallStatus);
	}
	
	public void buildBroadcast(Class<? extends BroadcastReceiver> className, int viewId, RemoteViews views) {
		Intent intent = new Intent(context, className);
		PendingIntent contentIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
		views.setOnClickPendingIntent(viewId, contentIntent);
	}

	public void setIsPlaylist(boolean isPlaylist) {
		this.isPlaylist = isPlaylist;
	}
	
	public void createIntent() {
		if(!isPlaylist) {
			createLocalIntent();			
		} else {
			createQueueIntent();			
		}		
	}
	
	private void createLocalIntent() {
//		if (model.hasNextModel()) {
//			Intent nextIntent = new Intent(context, PlayerService.OnNextStatusListener.class);
//			nextIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) model.nextModelId);
//			Log.i(TAG, " -- Put BR extra next track id: " + model.nextModelId);
//			PendingIntent nextContentIntent = PendingIntent.getBroadcast(context, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//			bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next);
//			bigStatus.setOnClickPendingIntent(R.id.big_notification_next, nextContentIntent);
//		} else {
//			bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next_alpha);
//		}
//
//		//предыдущий трек
//		if ( model.hasPreviousModel()) {
//			Intent prevIntent = new Intent(context, PlayerService.OnPrevStatusListener.class);
//			prevIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) model.previousModelId);
//			//Log.i(TAG, " -- Put BR extra prev track id: " + model.previousModelId);
//			PendingIntent prevContentIntent = PendingIntent.getBroadcast(context, 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//			bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous);
//			bigStatus.setOnClickPendingIntent(R.id.big_notification_prev, prevContentIntent);
//		} else {
//			bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous_alpha);
//		}

	}
	
	private void createQueueIntent() {
//		//следующий трек
//		if ( queueHasNext ) {
//			Intent nextIntent = new Intent(context, PlayerService.OnNextStatusListener.class);
//			nextIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) queueItems.get(queuePosition+1));
//			Log.i(TAG, " -- Put BR extra next track id: " + (long) queueItems.get(queuePosition+1));
//			PendingIntent nextContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//			bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next);
//			bigStatus.setOnClickPendingIntent(R.id.big_notification_next, nextContentIntent);
//		} else {
//			bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next_alpha);
//		}
//
//		//предыдущий трек
//		if ( queueHasPrev ) {
//			Intent prevIntent = new Intent(context, PlayerService.OnPrevStatusListener.class);
//			prevIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) queueItems.get(queuePosition-1));
//			Log.i(TAG, " -- Put BR extra prev track id: " + (long) queueItems.get(queuePosition-1));
//			PendingIntent prevContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//			bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous);
//			bigStatus.setOnClickPendingIntent(R.id.big_notification_prev, prevContentIntent);
//		} else {
//			bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous_alpha);
//		}

	}
	
	public void setNextModelId(int modelId) {
		this.nextModelId = modelId;
	}
	
	public void setPrevModelId(int modelId) {
		this.prevModelId = modelId;
	}
	
	
}

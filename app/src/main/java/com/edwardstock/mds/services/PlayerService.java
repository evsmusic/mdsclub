package com.edwardstock.mds.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;

import com.edwardstock.mds.PlayerActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.helpers.Dumper;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.threads.ModelItemLoaderTask;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class PlayerService extends Service implements
	MediaPlayer.OnPreparedListener,
	MediaPlayer.OnCompletionListener,
	MediaPlayer.OnBufferingUpdateListener,
	MediaPlayer.OnErrorListener {

	private final static int ONGOING_NOTIFICATION_ID = 2112;
	private final static String TAG = "PlayerServer (SERVICE)";
	public static boolean prepared = false;
	private static boolean isRunning = false;
	private static PlayerState currentState = PlayerState.UNREADY;
	private TelephonyManager mgr;
	private PlayerBinder binder = new PlayerBinder();
	private MediaPlayer mediaPlayer = new MediaPlayer();
	private PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			if (state == TelephonyManager.CALL_STATE_RINGING) {
				//Incoming call: Pause music
				if (mediaPlayer != null && !currentState.equals(PlayerState.IDLE)) {
					mediaPlayer.pause();
				}
			} else if (state == TelephonyManager.CALL_STATE_IDLE) {
				//Not in call: Play music
				if (mediaPlayer != null && !currentState.equals(PlayerState.IDLE)) {
					mediaPlayer.start();
				}
			} else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
				//A call is dialing, active or on hold
				if (mediaPlayer != null && !currentState.equals(PlayerState.IDLE)) {
					mediaPlayer.pause();
				}
			}
			super.onCallStateChanged(state, incomingNumber);
		}
	};
	private LocalModel model;
	private ViewAppearanceListener viewAppearanceListener;
	private OnMediaPreparedListener mediaPreparedListener;

	public static boolean isRunning() {
		return isRunning;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate()");
		mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		if (mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		}


		//инициализируем аудио подсистему
		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		audioManager.setMode(AudioManager.STREAM_MUSIC);

		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.setOnCompletionListener(this);
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnErrorListener(this);
		setState(PlayerState.INITIALIZED);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		isRunning = true;
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy()");
		super.onDestroy();

		if (getPlayerState().equals(PlayerState.PREPARED)) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
				mediaPlayer.reset();
				setState(PlayerState.IDLE);
			}
		}

		if (mgr != null) {
			mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
		}

		isRunning = false;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public static PlayerState getPlayerState() {
		return currentState;
	}

	private static void setState(PlayerState state) {
		currentState = state;
		Log.d(TAG, "____PLAYER_SERVICE_SET_STATE:   " + state.toString());
	}

	@Override
	public void onPrepared(MediaPlayer mediaPlayer) {
		Log.d(TAG, "MediaPlayer onPrepared()");
		prepared = true;
		playPause();

		if (mediaPreparedListener != null) {
			mediaPreparedListener.onMediaPrepared(mediaPlayer);
		}

		setState(PlayerState.PREPARED);
	}

	public void playPause() {
		try {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				setState(PlayerState.PAUSED);
			} else {
				mediaPlayer.start();
				setState(PlayerState.STARTED);
			}
		} catch (IllegalStateException err) {
			setModel(model);
			return;
		}

		if (!viewAppearanceListener.getIsViewOn()) {
			startPlayerForeground();
		}
	}

	public void startPlayerForeground() {
		try {
			if (mediaPlayer.isPlaying())
				startForeground(ONGOING_NOTIFICATION_ID, buildForegroundNotification());
		} catch (IllegalStateException e) {
			stopForeground(true);
		}
	}

	public Notification buildForegroundNotification() {
		RemoteViews smallStatus = new RemoteViews(getPackageName(), R.layout.notification_player_small);
		smallStatus.setTextViewText(R.id.small_notification_title, model.getTitle());
		smallStatus.setTextViewText(R.id.small_notification_author, model.author);
		buildBroadcast(OnCloseStatusListener.class, R.id.small_notification_close, smallStatus);

		RemoteViews bigStatus = new RemoteViews(getPackageName(), R.layout.notification_player_big);
		bigStatus.setTextViewText(R.id.big_notification_title, model.getTitle());
		bigStatus.setTextViewText(R.id.big_notification_author, model.author);
		Log.d(TAG, "Building NB is playlist? " + BaseActivity.queueExists() + " queue has nex? " + BaseActivity.isQueueHasNextTrack() + " queue has prev? " + BaseActivity.isQueueHasNextTrack());
		if (!BaseActivity.queueExists()) {
			//следующий трек
			if (model.hasNextModel()) {
				Intent nextIntent = new Intent(getApplicationContext(), OnNextStatusListener.class);
				nextIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) model.nextModelId);
				Log.i(TAG, " -- Put BR extra next track id: " + model.nextModelId);
				PendingIntent nextContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next);
				bigStatus.setOnClickPendingIntent(R.id.big_notification_next, nextContentIntent);
			} else {
				bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next_alpha);
			}

			//предыдущий трек
			if (model.hasPreviousModel()) {
				Intent prevIntent = new Intent(getApplicationContext(), OnPrevStatusListener.class);
				prevIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) model.previousModelId);
				Log.i(TAG, " -- Put BR extra prev track id: " + model.previousModelId);
				PendingIntent prevContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous);
				bigStatus.setOnClickPendingIntent(R.id.big_notification_prev, prevContentIntent);
			} else {
				bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous_alpha);
			}
		} else {
			//следующий трек
			if (BaseActivity.isQueueHasNextTrack()) {
				Intent nextIntent = new Intent(getApplicationContext(), OnNextStatusListener.class);
				nextIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) BaseActivity.getNextQueueId());
				PendingIntent nextContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next);
				bigStatus.setOnClickPendingIntent(R.id.big_notification_next, nextContentIntent);
			} else {
				bigStatus.setInt(R.id.big_notification_next, "setBackgroundResource", R.drawable.ic_media_next_alpha);
			}

			//предыдущий трек
			if (BaseActivity.isQueueHasPrevTrack()) {
				Intent prevIntent = new Intent(getApplicationContext(), OnPrevStatusListener.class);
				prevIntent.putExtra(PlayerActivity.PARAM_MODEL_ID, (long) BaseActivity.getPrevQueueId());
				PendingIntent prevContentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous);
				bigStatus.setOnClickPendingIntent(R.id.big_notification_prev, prevContentIntent);
			} else {
				bigStatus.setInt(R.id.big_notification_prev, "setBackgroundResource", R.drawable.ic_media_previous_alpha);
			}
		}


		//кнопка воспроизведения
		if (getMediaPlayer().isPlaying()) {
			bigStatus.setInt(R.id.big_notification_play, "setBackgroundResource", R.drawable.ic_mds_pause);
		} else {
			bigStatus.setInt(R.id.big_notification_play, "setBackgroundResource", R.drawable.ic_mds_play);
		}

		buildBroadcast(OnPlayStatusListener.class, R.id.big_notification_play, bigStatus);
		buildBroadcast(OnCloseStatusListener.class, R.id.big_notification_close, bigStatus);

		Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
		intent.putExtra(PlayerActivity.PARAM_MODEL_ID, model.getModelId());
		intent.putExtra(PlayerActivity.PARAM_AUTHOR, model.getAuthor());
		intent.putExtra(PlayerActivity.PARAM_TITLE, model.getTitle());
		intent.putExtra(PlayerActivity.PARAM_QUEUE_ID, BaseActivity.playlistId);
		intent.putExtra(PlayerActivity.PARAM_QUEUE_POSITION, BaseActivity.getQueuePosition());
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
		builder.setOngoing(true);
		builder.setContentIntent(pendingIntent);
		builder.setContentTitle(model.getTitle());
		builder.setContentText(model.getAuthor());
		builder.setSmallIcon(R.drawable.ic_launcher_base);

		Notification n = builder.build();
		n.contentView = smallStatus;
		n.bigContentView = bigStatus;

		return n;
	}

	private void buildBroadcast(Class<? extends BroadcastReceiver> className, int viewId, RemoteViews views) {
		Intent intent = new Intent(getApplicationContext(), className);
		PendingIntent contentIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
		views.setOnClickPendingIntent(viewId, contentIntent);
	}

	public MediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	@Override
	public void onCompletion(MediaPlayer mediaPlayer) {
		Log.d(TAG, "Media player onCompletion(): " + String.valueOf(mediaPlayer.getCurrentPosition()));
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
			mediaPlayer.reset();
			mediaPlayer.release();
			setState(PlayerState.RELEASED);
		}

		LocalModel.setListened(model.getModelId(), true);
		LocalModel.setBookmark(model.getModelId(), 0);

		if (!BaseActivity.queueExists()) {
			if (model.hasNextModel()) {
				ModelItemLoaderTask task = new ModelItemLoaderTask(model.nextModelId);
				task.execute();
				try {
					model = task.get();
					BaseActivity.model = model;
					setModel(model);
					if (mediaPreparedListener != null) {
						mediaPreparedListener.onNextModelPlayback();
					}
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			} else {
				stopForeground(true);
			}
		} else {
			BaseActivity.validateQueueArrows();

			if (BaseActivity.isQueueHasNextTrack()) {
				ModelItemLoaderTask task = new ModelItemLoaderTask(BaseActivity.getNextQueueId());
				BaseActivity.incrementQueue();
				task.execute();
				try {
					model = task.get();
					BaseActivity.model = model;
					setModel(model);
					if (mediaPreparedListener != null) {
						mediaPreparedListener.onNextModelPlayback();
					}
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}

		}
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
		Log.d(TAG, "Buffering update (ps) " + i);
	}

	@Override
	public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
		Log.d(TAG, "OnError - Error code: " + what + " Extra code: " + extra);

		switch (what) {
			case -1004:
				Log.d("Streaming Media", "MEDIA_ERROR_IO");
				break;
			case -1007:
				Log.d("Streaming Media", "MEDIA_ERROR_MALFORMED");
				break;
			case 200:
				Log.d("Streaming Media", "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK");
				break;
			case 100:
				Log.d("Streaming Media", "MEDIA_ERROR_SERVER_DIED");
				break;
			case -110:
				Log.d("Streaming Media", "MEDIA_ERROR_TIMED_OUT");
				break;
			case 1:
				Log.d("Streaming Media", "MEDIA_ERROR_UNKNOWN");
				break;
			case -1010:
				Log.d("Streaming Media", "MEDIA_ERROR_UNSUPPORTED");
				break;

			case -38:
				Log.d("Steaming Media", "MEDIA_PLAYER_NOT_PREPARED");

				if (this.model != null) {
					Log.d(TAG, "Try to set audio source again");
					setModel(this.model);
				} else {
					Log.d(TAG, "Model is already null. Can't prepare audio source");
				}
				break;

			case 703:
				Log.w("Streaming Media", "Bandwidth in recent past");

		}

		switch (extra) {
			case 800:
				Log.d("Streaming Media", "MEDIA_INFO_BAD_INTERLEAVING");
				break;
			case 702:
				Log.d("Streaming Media", "MEDIA_INFO_BUFFERING_END");
				break;
			case 701:
				Log.d("Streaming Media", "MEDIA_INFO_METADATA_UPDATE");
				break;
			case 802:
				Log.d("Streaming Media", "MEDIA_INFO_METADATA_UPDATE");
				break;
			case 801:
				Log.d("Streaming Media", "MEDIA_INFO_NOT_SEEKABLE");
				break;
			case 1:
				Log.d("Streaming Media", "MEDIA_INFO_UNKNOWN");
				break;
			case 3:
				Log.d("Streaming Media", "MEDIA_INFO_VIDEO_RENDERING_START");
				break;
			case 700:
				Log.d("Streaming Media", "MEDIA_INFO_VIDEO_TRACK_LAGGING");
				break;

		}

		return true;
	}

	public void setViewAppearanceListener(ViewAppearanceListener listener) {
		this.viewAppearanceListener = listener;
	}

	public void setMediaPreparedListener(OnMediaPreparedListener listener) {
		this.mediaPreparedListener = listener;
	}

	public int getDuration() {
		if (currentState.equals(PlayerState.IDLE)) {
			return 0;
		}
		return mediaPlayer.getDuration();
	}

	public int getCurrentPosition() {
		if (currentState.equals(PlayerState.IDLE)) {
			return 0;
		}

		return mediaPlayer.getCurrentPosition();
	}

	public LocalModel getModel() {
		return this.model;
	}

	public void setModel(LocalModel model) {
		this.model = model;

		try {
			Log.d(TAG, "Model source: " + model.getSource());
			try {
				mediaPlayer.reset();
				setState(PlayerState.IDLE);
			} catch (IllegalStateException er) {
				er.printStackTrace();
				mediaPlayer = new MediaPlayer();
				AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
				audioManager.setMode(AudioManager.STREAM_MUSIC);

				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.setOnCompletionListener(this);
				mediaPlayer.setOnPreparedListener(this);
				mediaPlayer.setOnErrorListener(this);
				setState(PlayerState.INITIALIZED);
			}

			mediaPlayer.setDataSource(model.getSource());
			mediaPlayer.prepareAsync();
			isRunning = true;

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stopPlayerForeground() {
		stopForeground(true);
	}

	public void seekTo(int position) {
		mediaPlayer.seekTo(position);
	}

	public enum PlayerState {
		UNREADY,
		IDLE,
		INITIALIZED,
		PREPARED,
		STARTED,
		STOPPED,
		PAUSED,
		COMPLETED,
		RELEASED
	}

	public interface ViewAppearanceListener {
		boolean getIsViewOn();
	}

	public interface OnMediaPreparedListener {
		void onMediaPrepared(MediaPlayer mediaPlayer);

		void onNextModelPlayback();
	}

	public static class OnPlayStatusListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			PlayerService service = BaseActivity.service;
			if (service.getMediaPlayer().isPlaying()) {
				service.getMediaPlayer().pause();
			} else {
				service.getMediaPlayer().start();
			}
			NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
			nm.notify(PlayerService.ONGOING_NOTIFICATION_ID, service.buildForegroundNotification());
		}
	}

	public static class OnNextStatusListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, Dumper.dumpBundleExtras(intent));
			int modelId = (int) intent.getLongExtra(PlayerActivity.PARAM_MODEL_ID, 0);
			ModelItemLoaderTask task = new ModelItemLoaderTask(modelId);
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			try {
				LocalModel model = task.get();
				BaseActivity.model = model;
				BaseActivity.incrementQueue();
				BaseActivity.validateQueueArrows();
				BaseActivity.service.setModel(model);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}

			NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
			nm.notify(PlayerService.ONGOING_NOTIFICATION_ID, BaseActivity.service.buildForegroundNotification());
		}
	}

	public static class OnPrevStatusListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, Dumper.dumpBundleExtras(intent));
			int modelId = (int) intent.getLongExtra(PlayerActivity.PARAM_MODEL_ID, 0);
			ModelItemLoaderTask task = new ModelItemLoaderTask(modelId);
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			try {
				LocalModel model = task.get();
				BaseActivity.model = model;
				BaseActivity.decrementQueue();
				BaseActivity.validateQueueArrows();
				BaseActivity.service.setModel(model);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}

			NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
			nm.notify(PlayerService.ONGOING_NOTIFICATION_ID, BaseActivity.service.buildForegroundNotification());
		}
	}

	public static class OnCloseStatusListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			PlayerService service = BaseActivity.service;
			if (service == null)
				return;

			PlayerService.prepared = false;
			PlayerService.isRunning = false;
			if (service.getMediaPlayer().isPlaying()) {
				service.getMediaPlayer().stop();
				service.getMediaPlayer().reset();
			}

			service.stopPlayerForeground();
			//service.stopSelf();
		}
	}

	public class PlayerBinder extends Binder {
		public PlayerService getService() {
			return PlayerService.this;
		}
	}
}

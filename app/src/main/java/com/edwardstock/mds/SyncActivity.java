package com.edwardstock.mds;

import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardstock.mds.api.SyncClient;
import com.edwardstock.mds.implementations.BaseActivity;
import com.google.android.gms.common.AccountPicker;

import java.io.IOException;

/**
 * Синхронизация данных с сервером происходит тута
 */
public class SyncActivity extends BaseActivity implements View.OnClickListener, SyncClient.SyncProgressListener {

	private final static int REQUEST_CODE_PICK_ACCOUNT = 1000;
	private final static String TAG = "OAuth";

	private SyncClient syncClient;
	private String mEmail;
	private boolean authenticated = false;

	private TextView syncDate;
	private Button syncNow;
	private ProgressDialog progressDialog;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sync);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.main_bar);
		initMenu(toolbar, false);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		syncDate = (TextView) findViewById(R.id.last_sync_date);
		syncNow = (Button) findViewById(R.id.sync_now);

		syncClient = new SyncClient(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setIndeterminate(true);
		progressDialog.setTitle(R.string.sync_on);

		if (syncClient.getLastSyncTime() == null) {
			syncDate.setText("Нет");
		} else {
			syncDate.setText(syncClient.getLastSyncTime());
		}

        /*
         Если пользователь еще не авторизовался/зарегался то заставляем его это сделать
         */
		if (!syncClient.userAuthenticated()) {
			String[] accountTypes = new String[]{"com.google"};
			Intent intent = AccountPicker.newChooseAccountIntent(null, null, accountTypes, false, null, null, null, null);
			startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
		} else {
			syncNow.setOnClickListener(SyncActivity.this);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
			if (resultCode == RESULT_OK) {
				mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				syncNow.setOnClickListener(this);
				authenticate();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, "Вы не выбрали аккаунт для авторизации", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

	private void authenticate() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					syncClient.setEmail(mEmail);
					authenticated = syncClient.authenticate();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}			protected void onPreExecute() {
				super.onPreExecute();
				progressDialog.setMessage("Авторизация на сервере...");
				progressDialog.show();
			}



			protected void onPostExecute(Void res) {
				progressDialog.dismiss();
				if (authenticated) {
					syncNow.setOnClickListener(SyncActivity.this);
				}
				super.onPostExecute(res);
			}
		}.execute();
	}

	@Override
	public void onClick(View v) {
		progressDialog.setMessage("Синхронизация данных");
		//syncClient.setSyncProgressListener(this);
		if (v.getId() == R.id.sync_now) {
			new AsyncTask<Void, Object, Void>() {
				protected void onPreExecute() {
					super.onPreExecute();
					progressDialog.show();
				}

				@Override
				protected Void doInBackground(Void... params) {
					try {
						syncClient.syncWithServer();

					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}

				protected void onPostExecute(Void res) {
					super.onPostExecute(res);
					progressDialog.dismiss();
					syncDate.setText(syncClient.getLastSyncTime());
				}
			}.execute();

		}
	}

	@Override
	public void onUpdate(int progress, int total, SyncClient.SyncType type) {
		switch (type) {
			case SYNC_IN:
				progressDialog.setMessage("Загрузка " + progress + " из " + total);
				break;
			case SYNC_OUT:
				progressDialog.setMessage("Отправка данных...");
				break;
			case SYNC_NONE:
				progressDialog.setMessage("Нет данных для синхронизации");
				break;
		}
	}

	@Override
	public void onComplete() {
		progressDialog.dismiss();
		syncDate.setText(syncClient.getLastSyncTime());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}

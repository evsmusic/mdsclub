package com.edwardstock.mds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.fragments.PlaylistSelectorDialog;
import com.edwardstock.mds.fragments.TimePickerFragment;
import com.edwardstock.mds.helpers.TimeHelper;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.models.PlaylistItem;
import com.edwardstock.mds.services.PlayerService;
import com.edwardstock.mds.threads.ModelItemLoaderTask;
import com.edwardstock.mds.threads.SyncTask;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class PlayerActivity extends BaseActivity implements
	SeekBar.OnSeekBarChangeListener,
	ModelItemLoaderTask.OnProcessListener,
	SearchView.OnQueryTextListener,
	View.OnClickListener,
	PlayerService.OnMediaPreparedListener {
	// Входные параметры для PlayerActivity (intent extras)
	public final static String PARAM_MODEL_ID = "PARAM_MODEL_ID";
	public final static String PARAM_TITLE = "PARAM_TITLE";
	public final static String PARAM_AUTHOR = "PARAM_AUTHOR";
	public final static String PARAM_START_FROM_TIME_POSITION = "PARAM_START_FROM_TIME_POSITION";
	public final static String PARAM_START_FROM_TIME_DURATION = "PARAM_START_FROM_TIME_DURATION";
	public static final String PARAM_QUEUE_ID = "PARAM_QUEUE_ID";
	public static final String PARAM_QUEUE_POSITION = "PARAM_QUEUE_POSITION";
	private final static String TAG = "PlayerActivity (PLAYER)";
	//===============================
	// Вьюхи плеера
	private TextView title;
	private TextView author;
	private TextView timeProgress;
	private TextView timeTotal;
	private ToggleButton playPauseButton;
	private Button nextTrack;
	private Button previousTrack;
	private ProgressBar loadProgress;
	private SeekBar seekBar;
	//===============================

	private boolean isNewModel = true;

	private Animation fadeIn;
	private Animation fadeOut;
	private Menu menu;

	private Handler seekBarHandler;
	private Runnable seekBarRunnable;
	private boolean mediaPrepared = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);
		Toolbar toolbar = findAndSetSupportActionBar(R.id.player_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		setTheme(R.style.AppTheme_PlayerTheme);

		LayoutInflater inflater = getLayoutInflater();
		View playerUnderbar = inflater.inflate(R.layout.player_underbar, null);
		toolbar.addView(playerUnderbar);


		initButtons();
		initTrackInfoViews();
		initProgressBar();

		if (!PlayerService.isRunning()) {
			startService(new Intent(this, PlayerService.class));
		}

		bindPlayerService();


		//получаем айдишник модели
		int incomeModelId;
		if (savedInstanceState != null && savedInstanceState.containsKey(PARAM_MODEL_ID)) {
			incomeModelId = savedInstanceState.getInt(PARAM_MODEL_ID);
		} else {
			incomeModelId = getIntent().getExtras().getInt(PARAM_MODEL_ID);
		}

		playlistId = getIntent().getExtras().getLong(PARAM_QUEUE_ID, -1);
		queuePosition = getIntent().getExtras().getInt(PARAM_QUEUE_POSITION, 0);
		if (playlistId > -1) {
			ArrayList<PlaylistItem> items = Playlist.findByPk(playlistId).getItems();
			queueItems = new ArrayList<>(items.size());
			for (PlaylistItem item : items) {
				queueItems.add(item.modelId);
			}
		} else {
			clearQueue();
		}


		this.author.setText(getIntent().getExtras().getString(PARAM_AUTHOR));
		this.title.setText(getIntent().getExtras().getString(PARAM_TITLE));

		lastPosition = getIntent().getExtras().getInt(PARAM_START_FROM_TIME_POSITION, 0);
		lastDuration = getIntent().getExtras().getInt(PARAM_START_FROM_TIME_DURATION, 0);


		modelId = incomeModelId;
		preparePlayer(incomeModelId);

		sendGAScreen();
	}

	/**
	 * Инициализация кнопок
	 */
	private void initButtons() {
		//init
		nextTrack = (Button) findViewById(R.id.nextTrack);
		previousTrack = (Button) findViewById(R.id.previousTrack);
		Button seekBack = (Button) findViewById(R.id.seekBack);
		Button seekForward = (Button) findViewById(R.id.seekForward);
		playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);

		//action assigning
		seekBack.setOnClickListener(this);
		seekForward.setOnClickListener(this);
	}

	/**
	 * Инициализируем общие плееровский вьюхи
	 */
	private void initTrackInfoViews() {
		if (title != null)
			return;

		title = (TextView) findViewById(R.id.model_title);
		author = (TextView) findViewById(R.id.model_author);
		timeProgress = (TextView) findViewById(R.id.timeProgress);
		timeTotal = (TextView) findViewById(R.id.timeTotal);
	}

	private void initProgressBar() {
		initProgressBar(false);
	}

	/**
	 * Подготовка плеера к воспроизведению (загрузка модели, выставление флагов и тд)
	 *
	 * @param incomeModelId
	 */
	protected void preparePlayer(int incomeModelId) {
		//грузим модельку
		if (model == null) {
			showLoadProgress();
			Log.d(TAG, "Model is null OR old model is not a new model: MODEL ID: " + incomeModelId);
			loadModel(incomeModelId);
			isNewModel = true;
		} else if (model.getModelId() != incomeModelId) {
			Log.d(TAG, "MODEL ID: " + incomeModelId + "; old model id: " + model.getModelId());
			showLoadProgress();
			loadModel(incomeModelId);
			isNewModel = true;
			try {
				service.getMediaPlayer().stop();
				service.getMediaPlayer().reset();
			} catch (IllegalStateException err) {
				Log.d(TAG, "IllegalStateException for MediaPlayer. Player is already in standby mode. Set new source for continuing playback");
			}

		} else {
			Log.d(TAG, "Model is not null and old model ID equals new model id " + incomeModelId);
			isNewModel = false;
			mediaPrepared = true;

			validateButtons();
			onPreTaskExecute();
			onPostTaskExecute(model);
			playPauseButton.setChecked(service.getMediaPlayer().isPlaying());
			startProgressBar();

			if (lastPosition > 0 && lastDuration > 0 && !service.getMediaPlayer().isPlaying()) {
				seekBar.setMax(lastDuration);
				seekBar.setProgress(lastPosition);
				timeProgress.setText(TimeHelper.getTimeString(lastPosition));
				timeTotal.setText(TimeHelper.getTimeString(lastDuration));
			}
		}
	}

	private void initProgressBar(boolean forced) {
		//прокрутка
		if (seekBar == null || forced) {
			seekBar = (SeekBar) findViewById(R.id.progressBar);
			seekBar.setActivated(true);
			seekBar.setEnabled(true);
			seekBar.setOnSeekBarChangeListener(this);
		}
	}

	/**
	 * Показываем загрузки модели
	 * (скрывает кнопку воспроизведения и отображает circular progress bar)
	 */
	private void showLoadProgress() {
		if (loadProgress == null)
			loadProgress = (ProgressBar) findViewById(R.id.loadProgress);

		loadProgress.setVisibility(View.VISIBLE);
		playPauseButton.setVisibility(View.GONE);
	}

	private void loadModel(int modelId) {
		ModelItemLoaderTask task = new ModelItemLoaderTask(this, modelId);
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	/**
	 * Валидируем кнопки управления, если управление не требуется то фейдим в 30% из 100%
	 */
	private void validateButtons() {
		if (!queueExists()) {
			if (model.hasNextModel()) {
				nextTrack.setAlpha(1f);
				nextTrack.setEnabled(true);
				nextTrack.setOnClickListener(this);
			} else {
				nextTrack.setEnabled(false);
				nextTrack.setAlpha(0.3f);
			}

			if (model.hasPreviousModel()) {
				previousTrack.setAlpha(1f);
				previousTrack.setEnabled(true);
				previousTrack.setOnClickListener(this);
			} else {
				previousTrack.setEnabled(false);
				previousTrack.setAlpha(0.3f);
			}
		} else {
			if (isQueueHasNextTrack()) {
				nextTrack.setAlpha(1f);
				nextTrack.setEnabled(true);
				nextTrack.setOnClickListener(this);
			} else {
				nextTrack.setEnabled(false);
				nextTrack.setAlpha(0.3f);
			}

			if (isQueueHasPrevTrack()) {
				previousTrack.setAlpha(1f);
				previousTrack.setEnabled(true);
				previousTrack.setOnClickListener(this);
			} else {
				previousTrack.setEnabled(false);
				previousTrack.setAlpha(0.3f);
			}
		}
	}

	@Override
	public void onPreTaskExecute() {
	}

	/**
	 * Запускаем асинхронную задачу для выставления текущего времени воспроизведения
	 * и прогрессбара
	 */
	private void startProgressBar() {
		if (!mediaPrepared || seekBarHandler != null)
			return;

		seekBarHandler = new Handler();
		seekBarHandler.post(this.seekBarRunnable = new Runnable() {
			@Override
			public void run() {
				try {
					seekBar.setMax(service.getDuration());
					timeTotal.setText(TimeHelper.getTimeString(service.getDuration()));
					if (service.getMediaPlayer().isPlaying()) {
						int current = service.getCurrentPosition();
						seekBar.setProgress(current);
						timeProgress.setText(TimeHelper.getTimeString(current));
					}
				} catch (IllegalStateException e) {
					int current = 0;
					seekBar.setProgress(current);
					timeProgress.setText(TimeHelper.getTimeString(current));
					seekBar.setMax(0);
					timeTotal.setText(TimeHelper.getTimeString(0));
				}

				seekBarHandler.postDelayed(seekBarRunnable, 1000);
			}
		});
	}

	/**
	 * Заполняем общие плееровский вьюхи
	 */
	protected void setTrackInfoData(final LocalModel model) {
		if (title == null) {
			initTrackInfoViews();
		}
		title.setText(model.getTitle());
		author.setText(model.getAuthor());
	}

	protected void afterServiceConnection(PlayerService service) {
		service.setViewAppearanceListener(this);
		service.setMediaPreparedListener(this);
		Log.d(TAG, "After service connection");
		if (model != null && isNewModel) {
			service.setModel(model);
		}
		initProgressBar(!isNewModel);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
			case android.R.id.home:
				finish();
				break;

			default:
				return true;
		}
		return true;
	}

	@Override
	protected void onStop() {
		super.onStop();
		isViewOn = false;

		if (service != null) {
			service.startPlayerForeground();
		}

		lastDuration = seekBar.getMax();
		lastPosition = seekBar.getProgress();

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		boolean useAutoBookmark = sp.getBoolean(SettingsActivity.Settings.PARAM_AUTO_BOOKMARK__STATE, true);

		if (useAutoBookmark) {
			int saveFromTime = Integer.parseInt(sp.getString(SettingsActivity.Settings.PARAM_AUTO_BOOKMARK__MIN_TIME, "15"));

			if (lastPosition >= saveFromTime) {
				LocalModel.setBookmark(modelId, lastPosition);
			}
		}


		unBindService();
		stopProgressBar();
		new SyncTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
		isViewOn = false;
	}

	@Override
	protected void onStart() {
		super.onStart();
		isViewOn = true;
		if (service != null) {
			service.stopPlayerForeground();
			setTrackInfoData(model);
			try {
				playPauseButton.setChecked(
					service.getMediaPlayer().isPlaying());
			} catch (IllegalStateException err) {
				playPauseButton.setChecked(false);
			}

			startProgressBar();
		}

		if (model != null) {
			validateButtons();
		}

		new SyncTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public void openRandom(View view) {
		if (service.getMediaPlayer().isPlaying()) {
			service.getMediaPlayer().pause();
		}

		try {
			int mId = ApiClient.getRandomId();
			isNewModel = true;
			showAnimatedLoadProgress();
			modelId = mId;
			getIntent().getExtras().remove(PARAM_MODEL_ID);
			getIntent().getExtras().putInt(PARAM_MODEL_ID, mId);
			lastDuration = 0;
			lastPosition = 0;

			loadModel(mId);
			model = null;
			if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
				drawerLayout.closeDrawers();
			}
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPostTaskExecute(LocalModel model) {
		Log.d(TAG, "onPostTaskExecute()");
		BaseActivity.model = model;
		modelId = BaseActivity.model.getModelId();
		Log.d(TAG, "______SET MODEL: " + BaseActivity.model.getModelId());
		setTrackInfoData(model);
		validateButtons();

		if (service != null)
			afterServiceConnection(service);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		startProgressBar();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.menu = menu;
		addListenedToggleMenuItem(menu);
		addBookmarkPlaybackMenuItem(menu);
		addBookmarkSetMenuItem(menu);
		addFavoriteToggleMenuItem(menu);
		addDownloadMenuItem(menu);
		addSearchMenuItem(menu);
		addPlaylistSelector(menu);
		addTimerMenuItem(menu);

		menu.add("Мои загрузки")
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem menuItem) {
					startActivity(new Intent(PlayerActivity.this, DownloadsActivity.class));
					return true;
				}
			})
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		this.menu = menu;
		try {
			LocalModel m = LocalModel.getByModelId(modelId);
			setAlphaMenuItem(menu, 0, m.getIsListened());
			setAlphaMenuItem(menu, 1, m.hasBookmark());
			setAlphaMenuItem(menu, 2, m.hasBookmark());
			setAlphaMenuItem(menu, 3, m.getIsFavorite());
			setAlphaMenuItem(menu, 4, m.getIsDownloaded());
		} catch (NullPointerException er) {
			Log.d(TAG, "Model is not ready yet...");
		}

		return true;
	}

	private void addListenedToggleMenuItem(Menu menu) {
		menu.add("Отметить как \"прослушанное\"").setIcon(R.drawable.ic_hearing_white_48dp)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					LocalModel m = LocalModel.getByModelId(modelId);

					if (m != null) {
						if (m.getIsListened()) {
							LocalModel.setListened(model.getModelId(), false);
							item.getIcon().setAlpha(0x7F);
						} else {
							LocalModel.setListened(model.getModelId(), true);
							item.getIcon().setAlpha(0xFF);
							Toast.makeText(PlayerActivity.this, "Отмечено как \"прослушано\"", Toast.LENGTH_SHORT).show();
						}
					}

					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	private void addBookmarkPlaybackMenuItem(Menu menu) {
		menu
			.add("Воспроизвести с закладки")
			.setIcon(R.drawable.ic_action_bookmark_play)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if (LocalModel.getByModelId(modelId).hasBookmark() && service != null) {
						item.getIcon().setAlpha(0xFF);
						service.seekTo(model.bookmarkPosition);
						if (!service.getMediaPlayer().isPlaying()) {
							onClickPlayPause(playPauseButton);
						}
						Toast.makeText(PlayerActivity.this, "Началось воспроизведение с закладки", Toast.LENGTH_SHORT).show();
					} else {
						item.getIcon().setAlpha(0x7F);
						Toast.makeText(PlayerActivity.this, "Закладка не установлена", Toast.LENGTH_SHORT).show();
					}
					return true;
				}
			})
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	private void addBookmarkSetMenuItem(Menu menu) {
		menu.add("Установить закладку").setIcon(R.drawable.ic_action_bookmark_add)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					LocalModel.setBookmark(model.getModelId(), service.getCurrentPosition());
					Toast.makeText(
						PlayerActivity.this,
						"Добавлена закладка на моменте: " + TimeHelper.getTimeString(service.getCurrentPosition()),
						Toast.LENGTH_SHORT
					).show();
					if (LocalModel.getByModelId(modelId).hasBookmark() && service != null) {
						item.getIcon().setAlpha(0xFF);
					} else {
						item.getIcon().setAlpha(0x7F);
					}
					return true;
				}
			})
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	private void addFavoriteToggleMenuItem(Menu menu) {
		menu.add("Добавить в избранное").setIcon(R.drawable.ic_action_rating)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if (LocalModel.getByModelId(modelId).getIsFavorite()) {
						item.getIcon().setAlpha(0x7F);
						LocalModel.setFavorite(model.getModelId(), false);
						Toast.makeText(PlayerActivity.this, "Модель удалена из избранного", Toast.LENGTH_SHORT).show();
					} else {
						item.getIcon().setAlpha(0xFF);
						LocalModel.setFavorite(model.getModelId(), true);
						Toast.makeText(PlayerActivity.this, "Модель добавлена в избранное", Toast.LENGTH_SHORT).show();
					}
					return true;
				}
			})
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	private void addDownloadMenuItem(Menu menu) {
		final int icon;
		final LocalModel m = LocalModel.getByModelId(modelId);
		if (m != null && m.getIsDownloaded()) {
			icon = R.drawable.ic_cloud_done_white_48dp;
		} else {
			icon = R.drawable.ic_cloud_download_white_48dp;
		}

		menu.add("Скачать").setIcon(icon)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					if (m != null && m.fileIsDownloaded()) {
						item.setEnabled(false);
						Toast.makeText(PlayerActivity.this, "Модель уже загружена", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(PlayerActivity.this, "Загрузка модели началась...", Toast.LENGTH_SHORT).show();
						downloadModel(null);
						item.setEnabled(false);
					}

					item.getIcon().setAlpha(0xFF);
					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	private void addSearchMenuItem(Menu menu) {
		menu.add("Поиск").setIcon(R.drawable.ic_search_white_48dp)
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					final EditText input = new EditText(getSupportActionBar().getThemedContext());
					new AlertDialog.Builder(getSupportActionBar().getThemedContext())
						.setTitle("Поиск моделей")
						.setMessage("Введите имя автора или название книги")
						.setIcon(R.drawable.ic_search_white_48dp)
						.setView(input)
						.setPositiveButton("Поиск", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent(PlayerActivity.this, CatalogActivity.class);
								intent.putExtra(CatalogActivity.PARAM_SEARCH_TEXT, input.getText().toString().trim());
								startActivity(intent);
							}
						})
						.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// nothing to do
							}
						}).show();

					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
	}

	private void addPlaylistSelector(Menu menu) {

		menu.add("Добавить в плейлист")
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					PlaylistSelectorDialog dialog = PlaylistSelectorDialog.newInstance(modelId);
					dialog.show(getFragmentManager(), "playlist_selector");

					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
	}

	private void addTimerMenuItem(final Menu menu) {
		menu.add("Таймер сна")
			.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					TimePickerFragment newFragment = new TimePickerFragment();
					newFragment.show(getFragmentManager(), "timePicker");

					return true;
				}
			}).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
	}

	/**
	 * Обработчик нажатия кнопки "воспроизведение/пауза"
	 *
	 * @param view
	 */
	public void onClickPlayPause(View view) {
		try {
			service.playPause();
			setPlayPauseStateImage(service.getMediaPlayer().isPlaying());
		} catch (IllegalStateException err) {
			unBindService();
			stopService(intent);
			startService(intent);
			bindPlayerService();
		}

		try {
			if (service.getMediaPlayer().isPlaying()) {
				hidePlaybackNow();
			} else {
				showPlaybackNow();
			}
		} catch (IllegalStateException e) {
			hidePlaybackNow();
		}
	}

	public void downloadModel(View view) {
		model.downloadFile(PlayerActivity.this, DownloadsActivity.class, true, null);
		if (view != null)
			view.setAlpha(1f);
	}

	/**
	 * Устанавливает изображение на кнопку воспроизведения в плеере
	 * Если нажата кнопка, то отображается иконка паузы и наоборот
	 *
	 * @param checked
	 */
	protected void setPlayPauseStateImage(boolean checked) {
		if (playPauseButton == null) {
			playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);
		}

		playPauseButton.setChecked(checked);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause()");
		isViewOn = false;

		if (service != null) {
			service.startPlayerForeground();
		}

		lastDuration = seekBar.getMax();
		lastPosition = seekBar.getProgress();
		stopProgressBar();

		unBindService();
		new SyncTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	/**
	 * Останавливаем асинхронную задачу по выставлению текущего времени
	 */
	private void stopProgressBar() {
		if (seekBarHandler == null || seekBarRunnable == null)
			return;

		seekBarHandler.removeCallbacks(this.seekBarRunnable);
		seekBarRunnable = null;
		seekBarHandler = null;
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (service != null) {
			service.stopPlayerForeground();
			startProgressBar();
		}

		if (model != null) {
			validateButtons();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(PARAM_MODEL_ID, BaseActivity.model.getModelId());
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		try {
			if (fromUser) {
				timeProgress.setText(TimeHelper.getTimeString(service.getCurrentPosition()));
				service.seekTo(progress);
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public boolean onQueryTextSubmit(String s) {
		Intent intent = new Intent(this, CatalogActivity.class);
		intent.putExtra(CatalogActivity.PARAM_SEARCH_TEXT, s.trim());
		startActivity(intent);
		return true;
	}

	@Override
	public boolean onQueryTextChange(String s) {
		return false;
	}

	@Override
	public void onClick(View view) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		int seekStep = Integer.parseInt(sp.getString(SettingsActivity.Settings.PARAM_SEEK_STEP, "10"));
		switch (view.getId()) {
			case R.id.nextTrack:
				if (service.getMediaPlayer().isPlaying()) {
					service.getMediaPlayer().pause();
				}
				isNewModel = true;
				showAnimatedLoadProgress();
				if (!queueExists()) {
					loadModel(model.nextModelId);
				} else {
					loadModel(getNextQueueId());
					incrementQueue();
				}

				break;

			case R.id.previousTrack:
				if (service.getMediaPlayer().isPlaying()) {
					service.getMediaPlayer().pause();
				}
				isNewModel = true;
				showAnimatedLoadProgress();
				if (!queueExists()) {
					loadModel(model.previousModelId);
				} else {
					loadModel(getPrevQueueId());
					decrementQueue();
				}
				break;

			case R.id.seekBack:
				if (service != null) {
					if (service.getCurrentPosition() < (seekStep * 1000)) {
						service.getMediaPlayer().seekTo(0);
					} else {
						try {
							int currentPosition = service.getCurrentPosition();
							service.getMediaPlayer().seekTo(currentPosition - (seekStep * 1000));
						} catch (IllegalStateException e) {
							e.printStackTrace();
						}

					}
				}
				break;

			case R.id.seekForward:
				if (service != null) {
					try {
						int duration = service.getDuration();
						int currentPosition = service.getCurrentPosition();
						int seekToTime = currentPosition + (seekStep * 1000);

						if (seekToTime > duration) {
							service.seekTo(duration);
							onClickPlayPause(playPauseButton);
							timeProgress.setText(TimeHelper.getTimeString(duration));
						} else {
							service.seekTo(seekToTime);
							timeProgress.setText(TimeHelper.getTimeString(seekToTime));
						}
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
				}
				break;
		}
	}

	/**
	 * Показываем анимашку загрузки модели
	 */
	private void showAnimatedLoadProgress() {
		initFadeAnimations();
		if (loadProgress == null)
			loadProgress = (ProgressBar) findViewById(R.id.loadProgress);

		playPauseButton.startAnimation(fadeOut);
		playPauseButton.setVisibility(View.GONE);

		loadProgress.setVisibility(View.VISIBLE);
		loadProgress.startAnimation(fadeIn);
	}

	/**
	 * Инициализация анимашек
	 */
	private void initFadeAnimations() {
		if (fadeOut != null)
			return;

		fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
		fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
	}

	@Override
	public void onMediaPrepared(MediaPlayer mediaPlayer) {
		Log.d(TAG, "Media prepared");
		if (isNewModel) {
			hideLoadProgress();
		}
		onPrepareOptionsMenu(this.menu);
		mediaPrepared = true;
		showPlaybackNow();
		setPlayPauseStateImage(true);
		startProgressBar();

		try {
			timeTotal.setText(TimeHelper.getTimeString(service.getDuration()));
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		String behaviorVal = sp.getString(SettingsActivity.Settings.PARAM_BM_BEHAVIOR, SettingsActivity.Settings.PARAM_BM__POSITION_ASK);

		if (model.hasBookmark()) {
			service.playPause();
			switch (behaviorVal) {
				case SettingsActivity.Settings.PARAM_BM__POSITION_ASK:

					AlertDialog.Builder ad = new AlertDialog.Builder(this);
					ad.setTitle("Воспроизвести с закладки?");
					ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							service.seekTo(model.bookmarkPosition);
							service.playPause();
						}
					});

					ad.setNegativeButton("Нет, начать сначала", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							service.seekTo(0);
							service.playPause();
						}
					});

					ad.show();
					break;
				case SettingsActivity.Settings.PARAM_BM__POSITION_NO_ASK:
					service.seekTo(model.bookmarkPosition);
					service.playPause();
					break;
				default:
					service.seekTo(lastPosition);
					service.playPause();
					break;
			}
		} else {
			service.seekTo(lastPosition);
		}
	}

	/**
	 * Скрывает анимашку загрузки модели
	 */
	private void hideLoadProgress() {
		initFadeAnimations();
		if (loadProgress == null)
			loadProgress = (ProgressBar) findViewById(R.id.loadProgress);

		loadProgress.startAnimation(fadeOut);
		loadProgress.setVisibility(View.GONE);
		playPauseButton.setVisibility(View.VISIBLE);
		playPauseButton.startAnimation(fadeIn);
	}

	private void setAlphaMenuItem(Menu menu, int index, boolean condition) {
		if (condition)
			menu.getItem(index).getIcon().setAlpha(0xFF);
		else
			menu.getItem(index).getIcon().setAlpha(0x7F);
	}

	@Override
	public void onNextModelPlayback() {
		validateButtons();
		setTrackInfoData(model);
	}

	public void onClickSearchAuthor(View view) {
		Intent intent = new Intent(PlayerActivity.this, CatalogActivity.class);
		intent.putExtra(CatalogActivity.PARAM_SEARCH_TEXT, ((TextView) findViewById(R.id.model_author)).getText().toString().trim());
		startActivity(intent);
	}
}

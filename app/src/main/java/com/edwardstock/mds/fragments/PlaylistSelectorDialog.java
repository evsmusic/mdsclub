package com.edwardstock.mds.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.edwardstock.mds.PlayerActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.adapters.PlaylistAdapter;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.Playlist;
import com.edwardstock.mds.models.PlaylistItem;


public class PlaylistSelectorDialog extends DialogFragment implements View.OnClickListener {
	
	private ListView list;
	private PlaylistAdapter adapter;
	private int modelId;

	public static PlaylistSelectorDialog newInstance(int modelId) {
		PlaylistSelectorDialog fragment = new PlaylistSelectorDialog();

		Bundle args = new Bundle();
		args.putInt(PlayerActivity.PARAM_MODEL_ID, modelId);
		fragment.setArguments(args);

		return fragment;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View v = inflater.inflate(R.layout.fragment_playlist_select, null);
		list = (ListView) v.findViewById(R.id.playlist_list);
		modelId = getArguments().getInt(PlayerActivity.PARAM_MODEL_ID, 0);
		
		adapter = new PlaylistAdapter(getActivity(), Playlist.findAll());
		adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				PlaylistItem.create((int)id, modelId);
				dismiss();
			}
		});
		list.setAdapter(adapter);
		
		
		Button addToPlaylist = (Button) v.findViewById(R.id.add_to_playlist);
		addToPlaylist.setOnClickListener(this);
		return v;
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.add_to_playlist) {
			((BaseActivity) getActivity()).showCreatePlaylistDialog(new Playlist.PlaylistDialogEventListener() {
				@Override
				public void onPositive() {
					adapter.reinitElements(Playlist.findAll());
					adapter.notifyDataSetChanged();
				}

				@Override
				public void onNegative() {

				}
			});
		}
	}
}

package com.edwardstock.mds.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TimePicker;
import android.widget.Toast;

import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.threads.SyncTask;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
	
	private final static String TAG = "TimePickerFragment";
	private final static int TIME_REPEAT = 30 * 1000; //30 секунд

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
	}
	
	@Override
	public void onTimeSet(TimePicker view, final int hourOfDay, final int minute) {
		Toast.makeText(getActivity(), "Таймер установлен на: "+hourOfDay+":"+minute+":00", Toast.LENGTH_LONG).show();

		final Timer myTimer = new Timer();
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				Calendar c = Calendar.getInstance();
				int currentHour = c.get(Calendar.HOUR_OF_DAY);
				int currentMinute = c.get(Calendar.MINUTE);
				
				if(currentHour == hourOfDay && currentMinute == minute) {
					if(BaseActivity.service != null && BaseActivity.service.getMediaPlayer().isPlaying()) {
						BaseActivity.service.stopSelf();
					}
					new SyncTask(getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					Log.d(TAG, "TIME TO PLAYER OFF!");
					myTimer.cancel();
				}
				
				c = null;
			}
		};


		myTimer.schedule(tt, 0, TIME_REPEAT);
	}
}

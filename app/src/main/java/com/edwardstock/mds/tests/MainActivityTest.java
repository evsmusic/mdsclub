package com.edwardstock.mds.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;

import com.edwardstock.mds.MainActivity;

public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

	Intent launchedIntent;

	public MainActivityTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getTargetContext(), MainActivity.class);
		startActivity(intent, null, null);
	}

	public void testLaunch() {
		assertTrue(getStartedActivityIntent() != null);
	}


}

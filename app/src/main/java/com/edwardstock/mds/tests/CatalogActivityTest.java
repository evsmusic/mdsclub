package com.edwardstock.mds.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.ListView;

import com.edwardstock.mds.CatalogActivity;
import com.edwardstock.mds.R;
import com.edwardstock.mds.adapters.CatalogAdapter;
import com.edwardstock.mds.models.RemoteModel;

public class CatalogActivityTest extends ActivityUnitTestCase<CatalogActivity> {

	private final static int ASYNC_WAIT_TIMEOUT = 5000;
	final private String searchQueryOne = "Айзек Азимов";
	final private String searchQueryTwo = "Хрень которой нету";
	private Intent defaultIntent;
	private Intent searchIntent;
	private Intent secondSearchIntent;

	public CatalogActivityTest() {
		super(CatalogActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		defaultIntent = new Intent(getInstrumentation().getTargetContext(), CatalogActivity.class);

		searchIntent = (Intent) defaultIntent.clone();
		searchIntent.putExtra("search_query", searchQueryOne);

		secondSearchIntent = (Intent) defaultIntent.clone();
		secondSearchIntent.putExtra("search_query", searchQueryTwo);

	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testDefaultLaunch() {
		startActivity(defaultIntent, null, null);
		assertTrue(getActivity() != null);
	}

	public void testFirstSearchLaunch() throws InterruptedException {
		startActivity(searchIntent, null, null);

		assertTrue(getAdapter() != null);

		RemoteModel item = getAdapter().getItem(0);

		assertTrue(item != null);
		assertEquals(item.getAuthor(), searchQueryOne);
	}

	private CatalogAdapter getAdapter() throws InterruptedException {
		CatalogActivity a = getActivity();
		ListView catalogList = (ListView) a.findViewById(R.id.modelsListView);
		Thread.sleep(ASYNC_WAIT_TIMEOUT);
		return (CatalogAdapter) catalogList.getAdapter();
	}

	public void testSecondSearchLaunch() throws InterruptedException {
		startActivity(secondSearchIntent, null, null);

		assertEquals(getAdapter().getCount(), 0);
	}


}

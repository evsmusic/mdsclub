package com.edwardstock.mds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.edwardstock.mds.adapters.BookmarksAdapter;
import com.edwardstock.mds.adapters.FavoritesAdapter;
import com.edwardstock.mds.implementations.BaseActivity;
import com.edwardstock.mds.models.LocalModel;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.ArrayList;
import java.util.List;


public class BookmarksActivity extends BaseActivity {

	private ListView listView;
	private BookmarksAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorites);

		Toolbar toolbar = findAndSetSupportActionBar(R.id.favorites_bar);
		initMenu(toolbar, false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		new AsyncTask<Void, Void, List<LocalModel>>() {
			@Override
			protected List<LocalModel> doInBackground(Void... params) {
				return LocalModel.getBookmarks();
			}

			protected void onPostExecute(List<LocalModel> result) {
				super.onPostExecute(result);
				initMainList(new ArrayList<>(result));
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		sendGAScreen();
	}

	private void initMainList(ArrayList<LocalModel> items) {
		adapter = new BookmarksAdapter(this, items);

		listView = (ListView) findViewById(R.id.favoritesListView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				startPlayer(adapter.getItem(position), true);
				listView.invalidate();
			}
		});

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {
				new AlertDialog.Builder(getSupportActionBar().getThemedContext())
					.setTitle("Удалить закладку?")
					.setIcon(android.R.drawable.stat_sys_warning)
					.setPositiveButton("Да", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							LocalModel.setBookmark((int) id, 0);
							listView.setAdapter(new FavoritesAdapter(BookmarksActivity.this, new ArrayList<>(LocalModel.getBookmarks())));
						}
					})
					.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// nothing to do
						}
					}).show();

				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (listView != null && adapter != null) {
			listView.invalidateViews();
			adapter.notifyDataSetInvalidated();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_bookmarks, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == android.R.id.home) {
			finish();
			listView.invalidateViews();
			adapter.notifyDataSetInvalidated();
		}

		if (id == R.id.bookmarks_clear) {
			new AlertDialog.Builder(this)
				.setTitle("Очистить закладки?")
				.setPositiveButton("Да", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						LocalModel.clearBookmarks();
						new AsyncTask<Void, Void, List<LocalModel>>() {
							@Override
							protected List<LocalModel> doInBackground(Void... params) {
								return LocalModel.getBookmarks();
							}

							protected void onPostExecute(List<LocalModel> result) {
								super.onPostExecute(result);
								initMainList(new ArrayList<>(result));
							}
						}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					}
				})
				.setNegativeButton("Нет", null)
				.show();

		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
}

package com.edwardstock.mds;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {

	private final static int SPLASH_DISPLAY_LENGTH = 1700;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				MainActivity.this.startActivity(new Intent(MainActivity.this, CatalogActivity.class));
				MainActivity.this.finish();
			}
		}, SPLASH_DISPLAY_LENGTH);
	}
}

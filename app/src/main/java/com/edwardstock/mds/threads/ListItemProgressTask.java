package com.edwardstock.mds.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.edwardstock.mds.helpers.ProgressProvider;
import com.edwardstock.mds.helpers.downloader.DisplayHelper;

public class ListItemProgressTask extends AsyncTask<Void, Integer, Void> {

	public final static int PROGRESS_INTERVAL = 500;
	private final static String TAG = "ListItemProgressTask";

	private int modelId;
	private Context context;
	private ProgressEventListener listener;
	private boolean error = false;

	public ListItemProgressTask(Context context, int modelId) {
		this.modelId = modelId;
		this.context = context;
	}

	public void setEventListener(ProgressEventListener listener) {
		this.listener = listener;
	}

	@Override
	protected synchronized Void doInBackground(Void... params) {
		ProgressProvider p = ProgressProvider.get();

		if (!p.exists(modelId)) {
			//какая то херня, но что-то вроде wait/notify получилось (потом надо капнуть именно туда)
			int waitingFor = 10;
			int waits = 0;
			while (!p.exists(modelId)) {
				if (waits >= waitingFor)
					break;
				Log.d(TAG, "Waiting for progress... " + waits + " of " + waitingFor);
				try {
					Thread.sleep(500);
					waits++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			if (waits >= waitingFor && !p.exists(modelId)) {
				error = true;
				return null;
			}
		}

		ProgressProvider.Proxy proxy = p.getProxy(modelId);
		int screenSize = DisplayHelper.getDisplayMetrics(context).widthPixels;
		int screenOnePercent = Math.round(screenSize / 100);
		int progressPercent;
		int screenPercent;
		while (!proxy.complete()) {
			if (proxy.errorOccurred()) {
				error = true;
				p.deleteProgress(modelId);
				return null;
			}

			progressPercent = Math.round((((float) proxy.getProgress()) * 100f) / ((float) proxy.getTotal()));
			screenPercent = (screenOnePercent * progressPercent) + progressPercent;

			//округляем, так как у нас плавающие числа работают в процентаже
			if ((screenPercent + 20) >= screenSize) {
				screenPercent = screenSize;
			}

			// тоже самое
			if ((progressPercent + 10) >= 100) {
				progressPercent = 100;
			}

			//публикуем прогресс
			publishProgress(
				screenPercent,
				screenSize,
				proxy.getProgress(),
				proxy.getTotal(),
				progressPercent
			);

			//идем в кроватку на пол секунды, дабы не грузить систему сильно
			try {

				Thread.sleep(PROGRESS_INTERVAL);
			} catch (InterruptedException e) {
				error = true;
				e.printStackTrace();
			}
		}

		p.deleteProgress(modelId);

		return null;
	}

	@Override
	protected void onPostExecute(Void res) {
		super.onPostExecute(res);
		if (!error)
			listener.onComplete();
		else
			listener.onError();

	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
		super.onProgressUpdate(progress);
		listener.onUpdate(
			progress[0], /* screen progress */
			progress[1], /* screen total width */
			progress[2], /* downloaded file size */
			progress[3], /* total file size */
			progress[4]  /* progress percentage */
		);
	}

	public interface ProgressEventListener {
		void onUpdate(int screenProgress, int screenTotalWidth, int progress, int total, int progressPercentage);

		void onComplete();

		void onError();
	}
}

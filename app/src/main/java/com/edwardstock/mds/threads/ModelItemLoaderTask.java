package com.edwardstock.mds.threads;

import android.os.AsyncTask;
import android.util.Log;

import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.models.LocalModel;

import java.io.IOException;

public class ModelItemLoaderTask extends AsyncTask<Void, Void, LocalModel>
{
	private int modelId;
	private OnProcessListener onProcessListener;

	public ModelItemLoaderTask(int modelId) {
		this.modelId = modelId;
	}

	public ModelItemLoaderTask(ModelItemLoaderTask.OnProcessListener context, int modelId) {
		this.onProcessListener = context;
		this.modelId = modelId;
	}

	@Override
	protected LocalModel doInBackground(Void... params) {
		ApiClient client = new ApiClient();
		LocalModel model = null;
		try {
			model = client.getOne(modelId);
			Log.d("ModelItemLoaderTask", "Model is null? "+(model==null));
		} catch (IOException e) {
			e.printStackTrace();
			cancel(true);
		}

		return model;
	}

	protected void onPreExecute() {
		if(onProcessListener != null)
			onProcessListener.onPreTaskExecute();
	}

	protected void onPostExecute(LocalModel result) {
		super.onPostExecute(result);

		if(onProcessListener != null)
			onProcessListener.onPostTaskExecute(result);
	}

	public interface OnProcessListener {
		void onPreTaskExecute();

		void onPostTaskExecute(LocalModel result);
	}
}

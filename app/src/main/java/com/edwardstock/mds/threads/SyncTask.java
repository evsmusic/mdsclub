package com.edwardstock.mds.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.edwardstock.mds.api.SyncClient;

import java.io.IOException;


public class SyncTask extends AsyncTask<Void,Void,Void> {
	
	private final static String TAG = "SyncTask";
	
	private boolean errorOccurred = false;
	private SyncTaskEventListener eventListener;
	private Context context;

	public SyncTask(Context context, SyncTaskEventListener eventListener) {
		this.context = context;
		this.eventListener = eventListener;
	}
	
	public SyncTask(Context context) {
		this.context = context;		
	}
	
	public void setEventListener(SyncTaskEventListener eventListener) {
		this.eventListener = eventListener;		
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		if(!SyncClient.userAuthenticated(context)) {
			//Log.d(TAG, "{{NOT AUTHENTICATED USER sync}}");
			return null;
		}

		SyncClient client = new SyncClient(context);
		try {
			client.syncWithServer();
		} catch (IOException e) {
			errorOccurred = true;
			if(eventListener != null) {
				eventListener.onError(e);
				//Log.d(TAG, "{{on error sync}} - "+e.getMessage());
			}

			e.printStackTrace();
		}

		return null;
	}
	
	protected void onPreExecute() {
		super.onPreExecute();
		if(eventListener != null)
			eventListener.onBefore();

		Log.d(TAG, "{{on pre sync}}");
	}
	
	protected void onPostExecute(Void res) {
		super.onPostExecute(res);
		if(!errorOccurred && eventListener != null) {
			eventListener.onSuccess();
			//Log.d(TAG, "{{on post sync}}");
		}
			
	}
	
	public interface SyncTaskEventListener{
		void onBefore();
		void onSuccess();
		void onError(Exception err);
	}
}

package com.edwardstock.mds.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;

import com.edwardstock.mds.CatalogActivity;
import com.edwardstock.mds.api.ApiClient;
import com.edwardstock.mds.api.ApiClientSearchParams;
import com.edwardstock.mds.models.RemoteModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Загрузчик моделей
 */
public class ModelLoaderTask extends AsyncTask<ListView, Void, List<RemoteModel>>
{

	private final static String TAG = ModelLoaderTask.class.getName();

	private String searchString = null;
	private Context context;
	private CatalogActivity activity;
	private int pageNumber = 0;
	private ApiClientSearchParams additionalParamsBuilder;

	public ModelLoaderTask(Context context) {
		this.context = context;
		this.activity = (CatalogActivity) this.context;
	}

	public ModelLoaderTask(Context context, int pageNumber) {
		this.context = context;
		this.activity = (CatalogActivity) this.context;
		this.pageNumber = pageNumber;
	}

	public ModelLoaderTask(Context context, String searchString) {
		this.context = context;
		this.activity = (CatalogActivity) this.context;
		this.searchString = searchString;
	}

	public ModelLoaderTask(Context context, int pageNumber, ApiClientSearchParams additionalParamsBuilder) {
		this.context = context;
		this.activity = (CatalogActivity) this.context;
		this.pageNumber = pageNumber;
		this.additionalParamsBuilder = additionalParamsBuilder;
	}

	protected void onPreExecute() {
		super.onPreExecute();
		activity.onPreTaskExecute();
	}

	@Override
	protected List<RemoteModel> doInBackground(ListView... params) {
		ApiClient api = new ApiClient();
		List<RemoteModel> remoteModels;

		try {
			if(this.searchString == null && additionalParamsBuilder == null)
				remoteModels = api.getAll(pageNumber);
			else if(this.additionalParamsBuilder != null && this.searchString == null)
				remoteModels = api.getAll(pageNumber, additionalParamsBuilder.buildParams());
			else if(this.additionalParamsBuilder != null)
				remoteModels = api.search(this.searchString, additionalParamsBuilder.buildParams());
			else
				remoteModels = api.search(this.searchString);

			return remoteModels;
		} catch (java.io.InterruptedIOException ie) {
			ie.printStackTrace();
		} catch (Throwable e) {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... voids) {
					return null;
				}
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
					((CatalogActivity)context).showServiceIsUnavailable();
					if(activity.progress != null) {
						activity.progress.setVisibility(View.INVISIBLE);
					}

				}
			}.execute();

			e.printStackTrace();
			cancel(true);
		}

		return null;
	}

	protected void onPostExecute(List<RemoteModel> result) {
		super.onPostExecute(result);
		activity.onPostTaskExecute(new ArrayList<>(result));
	}
}
